<?php

return [
    'customer' => 'Pelanggan',
    'new_customer' => 'Pelanggan Baru',
    'edit_customer' => 'Ubah Pelanggan',
    'name' => 'Nama',
    'address' => 'Alamat',
    'city' => 'Kota',
    'zip_code' => 'Kode Pos',
    'phone' => 'Telepon',
    'fax' => 'Faksimili',
    'email' => 'Surel'
];
