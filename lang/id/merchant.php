<?php

return [
    'version' => 'Version',
    'new' => 'Add MerchantType',
    'edit' => 'Edit MerchantType',
    'tenant' => 'Tenant',
    'name' => 'Merchant',
    'description' => 'description',
    'company' => 'Company',
    'district' => 'District',
    'city' => 'City',
    'state' => 'State',
    'address' => 'Address',
    'zipcode' => 'Zipcode',
    'merchant' => 'MerchantType',
    'merchant' => 'tenant', 
    'merchantType' => 'Merchant Type', 
 ];