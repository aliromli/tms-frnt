<?php

return [

    'add' => 'Tambah',
    'delete' => 'Hapus',
    'bulk_delete' => 'Hapus Masal',
    'edit' => 'Ubah',
    'save' => 'Simpan',
    'update' => 'Perbarui',
    'submit' => 'Kirim',
    'download' => 'Unduh',
    'upload' => 'Unggah',
    'print' => 'Cetak',
    'import' => 'Impor',
    'export' => 'Ekspor',
    'view' => 'Lihat',
    'create' => 'Buat',
    'new' => 'Baru',
    'action' => 'Opsi',
    'close' => 'Tutup',
    'select' => 'Pilih',
    'back' => 'Kembali',
    'yes' => 'Ya',
    'no' => 'Tidak',
    'compare' => 'Bandingkan',
    'required_field' => 'Wajib diisi',
    'all' => 'Semua',
	'diagnostic' => 'Tampilkan diagnosa',
	're-diagnostic' => 'Diagnosa ulang'
];
