<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Sandi harus memiliki minimal delapan karakter dan sama dengan konfirmasi sandi.',
    'reset' => 'Kata sandi telah berhasil di setel ulang!',
    'sent' => 'Kami telah mengirim email link setel ulang sandi.',
    'token' => 'Token setel ulang sandi ini tidak valid.',
    'user' => "Kami tidak dapat menemukan pengguna dengan email tersebut.",

];
