<?php

return [
    'version' => 'Version',
    'new_district' => 'Add New District',
    'edit_district' => 'Edit District',
    'city' => 'City',
    'name' => 'District'
 ];
