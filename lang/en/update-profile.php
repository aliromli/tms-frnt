<?php

return [
    'update_profile' => 'Update Profile',
    'upload_photo' => 'Upload Photo',
    'name' => 'Name',
    'email' => 'Email'
];
