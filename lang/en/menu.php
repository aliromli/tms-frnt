<?php

return [
    'dashboard' => 'Dashboard',
    'terminal_group' => 'Terminal Group',
    'terminal' => 'Terminal',
    'application' => 'Application',
    'device_profile' => 'Model Profile',
    'device_model' => 'Model Device',
    'merchanttype' => 'Merchant Type',
    'tenant' => 'Tenant',
    'merchant' => 'Merchant',
    'logout' => 'Logout',
    'user_management' => 'User Management',
    'user' => 'User',
    'user_group' => 'User Group',
    'menu' => 'Menu',
    'city' => 'City',
    'district' => 'District',
    'state' => 'State',
    'country' => 'Country',
    'master' => 'Master',
    'delete_task' => 'Delete Task',
    'diagnostic' => 'Diagnostic',
    'download_task' => 'Download Task',

    // Menu page
    'builder' => 'Builder',
    'new_menu' => 'New Menu',
    'edit_menu' => 'Edit Menu',
    'add_new_menu' => 'Add New Menu',
    'new_menu_item' => 'New Menu Item',
    'edit_menu_item' => 'Edit Menu Item',
    'name' => 'Name',
    'description' => 'Description',
    'tooltip' => 'Tooltip',
    'icon' => 'Icon',
    'url' => 'URL'
];
