<?php

return [
    'version' => 'Version',
    'name' => 'Name',
    'status' => 'Status',
    'new' => 'Add Download Task',
    'edit' => 'Edit Download Task',
	'publish_time' => 'Publish Time',
	'publish_time_type' => 'Publish Time Type',
	'installation_time_type' => 'Installation Time Type',
	'installation_time' => 'Installation Time',
	'installationNotification' => 'Installation Notification',
	'downloadTimeType' => 'Download Time Type'
 ];
