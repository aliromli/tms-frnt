<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Ali Romli',
                'email' => 'aleemantap@gmail.com',
                'password' => bcrypt('secret'),
                'user_group_id' => 1,
                'active' => 'Y'
            ],[
                'name' => 'Ali2 Romli',
                'email' => 'voicealiromli@gmail.com',
                'password' => bcrypt('secret'),
                'user_group_id' => 2,
                'active' => 'Y'
            ]
        ]);
    }
}
