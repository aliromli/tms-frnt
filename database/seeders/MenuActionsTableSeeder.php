<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_actions')->insert([
            [
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 1
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 2
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 2
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 2
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 2
            ],[
                'action_type' => 'IMPORT',
                'is_visible' => 'Y',
                'menu_id' => 2
            ],[
                'action_type' => 'EXPORT',
                'is_visible' => 'Y',
                'menu_id' => 2
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 3
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 3
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 3
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 3
            ],[
                'action_type' => 'IMPORT',
                'is_visible' => 'Y',
                'menu_id' => 3
            ],[
                'action_type' => 'EXPORT',
                'is_visible' => 'Y',
                'menu_id' => 3
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 4
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 4
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 4
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 4
            ],[
                'action_type' => 'IMPORT',
                'is_visible' => 'Y',
                'menu_id' => 4
            ],[
                'action_type' => 'EXPORT',
                'is_visible' => 'Y',
                'menu_id' => 4
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 5
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 6
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 7
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 8
            ],
            [
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 9
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 10
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 10
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 10
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 10
            ],[
                'action_type' => 'IMPORT',
                'is_visible' => 'Y',
                'menu_id' => 10
            ],[
                'action_type' => 'EXPORT',
                'is_visible' => 'Y',
                'menu_id' => 10
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 11
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 11
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 11
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 11
            ],[
                'action_type' => 'IMPORT',
                'is_visible' => 'Y',
                'menu_id' => 11
            ],[
                'action_type' => 'EXPORT',
                'is_visible' => 'Y',
                'menu_id' => 11
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 12
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 12
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 12
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 12
            ],[
                'action_type' => 'IMPORT',
                'is_visible' => 'Y',
                'menu_id' => 12
            ],[
                'action_type' => 'EXPORT',
                'is_visible' => 'Y',
                'menu_id' => 12
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 13
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 13
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 13
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 13
            ],[
                'action_type' => 'IMPORT',
                'is_visible' => 'Y',
                'menu_id' => 13
            ],[
                'action_type' => 'EXPORT',
                'is_visible' => 'Y',
                'menu_id' => 13
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 14
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 15
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 16
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 17
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 18
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 19
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 20
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 21
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 21
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 21
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 21
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 22
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 22
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 22
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 22
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 23
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 23
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 23
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 23
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 24
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 25
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 26
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 27
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 28
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 29
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 30
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 31
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 32
            ],[
                'action_type' => 'CREATE',
                'is_visible' => 'Y',
                'menu_id' => 33
            ],[
                'action_type' => 'READ',
                'is_visible' => 'Y',
                'menu_id' => 33
            ],[
                'action_type' => 'UPDATE',
                'is_visible' => 'Y',
                'menu_id' => 33
            ],[
                'action_type' => 'DELETE',
                'is_visible' => 'Y',
                'menu_id' => 33
            ]
        ]);
    }
}
