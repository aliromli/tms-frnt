<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserGroupActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_group_menu_actions')->insert([
            [
                'user_group_id' => 1,
                'menu_action_id' => 1
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 2
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 3
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 4
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 5
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 6
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 7
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 8
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 9
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 10
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 11
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 12
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 13
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 14
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 15
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 16
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 17
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 18
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 19
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 20
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 21
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 22
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 23
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 24
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 25
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 26
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 27
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 28
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 29
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 30
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 31
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 32
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 33
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 34
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 35
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 36
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 37
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 38
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 39
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 40
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 41
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 42
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 43
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 44
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 45
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 46
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 47
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 48
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 49
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 50
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 51
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 52
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 53
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 54
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 55
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 56
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 57
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 58
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 59
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 60
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 61
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 62
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 63
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 64
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 65
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 66
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 67
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 68
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 69
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 70
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 71
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 72
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 73
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 74
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 75
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 76
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 77
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 78
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 79
            ],[
                'user_group_id' => 1,
                'menu_action_id' => 80
            ]
        ]);
    }
}
