(function($) {
    "use strict";

    $('#dataTableState').wrap('<div class="dataTables_scroll" />');
    var dataTableState = null;
    if ($('#dataTableState').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableState =  $('#dataTableState').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"lengthChange": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/state-datatables",
            type: 'GET',
            data:  function(d){
                d.state = $('#search-state').val();
                d.country = $('#search-state-country').val();
                
            }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "name", name: "name"},
            {data: "country.name", name: "country.name"},
           
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
	        {
                targets: 4,
                render: function(d,data,row) {
                 
                    let v = row.version;
                   
                    return `
                    <button type="button" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></button>&nbsp;
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-state').click(function() {
        dataTableState.draw(true);
    });

    // Add 
    $('body').on('click', '#btn-add-state', function(){
        $('#modal-state').modal('show');
        $('#modal-state .modal-title').html('Add New State');
       
        $('#modal-state input[type=text],#modal-state input[type=hidden],#modal-state input[type=password],#modal-state input[type=email],#modal-state input[type=number]').val('').removeAttr('readonly');
        $('#divst-version').css('display','none');
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-state').val('');
        $('#search-state-country').val('');
        dataTableState.draw(true);
    });

    // Add New State or update
    $('#btn-submit').click(function(){

        // Update when user id has value
        var url = baseUrl + '/state/update';
        var action = "state";
        if(!$('#state-id').val()) {
            url = baseUrl + '/state/save';
            action = "save";
        }

        if($('#state-id').val()) {
            if(!$('#version-st').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-st').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#state-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'State can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#state-name').focus();
            return;
        }
        if(!$('#state-country').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Country can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#state-country').focus();
            return;
        }

       

        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#state-id').val(),
                'name': $('#state-name').val(),
                'country': $('#state-country').val(),
                'version' : $('#version-st').val()
               
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    dataTableState.ajax.reload();
                    // Reset Form
                    $('#modal-state input[type=text],#modal-state input[type=password],#modal-state input[type=email],#modal-state input[type=number]').val('');
                   
                    // Close modal
                    $('#modal-state').modal('hide');
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit state
    $('#dataTableState').on('click', '.btn-edit', function() {
        $('#modal-state').modal('show');
        $('#modal-state .modal-title').html('Edit State');
        //$('#email').attr('readonly','readonly');
        $('#divst-version').show();
       
        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/state/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    //console.log(resp.responseMessage[0].country.id);
                    $('#state-id').val(resp.responseMessage[0].id);
                    $('#state-name').val(resp.responseMessage[0].name);
                    $('#state-country').val(resp.responseMessage[0].country.id).trigger('change');
                    $('#state-country').focus();
                    $('#version-st').val(resp.responseMessage[0].version);

                }
                else
                {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
               
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    $('#dataTableState').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/state/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableState.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    


})(jQuery);