(function($) {
    "use strict";

    $('#dataTableDeleteTask').wrap('<div class="dataTables_scroll" />');
    var dataTableDeleteTask = null;
    if ($('#dataTableDeleteTask').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableDeleteTask =  $('#dataTableDeleteTask').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/delete-task-datatable",
            type: 'GET',
            data:  function(d){
                d.packageName = $('#search-pn-dt').val();
                d.appName = $('#search-appname-dt').val();
                d.name = $('#search-name-dt').val();
                d.sn = $('#search-sn-dt').val();
                d.terminalGroupId = JSON.stringify($("#search-terminalgroup-dt option:selected").toArray().map(item => item.value));
                d.terminalId =  JSON.stringify($("#search-terminal-dt option:selected").toArray().map(item => item.value));
                
            }
        },
        language: {
          
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "name", name: "name"},
            {data: "deleteTime", name: "deleteTime"},
            {data: "status", name: "status"},
            {data: "id", sortable: false, searchable: false, class: "action"}
			
        ],
        columnDefs:[
	        {
                targets: 5,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    return `
                    <a href="delete-task/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    `;
                }
            }
        ]
        });
      
    }
   
    
    $('#btn-cari-deltask').click(function() {
        dataTableDeleteTask.draw(true);
    });

    

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-pn-dt').val('');
        $('#search-appname-dt').val('');
        $('#search-name-dt').val('');
        $('#search-sn-dt').val('');
        $("#search-terminalgroup-dt").val('');
        $("#search-terminal-dt").val('');
    
        dataTableDeleteTask.draw(true);
    });

    // Add New or update
     $('#btn-submit-delete-task').click(function(){

        // Update when city id has value
        var url = baseUrl + '/delete-task/update';
        var action = "update";
        if(!$('#delete-task-id').val()) {
            url = baseUrl + '/delete-task/save';
            action = "save";
        }

        if($('#delete-task-id').val()) {
            if(!$('#version-app-dtask').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-app-dtask').focus();
                return;
            }
        }

        
        // Has error
        // var hasError = false;
        // Check requirement input d  

        if(!$('#delete-task-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#delete-task-name').focus();
            return;
        }

        if(!$('#delete-task-time').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Delete Time can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#delete-task-time').focus();
            return;
        }
      
        var arlistT = []; 
        $("#table-form-terminal>tbody>tr").each(function(){
            //let a = $(this).find('.sn').text();
            //let b = $(this).find('.model').text();
            //let c = $(this).find('.merchant').text();
            let d = $(this).find('.uid').text();
          	// var ob = new Object();
			// ob.sn = a;
			// ob.model = b;
			// ob.merchant = c;
            // ob.uid = d;
			// arlistT.push(ob);

            arlistT.push(d);
        }); 

        if(arlistT.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}
		
		var arlistapplist = [];
        $("#table-form-app>tbody>tr").each(function(){
            let a = $(this).find('.appn').text();
            let b = $(this).find('.pack').text();
            let c = $(this).find('.ver').text();
            
			var ob = new Object();
			ob.appName = a;
			ob.packageName = b;
			ob.appVersion = c;
			arlistapplist.push(ob);
        }); 
		
		
		if(arlistapplist.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Application can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}
		
		
        // Show loder
        $('.page-loader').removeClass('hidden');
        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: { 

                'id': $('#delete-task-id').val(), 
                'name' : $('#delete-task-name').val(),
                'deleteTime': $('#delete-task-time').val(),
                'applications' :  JSON.stringify(arlistapplist),
                'terminalIds' :  JSON.stringify(arlistT),
                'version' : $('#version-app-dtask').val() 
                 
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    //dataTableCity.ajax.reload();
                  
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    window.location.replace(baseUrl +"/delete-task");

                    //if($('#download-task-id').val()=="") {
                       
                        //$('#list-group-app').html('');
                        //$('#list-group-tg').html('');
                        //$('#list-group-t').html('');
                        //$('#form-download-task')[0].reset();
                       
                    //}


                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    $('#dataTableDeleteTask').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/delete-task/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableDeleteTask.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });
	
	/* modal */
	$('#dataTableTerListDT').wrap('<div class="dataTables_scroll" />');
    var dataTableTerListDT = null;
    if ($('#dataTableTerListDT').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTerListDT =  $('#dataTableTerListDT').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables",
            type: 'GET',
            data:  function(d){
                //d.model = $('#search-model').val();
                //d.vendorName = $('#search-vendor-name').val();
                //d.vendorCountry = $('#search-vendor-country').val();
            }
        },
        language: {
         
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "sn", name: "sn"},
            {data: "modelName", name: "modelName"},
            {data : "merchantName", name : "merchantName"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
	        {
                targets: 4,
                render: function(d,data,row) {
                   return `
                      <input 
                      type="checkbox" 
                      name="chmndt[]" 
                      value="true"  
                      data-id="`+d+`" 
                      data-model="`+row.modelName+`" 
                      data-merchant="`+row.merchantName+`"                      
                      data-sn="`+row.sn+`"
                      />
                    `;
                }
            }
        ]
        });
      
    }
    
	
	$('body').on('click', '#add-terminals-to-list', function() {
        
		$('#modal-terminals-dt').modal('show');
        $('#modal-terminals-dt .modal-title').html('Add Terminal');
		dataTableTerListDT.draw(true);
	
    });

    $('body').on('click', '#btn-submit-mdt', function() {
        var arlist = [];
        $("#table-form-terminal>tbody>tr").each(function(){

            let a = $(this).find('.sn').text();
            let b = $(this).find('.model').text();
            let c = $(this).find('.merchant').text();
            let d = $(this).find('.uid').text();

			var ob = new Object();
			ob.sn = a;
			ob.model = b;
			ob.merchant = c;
            ob.uid = d;
			arlist.push(ob);
        }); 

        var arlist2 = [];
        var datac = $('[name="chmndt[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.sn = $this.data("sn");
                    ob.model = $this.data("model");
                    ob.merchant = $this.data("merchant");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
			let sn = mergedArr[i].sn;
			let model = mergedArr[i].model;
			let merchant = mergedArr[i].merchant;
            let uid = mergedArr[i].uid;
			let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-x2"><i class="fa fa-remove"></i></button>`;
			d+=`<tr class="tr-dt">`+
					`<td><span class="sn">`+sn+`</span></td>`+
					`<td><span class="model">`+model+`</span></td>`+
					`<td><span class="merchant">`+merchant+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		}

        $('#table-form-terminal>tbody').find('tr').remove();  
        $('#table-form-terminal>tbody').html(d);       
		//$("#list-group-terminals").find('li').remove();
        //$("#list-group-terminals").append(html);
        $('#modal-terminals-dt').modal('hide');
		

    });
    $('body').on('click', '.btn-delete-list-delete-task', function() {
        $(this).parent().parent().remove();
    });
   
    //-------------------------- app	
	$('body').on('click', '#add-app', function() {
        $('#modal-app-form').modal('show');
        $('#modal-app-form .modal-title').html('Add Application');
    });
	
	$('body').on('click', '#btn-submit-modal-app', function() {
		 
		var arlist = [];
        $("#table-form-app>tbody>tr").each(function(){
            let a = $(this).find('.appn').text();
            let b = $(this).find('.pack').text();
            let c = $(this).find('.ver').text();
            
			var ob = new Object();
			ob.name = a;
			ob.pack = b;
			ob.version = c;
			arlist.push(ob);
        }); 
		
		 
		let name = $('#app-name').val();
		let pack = $('#app-package-name').val();
		let version = $('#app-version-app').val();
		let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-x2"><i class="fa fa-remove"></i></button>`;
         
		let k =`<tr class="tr-d">`+
					`<td><span class="appn">`+name+`</span></td>`+
					`<td><span class="pack">`+pack+`</span></td>`+
					`<td><span class="ver">`+version+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		var d = "";		
		for(var i=0; i<arlist.length; i++)
		{
			let name = arlist[i].name;
			let pack = arlist[i].pack;
			let version = arlist[i].version;
			let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-x2"><i class="fa fa-remove"></i></button>`;
			d+=`<tr class="tr-d">`+
					`<td><span class="appn">`+name+`</span></td>`+
					`<td><span class="pack">`+pack+`</span></td>`+
					`<td><span class="ver">`+version+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		}
		
		var g = d.concat(k);
				
		$('#table-form-app>tbody').html(g);  
		$('#modal-app-form').modal('hide');
		$('#app-name').val('');
		$('#app-package-name').val('');
		$('#app-version-app').val('');


    });
	
	$('body').on('click', '.btn-delete-x2', function() {
        $(this).parent().parent().remove();
    });

    /* auto complete */
    $("#search-terminal-dt").select2({
        ajax: {
            url: baseUrl+"/download-task-terminal-auto",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term, // search term
                page: params.page
              };
            },
            processResults: function (data, params) {
              // parse the results into the format expected by Select2
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data, except to indicate that infinite
              // scrolling can be used
              params.page = params.page || 1;
        
              return {
                results: data.items,
                pagination: {
                  more: (params.page * 30) < data.total_count
                }
              };
            },
            cache: true
          },
          placeholder: 'Search Terminal',
          minimumInputLength: 1,
          templateResult: formatRepo2,
          templateSelection: formatRepoSelection2
    });
        
    function formatRepo2 (repo) {
        if (repo.loading) {
        return repo.text;
        }
        
        var $container = $(
            "<div class='select2-result-repository clearfix'>" +
                //"<div class='select2-result-repository__avatar badge badge-light'>" + repo.name + "</div>" +
                "<h4 class='header-title'>"+repo.sn+"</h4>"+
                "<table class='table table-bordered'>"+ 
                    //"<thead class='text-uppercase'><tr><th>Name</th><th>Package Name</th><th>Description</th><th>App Version</th></tr></thead>"+
                    "<tbody>"+
                        //"<tr><td>Name</td><td>"+repo.name+"</td></tr>"+
                        "<tr><td>Package</td><td>"+repo.modelName+"</td></tr>"+
                        "<tr><td>Description</td><td>"+repo.merchantName+"</td></tr>"+
                        "<tr><td>App Version</td><td>"+repo.profileName+"</td></tr>"+
                    "</tbody>"+
                "</table>"+
            "</div>"
        );

        return $container;
    }
    
    function formatRepoSelection2 (repo) {
        return repo.sn || repo.text;
     
    }

    $("#search-terminalgroup-dt").select2({
        ajax: {
            url: baseUrl+"/download-task-terminalgroup-auto", 
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term, // search term
                page: params.page
              };
            },
            processResults: function (data, params) {
              // parse the results into the format expected by Select2
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data, except to indicate that infinite
              // scrolling can be used
              params.page = params.page || 1;
        
              return {
                results: data.items,
                pagination: {
                  more: (params.page * 30) < data.total_count
                }
              };
            },
            cache: true
          },
          placeholder: 'Search Terminal Group',
          minimumInputLength: 1,
          templateResult: formatRepo3,
          templateSelection: formatRepoSelection3
    });
        
    function formatRepo3 (repo) {
        if (repo.loading) {
        return repo.text;
        }
        
        var $container = $(
            "<div class='select2-result-repository clearfix'>" +
                //"<div class='select2-result-repository__avatar badge badge-light'>" + repo.name + "</div>" +
                "<h4 class='header-title'>"+repo.name+"</h4>"+
                "<table class='table table-bordered'>"+ 
                    //"<thead class='text-uppercase'><tr><th>Name</th><th>Package Name</th><th>Description</th><th>App Version</th></tr></thead>"+
                    "<tbody>"+
                        "<tr><td>SN</td><td>"+repo.sn+"</td></tr>"+
                        "<tr><td>Description</td><td>"+repo.description+"</td></tr>"+
                    "</tbody>"+
                "</table>"+
            "</div>"
        );

        return $container;
    }
    
    function formatRepoSelection3 (repo) {
        return repo.name || repo.text;
     
    }
   
   

	
	
   
   
})(jQuery);