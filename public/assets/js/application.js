(function($) {
    "use strict";

    $('#dataTableApplication').wrap('<div class="dataTables_scroll" />');
    var dataTableApplication = null;
	
    if ($('#dataTableApplication').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableApplication =  $('#dataTableApplication').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollY": "200px",
       // "scrollCollapse": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/application-datatables",
            type: 'GET',
            data:  function(d){
                d.deviceModelId = $('#search-model-app-id').val(); //search-model-app-id
                d.sn = $('#search-sn-app').val();
                d.name = $('#search-name-app').val();
                
            }
        },
        language: {
        
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "name", name: "name"},
            {data: "packageName", name: "packageName"}, 
			{data: "description", name: "description"},
			{data: "appVersion", name: "appVersion"},
			{data: "companyName", name: "companyName"},
            {data: "uninstallable", name: "uninstallable"},
            //{data: "iconUrl", name: "iconUrl"}, 
            {data: "fileSize", name: "fileSize"},
            {data: "id", sortable: false, searchable: false, class: "action"}
			
        ],
        columnDefs:[
            {
                targets: 8,
                render: function(d,data,row) {
                   return bytesToSize(d);
                }
            },
	        {
                targets: 9,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    return `
                    <a data-toggle="tooltip" data-placement="top" title="Get APK" href="application-get-apk/`+id+`" class="btn btn-flat btn-success btn-xs btn-edit" data-id="`+d+`"><i class="ti-android"></i></a>&nbsp;
                    <a data-toggle="tooltip" data-placement="top" title="Edit" href="application/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                    <button data-toggle="tooltip" data-placement="top" title="Delete" type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-app').click(function() {
        dataTableApplication.draw(true);
    });


    $('body').on('click', '#show-icon', function(){
        //jQuery.noConflict(); 
        $('#modal-show-icon').modal('show');
        //$('#modal-show-icon .modal-title').html('View Icon');
        //alert("ok");
    });




      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-sn-app').val('');
        $('#search-name-app').val('');
        $('#search-model-app-id').val('');
        $('#search-model-app').val('');
        dataTableApplication.draw(true);
    });

    // Add New  or update
    $('#btn-submit-application').click(function(){

        // Update when city id has value
        var url = baseUrl + '/application/update';
        var action = "application";
        if(!$('#application-id').val()) {
            url = baseUrl + '/application/save';
            action = "save";
        }

        if($('#application-id').val()) {
            if(!$('#version-app').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Application can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-app').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#application-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Application Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#application-name').focus();
            return;
        }

        if(!$('#application-packageName').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Package Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#application-packageName').focus();
            return;
        }
        if(!$('#application-description').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Description can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#application-description').focus();
            return;
        }

        if(!$('#application-appVersion').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'AppVersion can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#application-appVersion').focus();
            return;
        }
        //uninstallable[]
      
        if(!$('input[name="uninstallable[]"]:checked').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Company Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            //$('#application-companyName').focus();
            return;
        }
        
        
        if(!$('#application-companyName').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Company Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#application-companyName').focus();
            return;
        } 

        if(!$('#application-deviceModelId').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Model  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#application-deviceModelId').focus();
            return;
        }


        
        // Show loder
        $('.page-loader').removeClass('hidden');

        var form = new FormData();
        var apk = $('#application-apk')[0].files[0];
        var icon = $('#application-icon')[0].files[0];
        form.append('apk', apk);
        form.append('icon', icon);
        form.append('id',$('#application-id').val());
        form.append('packageName',$('#application-packageName').val());
        form.append('name',$('#application-name').val());
        form.append('appVersion',$('#application-appVersion').val());
        form.append('description',$('#application-description').val());
        form.append('companyName',$('#application-companyName').val());
        form.append('deviceModelId',$('#application-deviceModelId').val());
        form.append('uninstallable',$('input[name="uninstallable[]"]:checked').val());
        form.append('version',$('#version-app').val());
        
      
        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
			processData: false,
			cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            
            data: form,
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    //dataTableCity.ajax.reload();
                  
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    if($('#application-id').val()=="") {
                       
                        $('#application-packageName').val("");
                        $("#application-name").val("");
                        $("#application-appVersion'").val("");
                        $("#application-description").val("");
                        $("#application-companyName").val("");
                        $("#application-deviceModelId").val("");
                        $("#application-deviceModel-search").val("");
                        $("#application-deviceModelId").val("");
                        $('#version-app').val("");
                        //const form = document.querySelector('form');
                        //form.reset();
                       
                    }


                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

   
    $('#dataTableApplication').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/application/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableApplication.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });

    //----------  autocomplite devicemodel ----  
    //var path = "{{ route('autocomplete') }}";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $( "#application-deviceModel-search" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/application-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#application-deviceModel-search').val(ui.item.label); // display the selected text
            $('#application-deviceModelId').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });

    //autocomplete serach
   $( "#search-model-app" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/application-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#search-model-app').val(ui.item.label); // display the selected text
            $('#search-model-app-id').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });
  


    function bytesToSize(bytes, seperator = "") {
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
        if (bytes == 0) return 'n/a'
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
        if (i === 0) return `${bytes}${seperator}${sizes[i]}`
        return `${(bytes / (1024 ** i)).toFixed(1)}${seperator}${sizes[i]}`
    }
      


})(jQuery);