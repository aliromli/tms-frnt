(function($) {
    "use strict";
    $('#dataTableTerminal').wrap('<div class="dataTables_scroll" />');
    var dataTableTerminal = null;
    if ($('#dataTableTerminal').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTerminal =  $('#dataTableTerminal').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables",
            type: 'GET',
            data:  function(d){
                d.deviceModelId= $('#ter-deviceModel-id-search').val();
                d.merchantId= $('#ter-merchant-id-search').val();
                d.profileId= $('#ter-profile-id-search').val();
                d.sn= $('#ter-sn-search').val();
               
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "sn", name: "sn"},
            {data: "modelName", name: "modelName"}, 
            {data: "merchantName", name: "merchantName"}, 
            {data: "profileName", name: "profileName"}, 
            {data: "locked", name: "locked"},
            {data: "id", sortable: false, searchable: false, class: "action"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
           
            {
                targets: 7,
                render: function(d,data,row) {
                 //
                    let v = row.version;
                    let id = d;
                    return `
                    <a href="#" class="btn-restart" data-id="`+d+`"><span class="badge badge-pill badge-primary">Restart</span></a> 
                   
                    `;
                }
            },
	        {
                targets: 8,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    let lock = row.locked;
                    return `
                    <a href="terminal/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp; 
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>&nbsp;
                    <button data-toggle="tooltip" data-placement="top" title="UnLock"  type="button" class="btn btn-flat btn-info btn-xs btn-unlock" data-id="`+d+`" data-version="`+v+`" data-lock="`+lock+`"><i class="fa fa-unlock"></i></button>&nbsp;
                    <button data-toggle="tooltip" data-placement="top" title="Lock" type="button" class="btn btn-flat btn-primary btn-xs btn-lock" data-id="`+d+`" data-version="`+v+`" data-lock="`+lock+`"><i class="fa fa-unlock-alt"></i></button>
                    

                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-terminal').click(function() {
        dataTableTerminal.draw(true);
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
       
        $('#ter-deviceModel-id-search').val('');
        $('#ter-deviceModel-search').val('');
        $('#ter-merchant-id-search').val('');
        $('#ter-merchant-search').val('');
        $('#ter-profile-id-search').val('');
        $('#ter-profile-search').val('');
        $('#ter-sn-search').val('');
        dataTableTerminal.draw(true);
    });

    // Add New  or update
    $('#btn-submit-terminal').click(function(){

        // Update when city id has value
        var url = baseUrl + '/terminal/update';
        var action = "terminal";
        if(!$('#terminal-id').val()) {
            url = baseUrl + '/terminal/save';
            action = "save";
        }

        if($('#terminal-id').val()) {
            if(!$('#version-terminal').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-terminal').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#terminal-sn').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'SN can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-sn').focus();
            return;
        }

        if(!$('#terminal-devicemodel').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Model can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-devicemodel').focus();
            return;
        }
        if(!$('#terminal-merchant').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Merchant can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-merchant').focus();
            return;
        }
        
        if(!$('#terminal-deviceprofile').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Profile can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('terminal-#deviceprofile').focus();
            return;
        }

        
        var arlistTg = []; 
        $("#list-group-tg-t>tbody>tr").each(function(){
            let d = $(this).find('.uid').text();
         
            arlistTg.push(d);
        }); 

        if(arlistTg.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal Group can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}
       
        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "id": $('#terminal-id').val(),
                "sn" : $('#terminal-sn').val(),
                "modelId" : $('#terminal-devicemodel').val(),
                "merchantId" : $('#terminal-merchant').val(),
                "profileId" : $('#terminal-deviceprofile').val(),
                'terminalGroupIds' :  JSON.stringify(arlistTg),
                "version": $('#version-terminal').val(),
                
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    //dataTableCity.ajax.reload();
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    if($('#terminal-id').val()=="") {
                       
                        $('#terminal-sn').val("");
                        $('#terminal-devicemodel').val("");
                        $('#terminal-merchant').val("");
                        $('#terminal-deviceprofile').val("");
                    }


                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });
    // terminal group modal

     // Terminal group modal tabel
     $('#dataTableTgListGFormT').wrap('<div class="dataTables_scroll" />');
     var dataTableTgListGFormT = null;
     if ($('#dataTableTgListGFormT').length) {
         // You can use 'alert' for alert message
         // or throw to 'throw' javascript error
         // or none to 'ignore' and hide error
         // or you own function
         // please read https://datatables.net/reference/event/error
         // for more information
         $.fn.dataTable.ext.errMode = 'none';
         
         dataTableTgListGFormT =  $('#dataTableTgListGFormT').DataTable({
         processing: true,
         serverSide: true,
         //dom : '',
         dom: 'lrtip',
         "searching": false,
         
         pageLength: 10,
         lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
         pagingType: 'full_numbers',
         ajax: {
             url:   baseUrl+"/terminal-group-datatables",
             type: 'GET',
             data:  function(d){
 
                 //d.model = $('#search-model').val();
                 //d.vendorName = $('#search-vendor-name').val();
                 //d.vendorCountry = $('#search-vendor-country').val();
                 
             }
         },
         language: {
          
         },
         //rowId: 'TRANSPORT_ID',
         columns: [
             {data: "DT_RowIndex", 
                sortable: false, 
                searchable: false,
                "render": function (data, type, row, meta) {      
                           return meta.row + meta.settings._iDisplayStart + 1;     
                }  
             },
             {data: "version", name: "version"},
             {data: "name", name: "name"},
             {data : "description", name : "description"},
             {data: "id", sortable: false, searchable: false, class: "action"}
         ],
         columnDefs:[
             {
                 targets: 4,
                 render: function(d,data,row) {
                     let id = d;
                     let v = row.version;
                    
                     return `
                     <input 
                     type="checkbox" 
                     name="chtg[]" 
                     value="true"  
                     data-id="`+d+`" 
                     data-name="`+row.name+`" 
                     data-description="`+row.description+`"                      
                     data-version="`+v+`"
                     />
                   `;
                 }
             }
         ]
         });
       
     }
     

    $('body').on('click', '#add-tg-to-list-t', function() {
        
        $('#modal-list-tg-form-t').modal('show');
        $('#modal-list-tg-form-t .modal-title').html('List Terminal Group');
        dataTableTgListGFormT.draw(true);
       
    });

    $('body').on('click', '#btn-tg-to-list-t', function() {

        
        var arlist = [];
        $("#list-group-tg-t>tbody>tr").each(function(){

            let a = $(this).find('.name').text();
            let b = $(this).find('.description').text();
            let c = $(this).find('.uid').text();

			var ob = new Object();
			ob.name = a;
			ob.description = b;
			ob.uid = c;
			arlist.push(ob);
        }); 

        var arlist2 = [];
        var datac = $('[name="chtg[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.name = $this.data("name");
                    ob.description = $this.data("description");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    let name = mergedArr[i].name;
			let description = mergedArr[i].description;
            let uid = mergedArr[i].uid;
			let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-tg-t"><i class="fa fa-remove"></i></button>`;
			d+=`<tr class="tr-dt">`+
					`<td><span class="name">`+name+`</span></td>`+
					`<td><span class="description">`+description+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		}

        $('#list-group-tg-t>tbody').find('tr').remove();  
        $('#list-group-tg-t>tbody').html(d);       
		$('#modal-list-tg-form-t').modal('hide');
		
    });

    $('body').on('click', '.btn-delete-list-tg-t', function() {
        $(this).parent().parent().remove();
    });
    //end == tg

    
   $('#dataTableTerminal').on('click', '.btn-lock', function() {
    
        if($(this).data('lock')==1)
        {
            alert("System already locked");
        } 
        else
        {
            // if (confirm("Are you sure to Lock !!") == true) {

            
            // }
            $('#modal-lock').modal('show');
        }
        
    });

   $('#dataTableTerminal').on('click', '.btn-unlock', function() {

        if($(this).data('lock')==0)
        {
            alert("System already Unlocked");
        } 
        else
        {
            // if (confirm("Are you sure to UnLock !!") == true) {

            //      // Send data
            // $.ajax({
            //     url:  baseUrl + '/terminal/lockUnlock',
            //     type: 'POST',
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     data: {
            //         'id': $(this).data('id'),
            //         'version': $(this).data('version'),
            //         'action' : "UNLOCK",
            //         'lockReason' : "Kunci aja soih"
        
        
            //     },
            //     success: function(resp) {
            //         if(resp.responseCode === 200) {
            //             // Reload datatable
            //             dataTableDeviceprofile.ajax.reload();
                        
            //             // Send success message
            //             $.smallBox({
            //                 height: 50,
            //                 title : "Success",
            //                 content : resp.responseMessage,
            //                 color : "#109618",
            //                 sound_file: "voice_on",
            //                 timeout: 3000
            //                 //icon : "fa fa-bell swing animated"
            //             });
            //         } else {
            //             $.smallBox({
            //                 height: 50,
            //                 title : "Error",
            //                 content : resp.responseMessage,
            //                 color : "#dc3912",
            //                 sound_file: "smallbox",
            //                 timeout: 3000
            //                 //icon : "fa fa-bell swing animated"
            //             });
            //         }
            //         // Hide loder
            //         $('.page-loader').addClass('hidden');
            //     },
            //     error: function(xhr, ajaxOptions, thrownError) {
            //         $.smallBox({
            //             title : "Error",
            //             content : xhr.statusText,
            //             color : "#dc3912",
            //             timeout: 3000
            //             //icon : "fa fa-bell swing animated"
            //         });
            //         // Hide loder
            //         $('.page-loader').addClass('hidden');
            //     }
            // });

            
            // }
            $('#modal-unlock').modal('show');
           
            //$('#email').attr('readonly','readonly');
        }
    
   });

   $('#dataTableTerminal').on('click', '.btn-restart', function() {

        if (confirm("Are you sure to Restart!") == true) {

            
        }


   });

    $('#dataTableTerminal').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/terminal/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableDeviceprofile.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });

    //----------  autocomplite devicemodel ----  
    //var path = "{{ route('autocomplete') }}";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $( "#ter-deviceModel-search" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/terminal-device-model-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#ter-deviceModel-search').val(ui.item.label); // display the selected text
            $('#ter-deviceModel-id-search').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });

    $( "#ter-merchant-search" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/terminal-merchant-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#ter-merchant-search').val(ui.item.label); // display the selected text
            $('#ter-merchant-id-search').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });

    $( "#ter-profile-search" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/terminal-profile-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#ter-profile-search').val(ui.item.label); // display the selected text
            $('#ter-profile-id-search').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });



    
   

})(jQuery);