(function($) {
    "use strict";

    $('#dataTableUser').wrap('<div class="dataTables_scroll" />');
    var userDataTable = null;
    if ($('#dataTableUser').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';

        userDataTable =  $('#dataTableUser').DataTable({
        //dom: '<"dt-toolbar"<"col-sm-6 col-xs-12 hidden-xs"f><"col-sm-6 col-xs-12 hidden-xs text-right"<"toolbar">>>rt<"dt-toolbar-footer"<"col-sm-6 col-xs-12"i><"col-sm-6 col-xs-12 hidden-xs"p>><"clear">',
        processing: true,
        serverSide: true,
        dom: 'lrtip',
        "lengthChange": false,
        pageLength: 15,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/user-datatables",
            type: 'GET',
            data:  function(d){
                d.name = $('#search-name').val();
                d.tenant = $('#search-tenant').val();
                d.group = $('#search-group').val();
               
            }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            //{data: "DT_RowIndex", sortable: false, searchable: false},
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "name", name: "name"},
            {data: "email", name: "email"},
            //{data: "user_group.name", name: "userGroup.name"},
            {data: "userGroup", name: "userGroup"},
            {data: "tenant", name: "tenant"},
            {data: "active", name: "active"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
	        {
                targets: 6,
                render: function(d,data,row) {

                
                    let btnActive= '';
                    if(row.active == "Y"){
                        btnActive = `<button class="btn btn-default btn-flat btn-xs btn-inactive" data-id="`+d+`"><i class="fa fa-ban"></i></button>`;
                    }
                    else
                    {
                        btnActive = `<button class="btn btn-flat btn-default btn-xs btn-active" data-id="`+d+`"><i class="fa fa-check-circle"></i></button>`;

                    }
                    return `<button type="button" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></button>&nbsp; `+btnActive;
                    // return `
                    // <a href="merchant/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                    // <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    // `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-user').click(function() {
        userDataTable.draw(true);
    });

    
    // clear
    $('.clear-search').click(function() {
        $('#search-name').val(''); 
        $('#search-group').val(''); 
        userDataTable.draw(true);
    });
	

    // Add User Group
    $('body').on('click', '#btn-add-user', function(){
        $('#modal-user').modal('show');
        $('#modal-user .modal-title').html('Add New User');
        //$('#modal-user input').val('');
        $('#modal-user input[type=text],#modal-user input[type=hidden],#modal-user input[type=password],#modal-user input[type=email],#modal-user input[type=number]').val('').removeAttr('readonly');

    });

    // Add New Or Update Program
    $('#btn-submit').click(function(){

        // Update when user id has value
        var url = baseUrl + '/user/update';
        var action = "update";
        if(!$('#user-id').val()) {
            url = baseUrl + '/user/save';
            action = "save";
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#email').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Email can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#email').focus();
            return;
        }
        if(!$('#name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#name').focus();
            return;
        }
        if(!$('#password').val() && action === "save") {
            $.smallBox({
                title : "Error",
                content : 'Password can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#password').focus();
            return;
        }
        if(!$('#confirm-password').val() && action === 'save') {
            $.smallBox({
                title : "Error",
                content : 'Please confirm password',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#confirm-password').focus();
            return;
        }
        if($('#password').val() !== $('#confirm-password').val()) {
            $.smallBox({
                title : "Error",
                content : 'Password doest\'t match',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#confirm-password').focus();
            return;
        }
        if(!$('#user-group').val()) {
            $.smallBox({
                title : "Error",
                content : 'User group can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#user-group').focus();
            return;
        }

      
        if(!$('input[type="radio"][name="active[]"]:checked').val()) {
            $.smallBox({
                title : "Error",
                content : 'Active can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            //$('#user-group').focus();
            
            return;
        }

        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#user-id').val(),
                'email': $('#email').val(),
                'name': $('#name').val(),
                'password': $('#password').val(),
                'group': $('#user-group').val(),
                'tenant': $('#tenant').val(),
                //'vendor': $('#vendor').val(),
                'active': $('input[type="radio"][name="active[]"]:checked').val()
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    userDataTable.ajax.reload();
                    // Reset Form
                    $('#modal-user input[type=text],#modal-user input[type=password],#modal-user input[type=email],#modal-user input[type=number]').val('');
                    $('#modal-user select').val('').trigger('change');
                    // Close modal
                    $('#modal-user').modal('hide');
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit user
    $('#dataTableUser').on('click', '.btn-edit', function() {
        $('#modal-user').modal('show');
        $('#modal-user .modal-title').html('Edit User');
        $('#email').attr('readonly','readonly');

        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/user/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                $('#user-id').val(resp.id);
                $('#name').val(resp.name);
                $('#email').val(resp.email);
                $('#user-group').val(resp.user_group_id).trigger('change');
                $('#tenant').val(resp.tenant_id).trigger('change');
                //$('#vendor').val(resp.vendor_id).trigger('change');
                $('input[type="password"]'). val('');
                if(resp.active === 'Y') {
                    $('#user-active-yes').prop('checked', true);
                    $('#user-active-no').prop('checked', false);
                } else {
                    $('#user-active-yes').prop('checked', false);
                    $('#user-active-no').prop('checked', true);
                }

                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

     // Disabled user
     $('#dataTableUser').on('click', '.btn-active', function() {

        if(!confirm('Area you sure want to activate this user?')) {
            return;
        }

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/user/activate/'+$(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    userDataTable.ajax.reload();
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
            }
        });

    });

    // Activate user
    $('#dataTableUser').on('click', '.btn-inactive', function() {

        if(!confirm('Area you sure want to inactivate this user?')) {
            return;
        }

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/user/inactivate/'+$(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    userDataTable.ajax.reload();
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
            }
        });
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });



})(jQuery);