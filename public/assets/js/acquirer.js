(function($) {
	
    "use strict";
    $('#dataTableAcquirer').wrap('<div class="dataTables_scroll" />');
    var dataTableAcquirer = null;
    if ($('#dataTableAcquirer').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableAcquirer =  $('#dataTableAcquirer').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/acquirer-datatable",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "name", name: "name"},
            {data: "type", name: "type"}, 
            {data: "description", name: "description"},   
            {data: "hostId", name: "hostId"},   
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
		
        columnDefs:[
	        {
                targets: 6,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    return `
                    <a href="acquirer/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-ac').click(function() {
        dataTableAcquirer.draw(true);
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-name').val('');
       
        dataTableAcquirer.draw(true);
    });

    // Add New  or update
    $('#btn-submit-ac').click(function(){

        // Update when city id has value
        var url = baseUrl + '/acquirer/update';
        var action = "update";
        if(!$('#ac-id').val()) {
            url = baseUrl + '/acquirer/save';
            action = "save";
        }

        if($('#ac-id').val()) {
            if(!$('#version-ac').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-ac').focus();
                return;
            }
        }

		//disini
        // Check requirement input
        if(!$('#ac-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#ac-name').focus();
            return;
        }

        if(!$('#ac-type').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Type can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#ac-type').focus();
            return;
        }
		
		
		
		// Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#ac-id').val(),
                'name': $('#ac-name').val(),
                'type': $('#ac-type').val(),
                'description': $('#ac-description').val(),
                'hostId': $('#ac-hostId').val(),
                'settlementHostId': $('#ac-settlementHostId').val(),
                'numberOfPrint': $('#ac-numberOfPrint').val(),
                'respTimeout': $('#ac-respTimeout').val(),
                'acquirerId': $('#ac-acquirerId').val(),
                'hostDestAddr': $('#ac-hostDestAddr').val(),
                'hostDestPort': $('#ac-hostDestPort').val(),
                'tleAcquirer': $('#ac-tleAcquirer').val(),
                'tleSettingId': $('#ac-tleSettingId').val(),
                'masterKeyLocation': $('#ac-masterKeyLocation').val(),
                'masterKey': $('#ac-masterKey').val(),
                'workingKey': $('#ac-workingKey').val(),
                'batchNumber': $('#ac-batchNumber').val(),
                'merchantId': $('#ac-merchantId').val(),
                'terminalId': $('#ac-terminalId').val(),
                'showPrintExpDate': $('#ac-showPrintExpDate').val(),
                'checkCardExpDate': $('#ac-checkCardExpDate').val(),
                'creditSettlement': $('#ac-creditSettlement').val(),
                'debitSettlement': $('#ac-debitSettlement').val(),
                'terminalExtId': $('#ac-terminalExtId').val(),
                'version': $('#version-ac').val(),
                
            },
		
            success: function(resp) {
				
				
                if(resp.responseCode == '0000') { //sukses
                   
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					window.location.replace(baseUrl +"/acquirer");

                    // if($('#capk-id').val()=="") {
                       
						// $('#capk-name').val('');
                        // $('#capk-idx').val('');
						// $('#capk-rid').val();
						// $('#capk-modulus').val('');
						// $('#capk-exponent').val('');
						// $('#capk-algo').val('');
						// $('#capk-hash').val('');
						// $('#capk-expiryDate').val('');
						// $('#capk-remark').val('');
                    // }

 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit 
    $('#dataTableAcquirer').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/capk/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableAcquirer.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    function isJson(str) {
	  try {
		JSON.parse(str);
	  } catch (e) {
		return false;
	  }  
	  return true;
	}


})(jQuery);