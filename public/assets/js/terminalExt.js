(function($) {
    "use strict";
    $('#dataTableTe').wrap('<div class="dataTables_scroll" />');
    var dataTableTe = null;
    if ($('#dataTableTe').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTe =  $('#dataTableTe').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminalExt-datatable",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "sn", name: "sn"},
            {data: "tid", name: "tid"}, 
            {data: "mid", name: "mid"},   
            {data: "merchantName1", name: "merchantName1"},   
            {data: "merchantName2", name: "merchantName2"},   
            {data: "merchantName3", name: "merchantName3"},   
            {data: "callCenter1", name: "callCenter1"},   
            {data: "callCenter2", name: "callCenter2"},   
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
		
        columnDefs:[
	        {
                targets: 9,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    return `
                    <a href="terminalExt/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-te').click(function() {
        dataTableTe.draw(true);
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-name').val('');
       
        dataTableTe.draw(true);
    });

    // Add New  or update
    $('#btn-submit-te').click(function(){

        // Update when city id has value
        var url = baseUrl + '/terminalExt/update';
        var action = "update";
        if(!$('#te-id').val()) {
            url = baseUrl + '/terminalExt/save';
            action = "save";
        }

        if($('#te-id').val()) {
            if(!$('#version-te').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-te').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#te-tid').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'TID  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#te-tid').focus();
            return;
        }
		
		if(!$('#te-mid').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'MID  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#te-mid').focus();
            return;
        }

        if(!$('#te-merchantName1').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'MerchantName1 can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#te-merchantName1').focus();
            return;
        }
		
		
		// Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#te-id').val(),   
				'tid' : $('#te-tid').val(),
				'mid' : $('#te-mid').val(),
				'merchantName1' : $('#te-merchantName1').val(),
				'merchantName2' : $('#te-merchantName2').val(),
				'merchantName3' : $('#te-merchantName3').val(),
				'merchant_password' : $('#te-merchant_password').val(),
				'admin_password' : $('#te-admin_password').val(),
				'callCenter1' : $('#te-callCenter1').val(),
				'callCenter2' : $('#te-callCenter2').val(),
				'settlementMaxTrxCount' : $('#te-settlementMaxTrxCount').val(),
				'settlementWarningTrxCount' : $('te-#settlementWarningTrxCount').val(),
				'settlementPassword' : $('#te-settlementPassword').val(),
				'voidPassword' : $('#te-voidPassword').val(),
				'brizziDiscountPercentage' : $('#te-brizziDiscountPercentage').val(),
				'brizziDiscountAmount' : $('#te-brizziDiscountAmount').val(),
				'fallbackEnabled' : $('input[name="fallbackEnabled[]"]:checked').val(),
				'featureSale' : $('input[name="featureSale[]"]:checked').val(),
				'featureInstallment' : $('input[name="featureInstallment[]"]:checked').val(),
				'featureCardCerification' : $('#te-featureCardCerification').val(),
				'featureSaleRedemption' : $('input[name="featureSaleRedemption[]"]:checked').val(),
				'featureManualKeyIn' : $('input[name="featureManualKeyIn[]"]:checked').val(),
				'featureSaleCompletion' : $('input[name="featureSaleCompletion[]"]:checked').val(),
				'featureSaleTip' : $('input[name="featureSaleTip[]"]:checked').val(),
				'featureSaleFareNonFare' : $('input[name="featureSaleFareNonFare[]"]:checked').val(),
				'featureQris' : $('input[name="featureQris[]"]:checked').val(),
				'featureContactless' : $('input[name="featureContactless[]"]:checked').val(),
				'randomPinKeypad' : $('input[name="randomPinKeypad[]"]:checked').val(),
				'beepPinKeypad' : $('input[name="beepPinKeypad[]"]:checked').val(),
				'autoLogon' : $('input[name="autoLogon[]"]:checked').val(),
				'pushLogon' : $('#te-pushLogon').val(),
				'hostReport' : $('input[name="hostReport[]"]:checked').val(),
				'hostLogging' : $('input[name="hostLogging[]"]:checked').val(),
				'importDefault' : $('input[name="importDefault[]"]:checked').val(),
                'version': $('#version-te').val(),
                
            },
		
            success: function(resp) {
				
				
                if(resp.responseCode == '0000') { //sukses
                   
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    window.location.replace(baseUrl +"/terminalExt");

 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit 
    $('#dataTableTe').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/capk/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableTe.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    function isJson(str) {
	  try {
		JSON.parse(str);
	  } catch (e) {
		return false;
	  }  
	  return true;
	}


})(jQuery);