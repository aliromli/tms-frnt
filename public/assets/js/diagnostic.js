(function($) {
    "use strict";

    $('#dataTableLastDiagnostic').wrap('<div class="dataTables_scroll" />');
    var dataTableLastDiagnostic = null;
    if ($('#dataTableLastDiagnostic').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableLastDiagnostic =  $('#dataTableLastDiagnostic').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/diagnostic-lastDiagnostic-data",
            type: 'GET',
            data:  function(d){

                d.sn = $('#search-sn').val();
                d.terminalId = $('#search-terminal-id').val();
                
            }
        },
        language: {
          
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "sn", name: "sn"},
            {data: "batteryTemp", name: "batteryTemp"},
            {data: "batteryPercentage", name: "batteryPercentage"},
            {data: "latitude", name: "latitude"},
            {data: "longitude", name: "longitude"},
            {data: "meid", name: "meid"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
       
        columnDefs:[
	        {
                targets: 7,
                render: function(d,data,row) {
                 
                    
                    let id = row.sn;
                    return `
                    <a href="diagnostic-lastDiagnostic-detail/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`">Detail</a>&nbsp;
                    `;
                }
            }
        ]
        });
      
    }
    
    $('#btn-cari').click(function() {
        dataTableLastDiagnostic.draw(true);
    });

    

    /* filter tugel */
    // Toggle filter
    $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-sn').val();
        $('#search-terminal-id').val();
        dataTableLastDiagnostic.draw(true);
    });

  
  
})(jQuery);