(function($) {
    "use strict";
    $('#dataTableAid').wrap('<div class="dataTables_scroll" />');
    var dataTableAid = null;
    if ($('#dataTableAid').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableAid =  $('#dataTableAid').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/aid-datatable",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "name", name: "name"},
            {data: "txnType", name: "txnType"}, 
            {data: "aidVersion", name: "aidVersion"}, 
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
		
        columnDefs:[
	        {
                targets: 5,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    return `
                    <a href="aid/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-dp').click(function() {
        dataTableAid.draw(true);
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-name').val('');
       
        dataTableAid.draw(true);
    });

    // Add New  or update
    $('#btn-submit-aid').click(function(){

        // Update when city id has value
        var url = baseUrl + '/aid/update';
        var action = "update";
        if(!$('#aid-id').val()) {
            url = baseUrl + '/aid/save';
            action = "save";
        }

        if($('#aid-id').val()) {
            if(!$('#version-aid').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-aid').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#aid-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-name').focus();
            return;
        }

        if(!$('#aid-aidVersion').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'AidVersion can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-version').focus();
            return;
        }
		
		 if(!$('#aid-aid').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Aid can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-version').focus();
            return;
        }
		

        if(!$('#aid-tacDefault').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'TacDefault can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-tacDefault').focus();
            return;
        }

        if(!$('#aid-tacDenial').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'TacDenial can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-tacDenial').focus();
            return;
        }
        if(!$('#aid-tacOnline').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Tac Online can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-tacOnline').focus();
            return;
        }

        if(!$('#aid-threshold').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Threshold  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-threshold').focus();
            return;
        }


        if(!$('#aid-targetPercentage').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Target Percentage  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-targetPercentage').focus();
            return;
        }
	

		if(!$('#aid-maxTargetPercentage').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Max Target Percentage can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-maxTargetPercentage').focus();
            return;
        }
		
		if(!$('#aid-ddol').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Ddol can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-ddol').focus();
            return;
        }
		
		if(!$('#aid-tdol').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Tdol can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-tdol').focus();
            return;
        }
		if(!$('#aid-floorLimit').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'FloorLimit can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-floorLimit').focus();
            return;
        }
		if(!$('#aid-appSelect').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'AppSelect can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-appSelect').focus();
            return;
        }
		if(!$('#aid-aidPriority').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Aid Priority can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-aidPriority').focus();
            return;
        }
		if(!$('#aid-trxType9C').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'TrxType9C can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-trxType9C').focus();
            return;
        }
		if(!$('#aid-categoryCode').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'CategoryCode can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-categoryCode').focus();
            return;
        }
		if(!$('#aid-clKernelToUse').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'ClKernelToUse can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-clKernelToUse').focus();
            return;
        }
		if(!$('#aid-clOptions').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'clOptions can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-clOptions').focus();
            return;
        }
		if(!$('#aid-clTrxLimit').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'clTrxLimit can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-clTrxLimit').focus();
            return;
        }
		if(!$('#aid-clCVMLimit').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'clCVMLimit can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-clCVMLimit').focus();
            return;
        }
		if(!$('#aid-clFloorLimit').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'clFloorLimit can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-clFloorLimit').focus();
            return;
        }
		if(!$('#aid-remark').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'remark can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-remark').focus();
            return;
        }
		if(!$('#aid-emvConfTerminalCapability').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'emvConfTerminalCapability can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-emvConfTerminalCapability').focus();
            return;
        }
		if(!$('#aid-additionalTerminalCapability').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Additional Terminal Capability can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-additionalTerminalCapability').focus();
            return;
        }
		if(!$('#aid-dataTtq').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Additional Terminal Capability can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#aid-dataTtq').focus();
            return;
        }

        
        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#aid-id').val(),
                'name' : $('#aid-name').val(),
                'txnType' : $('#aid-txnType').val(),
                'aid' : $('#aid-aid').val(),
                'aidVersion' : $('#aid-aidVersion').val(),
                'tacDefault' : $('#aid-tacDefault').val(),
                'tacDenial' : $('#aid-tacDenial').val(),
                'tacOnline' : $('#aid-tacOnline').val(),
                'threshold' : $('#aid-threshold').val(),
                'targetPercentage' : $('#aid-targetPercentage').val(),
                'maxTargetPercentage' : $('#aid-maxTargetPercentage').val(),
                'ddol' : $('#aid-ddol').val(),
                'tdol' : $('#aid-tdol').val(),
                'floorLimit' : $('#aid-floorLimit').val(),
                'appSelect' : $('#aid-appSelect').val(),
                'aidPriority' : $('#aid-aidPriority').val(),
                'trxType9C' : $('#aid-trxType9C').val(),
                'categoryCode' : $('#aid-categoryCode').val(),
                'clKernelToUse' : $('#aid-clKernelToUse').val(),
                'clOptions' : $('#aid-clOptions').val(),
                'clTrxLimit' : $('#aid-clTrxLimit').val(),
                'clCVMLimit' : $('#aid-clCVMLimit').val(),
                'clFloorLimit' : $('#aid-clFloorLimit').val(),
                'remark' : $('#aid-remark').val(),
                'emvConfTerminalCapability' : $('#aid-emvConfTerminalCapability').val(),
                'additionalTerminalCapability' : $('#aid-additionalTerminalCapability').val(),
                'dataTtq' : $('#aid-dataTtq').val(),
                'version': $('#version-aid').val(),
                
                
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    //dataTableCity.ajax.reload();
                  
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    if($('#aid-id').val()=="") {
                       
						$('#aid-name').val('');
                        $('#aid-txnType').val('');
                        $('#aid-aid').val('');
                        $('#aid-aidVersion').val('');
					    $('#aid-tacDefault').val('');
						$('#aid-tacDenial').val('');
						$('#aid-tacOnline').val('');
						$('#aid-threshold').val('');
                        $('#aid-targetPercentage').val('');
						$('#aid-maxTargetPercentage').val('');
						$('#aid-ddol').val('');
						$('#aid-tdol').val('');
						$('#aid-floorLimit').val('');
						$('#aid-appSelect').val('');
						$('#aid-aidPriority').val('');
						$('#aid-trxType9C').val('');
						$('#aid-categoryCode').val('');
						$('#aid-clKernelToUse').val('');
						$('#aid-clOptions').val('');
						$('#aid-clTrxLimit').val('');
						$('#aid-clCVMLimit').val('');
						$('#aid-clFloorLimit').val('');
						$('#aid-remark').val('');
						$('#aid-emvConfTerminalCapability').val('');
						$('#aid-additionalTerminalCapability').val('');
						$('#aid-dataTtq').val('');
                        
                      
                    }


                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit 
    

    $('#dataTableAid').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/aid/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableAid.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    


})(jQuery);