(function($) {
    "use strict";

     //---
   
   $('#dataTableLastHeartBeat').wrap('<div class="dataTables_scroll" />');
   var dataTableLastHeartBeat = null;
   if ($('#dataTableLastHeartBeat').length) {
       // You can use 'alert' for alert message
       // or throw to 'throw' javascript error
       // or none to 'ignore' and hide error
       // or you own function
       // please read https://datatables.net/reference/event/error
       // for more information
       $.fn.dataTable.ext.errMode = 'none';
       
       dataTableLastHeartBeat =  $('#dataTableLastHeartBeat').DataTable({
       processing: true,
       serverSide: true,
       //dom : '',
       dom: 'lrtip',
       "searching": false,
       pageLength: 10,
       lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
       pagingType: 'full_numbers',
       ajax: {
           url:   baseUrl+"/diagnostic-lastheartbeat-data",
           type: 'GET',
           data:  function(d){

               d.sn = $('#search-sn').val();
               d.terminalId = $('#search-terminal-id').val();
               
           }
       },
       language: {
         
       },
       //rowId: 'TRANSPORT_ID',
     
       columns: [
           {data: "DT_RowIndex", 
              sortable: false, 
              searchable: false,
              "render": function (data, type, row, meta) {      
                         return meta.row + meta.settings._iDisplayStart + 1;     
              }  
           },
           {data: "sn", name: "sn"},
           {data: "batteryTemp", name: "batteryTemp"},
           {data: "batteryPercentage", name: "batteryPercentage"},
           {data: "latitude", name: "latitude"},
           {data: "longitude", name: "longitude"},
           {data: "cellName", name: "cellName"},
           {data: "cellType", name: "cellType"},
           {data: "cellStrength", name: "cellStrength"},
           {data: "updateTime", name: "updateTime"},
           //{data: "id", sortable: false, searchable: false, class: "action"}
       ], 
    //    columnDefs:[
    //        {
    //            targets: 10,
    //            render: function(d,data,row) {
                
    //                let v = row.version;
    //                let id = d;
    //                return `
    //                <a href="device-model/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
    //                <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
    //                `;
    //            }
    //        }
    //    ]
       });
     
   }

   $('#btn-cari-lhb').click(function() {
         dataTableLastHeartBeat.draw(true);
    });



    /* filter tugel */
    // Toggle filter
    $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        //alert("ok");
    });

    

    // clear
    $('.clear-search').click(function() {
        $('#search-sn').val('');
        $('#search-termina-id').val('');
        dataTableLastHeartBeat.draw(true);
    });

  
  
})(jQuery);