(function($) {
    "use strict";
    
    $('#dataTableDownloadTask').wrap('<div class="dataTables_scroll" />');
    var dataTableDownloadTask = null;
    if ($('#dataTableDownloadTask').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableDownloadTask =  $('#dataTableDownloadTask').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/download-task-datatables",
            type: 'GET',
            data:  function(d){
                d.name = $('#search-name-dnt').val();
                d.sn = $('#search-sn-dnt').val();
                d.packageName = $('#search-pn-dnt').val();
                d.applicationId = JSON.stringify($("#search-application-dnt option:selected").toArray().map(item => item.value));
                d.terminalId = JSON.stringify($("#search-terminal-dnt option:selected").toArray().map(item => item.value));
                d.terminalGroupId = JSON.stringify($("#search-terminalgroup-dnt option:selected").toArray().map(item => item.value));
                
                
                //d.model = $('#search-model').val();
                //d.vendorName = $('#search-vendor-name').val();
                //d.vendorCountry = $('#search-vendor-country').val();
                // $('#SelectQButton option:selected')
                //.toArray().map(item => item.value);
            }
        },
        language: {
          
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "name", name: "name"},
            {data: "status", name: "status"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
	        {
                targets: 4,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    return `
                    <a href="download-task/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    `;
                }
            }
        ]
        });
      
    }
   
    
    $('#btn-cari-dnt').click(function() {
        dataTableDownloadTask.draw(true);
    });

    

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-name-dnt').val('');
        $('#search-sn-dnt').val('');
        $('#search-pn-dnt').val('');
        $('#search-application-dnt').val('');
        $('#search-terminal-dnt').val('');
        $('#search-terminalgroup-dnt').val('');
        dataTableDownloadTask.draw(true);
    });

    // Add New or update
    $('#btn-submit-dt').click(function(){

        // Update when city id has value
        var url = baseUrl + '/download-task/update';
        var action = "download-task";
        if(!$('#download-task-id').val()) {
            url = baseUrl + '/download-task/save';
            action = "save";
        }

        if($('#download-task-id').val()) {
            if(!$('#version-dt').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-dm').focus();
                return;
            }
        }

        if(!$('input[name="downloadstatus[]"]:checked').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Status Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            //$('#application-companyName').focus();
            return;
        }
        
        // Has error
        // var hasError = false;
        // Check requirement input d  

        if(!$('#download-task-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#download-task-name').focus();
            return;
        }

        if(!$('#download-task-installationNotification').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Installation Notification can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('# download-task-installationNotification').focus();
            return;
        }
      
       

        if(!$('#download-task-downloadTimeType').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Download Time Type can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#download-task-downloadTimeType').focus();
            return;
        }

        if($('#download-task-downloadTimeType').val()==2)
        {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Download Time Type can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            //$('#download-task-downloadTime').focus();
            return;
        }


        if(!$('#download-task-publish_time').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Publish Time can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#download-task-publish_time').focus();
            return;
        }

        if(!$('#download-task-publish_time_type').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Publish Time Type can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#download-task-publish_time_type').focus();
            return;
        }

    

        if(!$('#download-task-installation_time').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Isntallation Time can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#download-task-installation_time').focus();
            return;
        }

        if(!$('#download-task-installation_time_type').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Isntallation Time Type can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#download-task-installation_time_type').focus();
            return;
        }

        var arlistApp = []; 
        $("#list-group-app>tbody>tr").each(function(){
            let d = $(this).find('.uid').text();
        
            arlistApp.push(d);
        }); 

        if(arlistApp.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Application can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}

        var arlistTg = []; 
        $("#list-group-tg>tbody>tr").each(function(){
            let d = $(this).find('.uid').text();
        
            arlistTg.push(d);
        }); 

        if(arlistTg.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal Group can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}

        var arlistT = []; 
        $("#table-form-terminal-dnt>tbody>tr").each(function(){
            let d = $(this).find('.uid').text();
         
            arlistT.push(d);
        }); 

        if(arlistT.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}


       
       
        // Show loder
        $('.page-loader').removeClass('hidden');
        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: { 

                'id': $('#download-task-id').val(), 
                'name' : $('#download-task-name').val(),
                'downloadTimeType': $('#download-task-downloadTimeType').val(),
                'installationTimeType': $('#download-task-installation_time_type').val(),
                'installationTime': $('#download-task-installation_time').val(),
                'publishTimeType' : $('#download-task-publish_time_type').val(),
                'downloadTime' : $('#download-task-downloadTime').val(),
                'publishTime' : $('#download-task-publish_time').val(),
                'status': $('input[name="downloadstatus[]"]:checked').val(),
                'installationNotification' :  $('#download-task-installationNotification').val(),
                'applicationIds' :  JSON.stringify(arlistApp),
                'terminalGroupIds' :  JSON.stringify(arlistTg),
                'terminalIds' :  JSON.stringify(arlistT),
                'version' : $('#version-dt').val() 
                
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    //dataTableCity.ajax.reload();
                  
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    window.location.replace(baseUrl +"/download-task");

                    //if($('#download-task-id').val()=="") {
                       
                        //$('#list-group-app').html('');
                        //$('#list-group-tg').html('');
                        //$('#list-group-t').html('');
                        //$('#form-download-task')[0].reset();
                       
                    //}


                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    //resp.responseMessage
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    

    $('#dataTableDownloadTask').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/download-task/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableDownloadTask.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    $(document).on("input", ".numeric", function() {
        this.value = this.value.replace(/\D/g,'');
    });

   // $('#download-task-installation_time').datepicker();

   // $('#download-task-installation_time').datetimepicker({
     //   format: 'Y-m-d H:i'
     //});
    
    //$("#download-task-installation_time").datepicker();
    //$("#download-task-publish_time").datepicker();

    //----------- sub table application ------
   
    $('#dataTableAppListGForm').wrap('<div class="dataTables_scroll" />');
    var dataTableAppListGForm = null;
    if ($('#dataTableAppListGForm').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableAppListGForm =  $('#dataTableAppListGForm').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/application-datatables",
            type: 'GET',
            data:  function(d){

                //d.model = $('#search-model').val();
                //d.vendorName = $('#search-vendor-name').val();
                //d.vendorCountry = $('#search-vendor-country').val();
                
            }
        },
        language: {
          
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "appVersion", name: "appVersion"},
            {data: "name", name: "name"},
            {data : "packageName", name : "packageName"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
	        {
                targets: 4,
                render: function(d,data,row) {
                    let id = d;
                    let v = row.version;
                    return `
                      <input type="checkbox" 
                      name="chdt[]"
                      value="true" lass="custom-control-input checkListdt" 
                      data-id="`+d+`" 
                      data-app_version="`+row.appVersion+`"
                      data-name="`+row.name+`"
                      data-package_name="`+row.packageName+`"
                      data-version="`+v+`">
                    `;
                   
                }
            }
        ]
        });
      
    }
    
    // Terminal group modal tabel
    $('#dataTableTgListGForm').wrap('<div class="dataTables_scroll" />');
    var dataTableTgListGForm = null;
    if ($('#dataTableTgListGForm').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTgListGForm =  $('#dataTableTgListGForm').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-group-datatables",
            type: 'GET',
            data:  function(d){

                //d.model = $('#search-model').val();
                //d.vendorName = $('#search-vendor-name').val();
                //d.vendorCountry = $('#search-vendor-country').val();
                
            }
        },
        language: {
         
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "name", name: "name"},
            {data : "description", name : "description"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
	        {
                targets: 4,
                render: function(d,data,row) {
                    let id = d;
                    let v = row.version;
                   
                    return `
                    <input 
                    type="checkbox" 
                    name="chtg[]" 
                    value="true"  
                    data-id="`+d+`" 
                    data-name="`+row.name+`" 
                    data-description="`+row.description+`"                      
                    data-version="`+v+`"
                    />
                  `;
                }
            }
        ]
        });
      
    }
    // terminal modal tabel
    
    $('#dataTableTerListGForm').wrap('<div class="dataTables_scroll" />');
    var dataTableTerListGForm = null;
    if ($('#dataTableTerListGForm').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTerListGForm =  $('#dataTableTerListGForm').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables",
            type: 'GET',
            data:  function(d){

                //d.model = $('#search-model').val();
                //d.vendorName = $('#search-vendor-name').val();
                //d.vendorCountry = $('#search-vendor-country').val();
                
            }
        },
        language: {
         
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "sn", name: "sn"},
            {data: "modelName", name: "modelName"},
            {data: "merchantName", name : "merchantName"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
	        {
                targets: 4,
                render: function(d,data,row) {
                    let id = d;
                    let v = row.version;
                   
                    return `
                        <input 
                        type="checkbox" 
                        name="chter[]" 
                        value="true"  
                        data-id="`+d+`" 
                        data-model="`+row.modelName+`" 
                        data-merchant="`+row.merchantName+`" 
                        data-version="`+v+`"                     
                        data-sn="`+row.sn+`"
                        />
                        `;
                   
                }
            }
        ]
        });
      
    }
    
    

    $('body').on('click', '#add-app-to-list', function() {
      
        $('#modal-list-app-form').modal('show');
        $('#modal-list-app-form .modal-title').html('List Application');
        dataTableAppListGForm.draw(true);
       
    });

    $('body').on('click', '#btn-app-to-list', function() {

        var arlist = [];
        $("#list-group-app>tbody>tr").each(function(){

            let a = $(this).find('.app_version').text();
            let b = $(this).find('.name').text();
            let c = $(this).find('.package_name').text();
            let d = $(this).find('.uid').text();

			var ob = new Object();
			ob.app_version = a;
			ob.name = b;
            ob.packageName = c;
			ob.uid = d;
			arlist.push(ob);
        }); 
       
        var arlist2 = [];
        var datac = $('[name="chdt[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.app_version = $this.data("app_version");
                    ob.name = $this.data("name");
                    ob.packageName = $this.data("package_name");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    let name = mergedArr[i].name
            let app_version = mergedArr[i].app_version;
			let packageName = mergedArr[i].packageName;
            let uid = mergedArr[i].uid;
			let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-dt"><i class="fa fa-remove"></i></button>`;
			d+=`<tr class="tr-dt">`+
                    `<td><span class="app_version">`+app_version+`</span></td>`+
					`<td><span class="name">`+name+`</span></td>`+
					`<td><span class="package_name">`+packageName+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		}

        $('#list-group-app>tbody').find('tr').remove();  
        $('#list-group-app>tbody').html(d);       
        $('#modal-list-app-form').modal('hide');
		
       
        

    });
    $('body').on('click', '.btn-delete-list-dt', function() {
            $(this).parent().parent().remove();
    });

    
    $('body').on('click', '#add-tg-to-list', function() {
        
        $('#modal-list-tg-form').modal('show');
        $('#modal-list-tg-form .modal-title').html('List Terminal Group');
        dataTableTgListGForm.draw(true);
       
    });

    $('body').on('click', '#btn-tg-to-list', function() {

        
        var arlist = [];
        $("#list-group-tg>tbody>tr").each(function(){

            let a = $(this).find('.name').text();
            let b = $(this).find('.description').text();
            let c = $(this).find('.uid').text();

			var ob = new Object();
			ob.name = a;
			ob.description = b;
			ob.uid = c;
			arlist.push(ob);
        }); 

        var arlist2 = [];
        var datac = $('[name="chtg[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.name = $this.data("name");
                    ob.description = $this.data("description");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    let name = mergedArr[i].name;
			let description = mergedArr[i].description;
            let uid = mergedArr[i].uid;
			let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-tg"><i class="fa fa-remove"></i></button>`;
			d+=`<tr class="tr-dt">`+
					`<td><span class="name">`+name+`</span></td>`+
					`<td><span class="description">`+description+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		}

        $('#list-group-tg>tbody').find('tr').remove();  
        $('#list-group-tg>tbody').html(d);       
		$('#modal-list-tg-form').modal('hide');
		
    });
    $('body').on('click', '.btn-delete-list-tg', function() {
        $(this).parent().parent().remove();
    });

    $('body').on('click', '#add-t-to-list', function() {
        
        $('#modal-list-t-form').modal('show');
        $('#modal-list-t-form .modal-title').html('List Terminal');
        dataTableTerListGForm.draw(true);
       
    });

    $('body').on('click', '#btn-t-to-list', function() {


        var arlist = [];
        $("#table-form-terminal-dnt>tbody>tr").each(function(){

            let a = $(this).find('.sn').text();
            let b = $(this).find('.model').text();
            let c = $(this).find('.merchant').text();
            let d = $(this).find('.uid').text();

			var ob = new Object();
			ob.sn = a;
			ob.model = b;
			ob.merchant = c;
            ob.uid = d;
			arlist.push(ob);
        }); 

        var arlist2 = [];
        var datac = $('[name="chter[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.sn = $this.data("sn");
                    ob.model = $this.data("model");
                    ob.merchant = $this.data("merchant");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
			let sn = mergedArr[i].sn;
			let model = mergedArr[i].model;
			let merchant = mergedArr[i].merchant;
            let uid = mergedArr[i].uid;
			let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-ter"><i class="fa fa-remove"></i></button>`;
			d+=`<tr class="tr-dt">`+
					`<td><span class="sn">`+sn+`</span></td>`+
					`<td><span class="model">`+model+`</span></td>`+
					`<td><span class="merchant">`+merchant+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		}

        $('#table-form-terminal-dnt>tbody').find('tr').remove();  
        $('#table-form-terminal-dnt>tbody').html(d);       
		$('#modal-list-t-form').modal('hide');
		
      
    });
    $('body').on('click', '.btn-delete-list-ter', function() {
        $(this).parent().parent().remove();
    });

    //$('#download-task-publish_time').datetimepicker({
        // showSecond: true,
        // dateFormat: 'yyyy-mm-dd',
        // timeFormat: 'hh:mm:ss',
        // stepHour: 2,
        // stepMinute: 10,
        // stepSecond: 10
        //format: 'Y-m-d H:i'
   // });

    

    $("#search-application-dnt").select2({
        ajax: {
            url: baseUrl+"/download-task-app-auto", //"https://api.github.com/search/repositories",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term, // search term
                page: params.page
              };
            },
            processResults: function (data, params) {
              // parse the results into the format expected by Select2
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data, except to indicate that infinite
              // scrolling can be used
              params.page = params.page || 1;
        
              return {
                results: data.items,
                pagination: {
                  more: (params.page * 30) < data.total_count
                }
              };
            },
            cache: true
          },
          placeholder: 'Search Application',
          minimumInputLength: 1,
          templateResult: formatRepo,
          templateSelection: formatRepoSelection
    });
        
    function formatRepo (repo) {
        if (repo.loading) {
        return repo.text;
        }
        
        var $container = $(
            "<div class='select2-result-repository clearfix'>" +
                //"<div class='select2-result-repository__avatar badge badge-light'>" + repo.name + "</div>" +
                "<h4 class='header-title'>"+repo.name+"</h4>"+
                "<table class='table table-bordered'>"+ 
                    //"<thead class='text-uppercase'><tr><th>Name</th><th>Package Name</th><th>Description</th><th>App Version</th></tr></thead>"+
                    "<tbody>"+
                        //"<tr><td>Name</td><td>"+repo.name+"</td></tr>"+
                        "<tr><td>Package</td><td>"+repo.packageName+"</td></tr>"+
                        "<tr><td>Description</td><td>"+repo.description+"</td></tr>"+
                        "<tr><td>App Version</td><td>"+repo.appVersion+"</td></tr>"+
                    "</tbody>"+
                "</table>"+
            "</div>"
        );

        return $container;
    }
    
    function formatRepoSelection (repo) {
        return repo.name || repo.text;
        //return repo.full_name || repo.text;
    }
   

    $("#search-terminal-dnt").select2({
        ajax: {
            url: baseUrl+"/download-task-terminal-auto",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term, // search term
                page: params.page
              };
            },
            processResults: function (data, params) {
              // parse the results into the format expected by Select2
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data, except to indicate that infinite
              // scrolling can be used
              params.page = params.page || 1;
        
              return {
                results: data.items,
                pagination: {
                  more: (params.page * 30) < data.total_count
                }
              };
            },
            cache: true
          },
          placeholder: 'Search Terminal',
          minimumInputLength: 1,
          templateResult: formatRepo2,
          templateSelection: formatRepoSelection2
    });
        
    function formatRepo2 (repo) {
        if (repo.loading) {
        return repo.text;
        }
        
        var $container = $(
            "<div class='select2-result-repository clearfix'>" +
                //"<div class='select2-result-repository__avatar badge badge-light'>" + repo.name + "</div>" +
                "<h4 class='header-title'>"+repo.sn+"</h4>"+
                "<table class='table table-bordered'>"+ 
                    //"<thead class='text-uppercase'><tr><th>Name</th><th>Package Name</th><th>Description</th><th>App Version</th></tr></thead>"+
                    "<tbody>"+
                        //"<tr><td>Name</td><td>"+repo.name+"</td></tr>"+
                        "<tr><td>Package</td><td>"+repo.modelName+"</td></tr>"+
                        "<tr><td>Description</td><td>"+repo.merchantName+"</td></tr>"+
                        "<tr><td>App Version</td><td>"+repo.profileName+"</td></tr>"+
                    "</tbody>"+
                "</table>"+
            "</div>"
        );

        return $container;
    }
    
    function formatRepoSelection2 (repo) {
        return repo.sn || repo.text;
     
    }

    $("#search-terminalgroup-dnt").select2({
        ajax: {
            url: baseUrl+"/download-task-terminalgroup-auto", 
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term, // search term
                page: params.page
              };
            },
            processResults: function (data, params) {
              // parse the results into the format expected by Select2
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data, except to indicate that infinite
              // scrolling can be used
              params.page = params.page || 1;
        
              return {
                results: data.items,
                pagination: {
                  more: (params.page * 30) < data.total_count
                }
              };
            },
            cache: true
          },
          placeholder: 'Search Terminal Group',
          minimumInputLength: 1,
          templateResult: formatRepo3,
          templateSelection: formatRepoSelection3
    });
        
    function formatRepo3 (repo) {
        if (repo.loading) {
        return repo.text;
        }
        
        var $container = $(
            "<div class='select2-result-repository clearfix'>" +
                //"<div class='select2-result-repository__avatar badge badge-light'>" + repo.name + "</div>" +
                "<h4 class='header-title'>"+repo.name+"</h4>"+
                "<table class='table table-bordered'>"+ 
                    //"<thead class='text-uppercase'><tr><th>Name</th><th>Package Name</th><th>Description</th><th>App Version</th></tr></thead>"+
                    "<tbody>"+
                        //"<tr><td>Name</td><td>"+repo.name+"</td></tr>"+
                        "<tr><td>Package</td><td>"+repo.description+"</td></tr>"+
                        //"<tr><td>Description</td><td>"+repo.merchantName+"</td></tr>"+
                        //"<tr><td>App Version</td><td>"+repo.profileName+"</td></tr>"+
                    "</tbody>"+
                "</table>"+
            "</div>"
        );

        return $container;
    }
    
    function formatRepoSelection3 (repo) {
        return repo.name || repo.text;
     
    }
   
   

})(jQuery);