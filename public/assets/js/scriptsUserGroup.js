(function($) {
    "use strict";

    $('#dataTableUserGroup').wrap('<div class="dataTables_scroll" />');
    var userDataTable = null;
    if ($('#dataTableUserGroup').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';

        userDataTable =  $('#dataTableUserGroup').DataTable({
        //dom: '<"dt-toolbar"<"col-md-6 col-xs-12 hidden-xs"f><"col-md-6 col-xs-12 hidden-xs text-right"<"toolbar">>>rt<"dt-toolbar-footer"<"col-sm-6 col-xs-12"i><"col-sm-6 col-xs-12 hidden-xs"p>><"clear">',
        //dom: '<"dt-toolbar"<"col-dm-6"f><"col-dm-6 text-right"<"toolbar">>>lt<"dt-toolbar-footer"<"col-dm-6"i><"col-dm-6"p>><"clear">',
         dom: 'lrtip',
        //dom: '<"dt-toolbar',
        processing: false,
        serverSide: true,
        //"scrollX": true,
        pageLength: 15,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        "lengthChange": false,
        pagingType: 'full_numbers',       
        ajax: {
            url:  baseUrl+"/user-group-datatables",
            type: 'GET',
            data:  function(d){
                d.cari = $('#search-user-group').val();
            }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", sortable: false, searchable: false},
            // {data: "id", name: "id"},
            {data: "name", name: "name"},
            {data: "description", name: "description"},
            {data: "action", sortable: false, searchable: false, class: "action"}
        ]
        });
        //$("div.toolbar").html("<button id='btn-add-user' class='btn btn-primary pull-right'><i class='fa fa-plus-circle'></i> New User</button>");

    }

    $('#btn-cari-usergroup').click(function() {
        userDataTable.draw(true);
    });
	

    // Add User Group
    $('body').on('click', '#btn-add-user-group', function(){
        $('#modal-user-group').modal('show');
        $('#modal-user-group .modal-title').html('Add New User Group');
        $('#modal-user-group input').val('');
    });

     // Add New Or Update Program
     $('#btn-submit').click(function(){
        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#group-name').val()) {
            $('#group-name').focus();
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Group name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
                //icon : "fa fa-bell swing animated"
            });
            //hasError = true;
            return;
        }

        // Show loder
        $('.page-loader').removeClass('hidden');

        // Set URL
        var url = '/user-group/save';
        if($('#group-id').val()) {
            url = '/user-group/update';
        }

        // Send data
        $.ajax({
            url: baseUrl + url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'groupId': $('#group-id').val(),
                'groupName': $('#group-name').val(),
                'groupDescription': $('#group-description').val()
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    userDataTable.ajax.reload();
                    // Reset Form
                    $('#modal-user input').val('');
                    // Close modal
                    $('#modal-user').modal('hide');
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit user
    $('#dataTableUserGroup').on('click', '.btn-edit', function() {
        $('#modal-user-group').modal('show');
        $('#modal-user-group .modal-title').html('Edit User Group');

        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/user-group/detail/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                $('#group-id').val(resp.id);
                $('#group-name').val(resp.name);
                $('#group-description').val(resp.description);

                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

     // Disabled user group
     $('#dataTableUserGroup').on('click', '.btn-disable', function() {

        if(!confirm('Area you sure want to disable this user group')) {
            return;
        }

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/user-group/disable/'+$(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    userDataTable.ajax.reload();
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
            }
        });


    });

    // Activate user group
    $('#dataTableUserGroup').on('click', '.btn-active', function() {

        if(!confirm('Area you sure want to activate this user group')) {
            return;
        }

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/user-group/activate/'+$(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    userDataTable.ajax.reload();
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
            }
        });
    });

     // Set privileges user group
     $('#dataTableUserGroup').on('click', '.btn-privileges', function() {
        $('#modal-privileges').modal('show');

        // Hide loder
        $('.page-loader').removeClass('hidden');

        $('#priv-group-id').val($(this).data('id'));
        // Get data
        $.ajax({
            url: baseUrl + '/user-group/privileges/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {

                // Reset form
                $('input[type=checkbox]').prop('checked', false);

                // Check privileges menus
                $.each(resp.result, function(i, o) {
                    $('#checkbox-'+o.id).prop('checked', true);
                });

                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    // Set privileges
    $('#btn-submit-privileges').click(function(){
        // Show loder
        $('.page-loader').removeClass('hidden');

        var actions = $('input[name="action[]"]:checked').map(function(){
            return this.value;
        }).get();

        // Send data
        $.ajax({
            url: baseUrl + '/user-group/privileges',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'userGroupId': $('#priv-group-id').val(),
                'menuActions': actions
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Close modal
                    $('#modal-privileges').modal('hide');
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

})(jQuery);