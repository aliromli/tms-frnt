(function($) {
    "use strict";

    $('#dataTableMerchant').wrap('<div class="dataTables_scroll" />');
    var dataTableMerchant = null;
    if ($('#dataTableMerchant').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableMerchant =  $('#dataTableMerchant').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/merchant-datatables",
            type: 'GET',
            data:  function(d){ 
                d.name = $('#search-merchant').val();
                //d.state = $('#search-city-state').val();
                
            }
        },
        language: {
            
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
           {data: "version", name: "version"},
            {data: "name", name: "name"},
            {data: "companyName", name: "companyName"}, 
            {data: "districtName", name: "districtName"},
            {data: "cityName", name: "cityName"},
            {data: "stateName", name: "stateName"},
            {data: "address", name: "address"},
            {data: "zipcode", name: "zipcode"}, 
            {data: "merchantType", name: "merchantType"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
	        {
                targets: 10,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    return `
                    <a href="merchant/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-merchant').click(function() {
        dataTableMerchant.draw(true);
    });

    //$('#div-m-version').css('display','none');

    // Add 
    // $('body').on('click', '#btn-add-merchant', function(){
    //     $('#modal-merchant').modal('show');
    //     $('#modal-merchant .modal-title').html('Add New Merchant');
       
    //     $('#modal-merchant input[type=text],#modal-merchant input[type=hidden],#modal-merchant input[type=password],#modal-merchant input[type=email],#modal-merchant input[type=number]').val('').removeAttr('readonly');
    //     $('#div-m-version').css('display','none');
    // });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-city').val('');
        $('#search-city-state').val('');
        dataTableMerchant.draw(true);
    });

    // Add New City or update
    $('#btn-submit-merchant').click(function(){

        // Update when city id has value
        var url = baseUrl + '/merchant/update';
        var action = "merchant";
        if(!$('#merchant-id').val()) {
            url = baseUrl + '/merchant/save';
            action = "save";
        }

        if($('#merchant-id').val()) {
            if(!$('#version-m').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Merchant can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-m').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#merchant-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Marchant Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-company').focus();
            return;
        }

        if(!$('#merchant-company').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'District can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-company').focus();
            return;
        }
        if(!$('#merchant-address').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'District can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-address').focus();
            return;
        }

        if(!$('#merchant-district').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'District can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-district').focus();
            return;
        }
        if(!$('#merchant-zipcode').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Zipcode can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-zipcode').focus();
            return;
        }

        if(!$('#merchant-merchantType').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Merchant Type can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-merchantType').focus();
            return;
        }


        
        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#merchant-id').val(),
                'name': $('#merchant-name').val(),
                'companyName': $('#merchant-company').val(),
                'address' : $('#merchant-address').val(),
                'districtId' : $('#merchant-district').val(),
                'zipcode' : $('#merchant-zipcode').val(),
                'merchantTypeId' : $('#merchant-merchantType').val(),
                'version': $('#version-m').val(),

            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    //dataTableCity.ajax.reload();
                  
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    if($('#merchant-id').val()=="") {
                       
                        $('#merchant-name').val("");
                        $("#merchant-company").val("");
                        $("#merchant-address").val("");
                        $("#merchant-district").val("");
                        $("#merchant-zipcode").val("");
                        $("#merchant-merchantType").val("");
                       
                    }


                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });


    $('#dataTableMerchant').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/merchant/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableMerchant.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    


})(jQuery);