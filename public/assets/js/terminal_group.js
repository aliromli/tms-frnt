(function($) {
    "use strict";
    $('#dataTableTerminalGroup').wrap('<div class="dataTables_scroll" />');
    var dataTableTerminalGroup = null;
    if ($('#dataTableTerminalGroup').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTerminalGroup =  $('#dataTableTerminalGroup').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-group-datatables",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name-terminal-group').val();
                d.terminalId =  $('#search-terminal-id-g').val();
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "name", name: "name"}, 
            {data: "description", name: "description"}, 
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
            {
                targets: 4,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    let lock = row.locked;
                    return `
                    <a href="terminal-group/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp; 
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>&nbsp;
                  
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-terminal-group').click(function() {
        dataTableTerminalGroup.draw(true);
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-terminal-id-g').val('');
        $('#search-name-terminal-group').val('');
        
        dataTableTerminalGroup.draw(true);
    });

    // Add New  or update
    $('#btn-submit-terminal-group').click(function(){

        // Update when city id has value
        var url = baseUrl + '/terminal-group/update';
        var action = "update";
        if(!$('#terminal-g-id').val()) {
            url = baseUrl + '/terminal-group/save';
            action = "save";
        }

        if($('#terminal-g-id').val()) {
            if(!$('#version-terminal-g').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-terminal-g').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#terminal-g-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-g-name').focus();
            return;
        }

        if(!$('#terminal-g-description').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Description can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-g-description').focus();
            return;
        }

        var arlist = []; 
        $("#table-form-terminal-group>tbody>tr").each(function(){
            let d = $(this).find('.uid').text();
            arlist.push(d);
        }); 

        if(arlist.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}

        
        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "id": $('#terminal-g-id').val(),
                "version": $('#version-terminal-g').val(),
                "name" : $('#terminal-g-name').val(),
                "description" : $('#terminal-g-description').val(),
                "terminalIds" : JSON.stringify(arlist)
                
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    //dataTableCity.ajax.reload();
                  
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    if($('#terminal-g-id').val()=="") {
                       
                        $('#terminal-g-name').val("");
                        $('#terminal-g-description').val("");
                        $('#list-group').html('');
                      
                    }


                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });
    $('#dataTableTerminalGroup').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/terminal-group/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableTerminalGroup.ajax.reload();
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });
    
   
  

    /* list terminal modal */
    $('#dataTableTerminalListG').wrap('<div class="dataTables_scroll" />');
    var dataTableTerminalListG = null;
    if ($('#dataTableTerminalListG').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTerminalListG =  $('#dataTableTerminalListG').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "sn", name: "sn"}, 
            {data: "modelName", name: "modelName"}, 
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
            {
                targets: 4,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    let lock = row.locked;
                    return `
                     <button type="button" class="btn btn-flat btn-success btn-xs btn-select-terminal" data-id="`+d+`" data-version="`+v+`">Select</button>&nbsp;
                  
                    `;
                }
            }
        ]
        });
      
    }

   
    $('#search-terminal-id-g').on('click',function(){
   
        $('#modal-list-terminal').modal('show');
        $('#modal-list-terminal .modal-title').html('List Terminal');
        dataTableTerminalListG.draw(true);

    });
    
    $('#dataTableTerminalListG').on('click', '.btn-select-terminal', function() {
        $("#search-terminal-id-g").val($(this).data('id'));
        $('#modal-list-terminal').modal('hide');
    });

    /* list terminal modal form */
    $('#dataTableTerminalListGForm').wrap('<div class="dataTables_scroll" />');
    var dataTableTerminalListGForm = null;
    if ($('#dataTableTerminalListGForm').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTerminalListGForm =  $('#dataTableTerminalListGForm').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "sn", name: "sn"},
            {data: "modelName", name: "modelName"},
            {data : "merchantName", name : "merchantName"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
            {
                targets: 4,
                render: function(d,data,row) {
                    return `
                       <input 
                       type="checkbox" 
                       name="ch[]" 
                       value="true"  
                       data-id="`+d+`" 
                       data-model="`+row.modelName+`" 
                       data-merchant="`+row.merchantName+`"                      
                       data-sn="`+row.sn+`"
                       />
                     `;
                 }
                // render: function(d,data,row) {
                 
                //     let v = row.version;
                //     return `
                //       <input type="checkbox" name="ch[]" value="true" lass="custom-control-input checkList" data-id="`+d+`" data-version="`+v+`">
                //     `;
                // }
            }
        ]
        });
      
    }

    $('body').on('click', '#add-terminal-to-list', function() {

        $('#modal-list-terminal-form').modal('show');
        $('#modal-list-terminal-form .modal-title').html('List Terminal');
        dataTableTerminalListGForm.draw(true);

    });

    $('body').on('click', '#btn-select-to-list', function() {
      
        var arlist = [];
        $("#table-form-terminal-group>tbody>tr").each(function(){

            let a = $(this).find('.sn').text();
            let b = $(this).find('.model').text();
            let c = $(this).find('.merchant').text();
            let d = $(this).find('.uid').text();

			var ob = new Object();
			ob.sn = a;
			ob.model = b;
			ob.merchant = c;
            ob.uid = d;
			arlist.push(ob);
        }); 

        var arlist2 = [];
        var datac = $('[name="ch[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<=arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.sn = $this.data("sn");
                    ob.model = $this.data("model");
                    ob.merchant = $this.data("merchant");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
			let sn = mergedArr[i].sn;
			let model = mergedArr[i].model;
			let merchant = mergedArr[i].merchant;
            let uid = mergedArr[i].uid;
			let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-t"><i class="fa fa-remove"></i></button>`;
			d+=`<tr class="tr-dt">`+
					`<td><span class="sn">`+sn+`</span></td>`+
					`<td><span class="model">`+model+`</span></td>`+
					`<td><span class="merchant">`+merchant+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		}

        $('#table-form-terminal-group>tbody').find('tr').remove();  
        $('#table-form-terminal-group>tbody').html(d);       
		$('#modal-list-terminal-group-form').modal('hide');

    });
    $('body').on('click', '.btn-delete-list-t', function() {
            $(this).parent().parent().remove();
    });


   
})(jQuery);