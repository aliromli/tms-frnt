<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CountriesController;
use App\Http\Controllers\StatesController;
use App\Http\Controllers\CitiesController;
use App\Http\Controllers\DistrictsController;
use App\Http\Controllers\MerchantController;
use App\Http\Controllers\TenantController;
use App\Http\Controllers\MerchanttypeController;
use App\Http\Controllers\DeviceModelController;
use App\Http\Controllers\DeviceProfileController;
use App\Http\Controllers\TerminalGroupController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\TerminalController;
use App\Http\Controllers\DownloadTaskController;
use App\Http\Controllers\DeleteTaskController;
use App\Http\Controllers\DiagnosticController;
use App\Http\Controllers\AidController;
use App\Http\Controllers\CapkController;
use App\Http\Controllers\PublicKeyController;
use App\Http\Controllers\TleSettingController; 
use App\Http\Controllers\TerminalExtController;
use App\Http\Controllers\AcquirerController;
use App\Http\Controllers\IssuerController;
use App\Http\Controllers\CardController;






/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

use Illuminate\Support\Facades\Session;

Auth::routes();

Route::middleware(['auth','locale'])->group(function () {
    //Route::get('/', 'HomeController@index');
    //Route::get('/home', 'HomeController@index');
    Route::get('/', [HomeController::class, 'index']);
    Route::get('/home', [HomeController::class, 'index']);
    ///Route::get('/login', [HomeController::class, 'index']);
   

     //tenant
     Route::get('/tenant', [TenantController::class, 'index']);
     Route::get('/tenant-datatables', [TenantController::class, 'list']);
     Route::post('/tenant/save', [TenantController::class, 'store']);
     Route::post('/tenant/update', [TenantController::class, 'update']);
     Route::post('/tenant/delete', [TenantController::class, 'delete']);
     Route::get('/tenant/{id}', [TenantController::class, 'show']);

    //country
    Route::get('/country', [CountriesController::class, 'index']);
    Route::get('/country-datatables', [CountriesController::class, 'list']);
    Route::post('/country/save', [CountriesController::class, 'store']);
    Route::post('/country/update', [CountriesController::class, 'update']);
    Route::post('/country/delete', [CountriesController::class, 'delete']);
    Route::get('/country/{id}', [CountriesController::class, 'show']);
	
	//state
    Route::get('/state', [StatesController::class, 'index']);
    Route::get('/state-datatables', [StatesController::class, 'list']);
    Route::post('/state/save', [StatesController::class, 'store']);
    Route::post('/state/update', [StatesController::class, 'update']);
    Route::post('/state/delete', [StatesController::class, 'delete']);
    Route::get('/state/{id}', [StatesController::class, 'show']);

	//city
    Route::get('/city', [CitiesController::class, 'index']);
    Route::get('/city-datatables', [CitiesController::class, 'list']);
    Route::post('/city/save', [CitiesController::class, 'store']);
    Route::post('/city/update', [CitiesController::class, 'update']);
    Route::post('/city/delete', [CitiesController::class, 'delete']);
    Route::get('/city/{id}', [CitiesController::class, 'show']);
	
	//districs district 
    Route::get('/district', [DistrictsController::class, 'index']);
    Route::get('/district-datatables', [DistrictsController::class, 'list']);
    Route::post('/district/save', [DistrictsController::class, 'store']);
    Route::post('/district/update', [DistrictsController::class, 'update']);
    Route::post('/district/delete', [DistrictsController::class, 'delete']);
    Route::get('/district/{id}', [DistrictsController::class, 'show']);


    //merchant
    Route::get('/merchant', [MerchantController::class, 'index']);
    Route::get('/merchant-datatables', [MerchantController::class, 'list']);
    Route::post('/merchant/save', [MerchantController::class, 'store']);
    Route::post('/merchant/update', [MerchantController::class, 'update']);
    Route::post('/merchant/delete', [MerchantController::class, 'delete']);
    Route::get('/merchant/{id}', [MerchantController::class, 'formEdit']);
    Route::get('/merchant-form', [MerchantController::class, 'form']);
  
    //merchant type
    Route::get('/merchanttype', [MerchanttypeController::class, 'index']);
    Route::get('/merchanttype-datatables', [MerchanttypeController::class, 'list']);
    Route::post('/merchanttype/save', [MerchanttypeController::class, 'store']);
    Route::post('/merchanttype/update', [MerchanttypeController::class, 'update']);
    Route::post('/merchanttype/delete', [MerchanttypeController::class, 'delete']);
    Route::get('/merchanttype/{id}', [MerchanttypeController::class, 'show']);

    //device model
    Route::get('/device-model', [DeviceModelController::class, 'index']);
    Route::get('/device-model-datatables', [DeviceModelController::class, 'list']);
    Route::post('/device-model/save', [DeviceModelController::class, 'store']);
    Route::post('/device-model/update', [DeviceModelController::class, 'update']);
    Route::post('/device-model/delete', [DeviceModelController::class, 'delete']);
    Route::get('/device-model/{id}', [DeviceModelController::class, 'formEdit']);
    Route::get('/device-model-form', [DeviceModelController::class, 'form']);
  
	
	//device profile
    Route::get('/device-profile', [DeviceProfileController::class, 'index']);
    Route::get('/device-profile-datatables', [DeviceProfileController::class, 'list']);
    Route::post('/device-profile/save', [DeviceProfileController::class, 'store']);
    Route::post('/device-profile/update', [DeviceProfileController::class, 'update']);
    Route::post('/device-profile/delete', [DeviceProfileController::class, 'delete']);
    Route::get('/device-profile/{id}', [DeviceProfileController::class, 'formEdit']);
	Route::get('/device-profile-form', [DeviceProfileController::class, 'form']);

    //dapplication
    Route::get('/application', [ApplicationController::class, 'index']);
    Route::get('/application-datatables', [ApplicationController::class, 'list']);
    Route::post('/application-auto', [ApplicationController::class, 'autoCompleteDeviceModel']);
    Route::get('/application-form', [ApplicationController::class, 'form']);
    Route::post('/application/save', [ApplicationController::class, 'store']);
    Route::post('/application/update', [ApplicationController::class, 'update']);
    Route::post('/application/delete', [ApplicationController::class, 'delete']);
    Route::get('/application/{id}', [ApplicationController::class, 'formEdit']);
    Route::get('/application-get-apk/{id}', [ApplicationController::class,'getApk']);
    
    

    //terminal 
   
    Route::get('/terminal', [TerminalController::class, 'index']);
    Route::get('/terminal-datatables', [TerminalController::class, 'list']);
    Route::post('/terminal/save', [TerminalController::class, 'store']);
    Route::get('/terminal-form', [TerminalController::class, 'form']);
    Route::get('/terminal/{id}', [TerminalController::class, 'formEdit']);
	Route::post('/terminal/update', [TerminalController::class, 'update']);
    Route::post('/terminal/delete', [TerminalController::class, 'delete']);
    Route::post('/terminal/lockUnlock', [TerminalController::class, 'lockUnlock']);
    Route::post('/terminal/restart', [TerminalController::class, 'restart']);
    Route::post('/terminal-device-model-auto', [TerminalController::class, 'autoCompleteDeviceModel']);
    Route::post('/terminal-merchant-auto', [TerminalController::class, 'autoComplateMerchant']);
    Route::post('/terminal-profile-auto', [TerminalController::class, 'autoComplateProfile']);
   
    //terminal group
    Route::post('/terminal-group-get-terminal-auto', [TerminalGroupController::class, 'autoCompleteTerminal']);
    Route::get('/terminal-group', [TerminalGroupController::class, 'index']);
    Route::get('/terminal-group-datatables', [TerminalGroupController::class, 'list']);
    Route::get('/terminal-group-terminal-datatables', [TerminalGroupController::class, 'listTerminal']);
    Route::post('/terminal-group/save', [TerminalGroupController::class, 'store']);
    Route::get('/terminal-group-form', [TerminalGroupController::class, 'form']);
    Route::get('/terminal-group/{id}', [TerminalGroupController::class, 'formEdit']);
    Route::post('/terminal-group/deleteTerminals', [TerminalGroupController::class, 'deleteTerminals']);
    Route::post('/terminal-group/delete', [TerminalGroupController::class, 'delete']);
    Route::post('/terminal-group/saveTerminal', [TerminalGroupController::class, 'addTerminals']);
    Route::post('/terminal-group/update', [TerminalGroupController::class, 'update']);

    
    //downloadTask
    Route::get('/download-task', [DownloadTaskController::class,'index']);
    Route::get('/download-task-datatables', [DownloadTaskController::class,'list']);
    Route::get('/download-task-form', [DownloadTaskController::class, 'form']);
    Route::post('/download-task/save', [DownloadTaskController::class, 'store']);
    Route::get('/download-task/{id}', [DownloadTaskController::class, 'formEdit']);
    Route::get('/download-task/listTerminal', [DownloadTaskController::class,'listTerminal']);
    Route::get('/download-task/listTerminalGroup', [DownloadTaskController::class,'listGroup']);
    Route::get('/download-task/get',  [DownloadTaskController::class,'show']);
    Route::post('/download-task/update',  [DownloadTaskController::class,'update']);
    Route::post('/download-task/delete',  [DownloadTaskController::class,'delete']);
    Route::post('/download-task/cancel',  [DownloadTaskController::class,'cancel']);
    Route::post('/download-task/history',  [DownloadTaskController::class,'history']);
    Route::post('/download-task/terminalHistory',  [DownloadTaskController::class,'terminalHistory']);
    Route::get('/download-task-app-auto', [DownloadTaskController::class, 'autoCompleteApplication']);
    Route::get('/download-task-terminal-auto', [DownloadTaskController::class, 'autoCompleteTerminal']);
    Route::get('/download-task-terminalgroup-auto', [DownloadTaskController::class, 'autoCompleteTerminalGroup']);
    
    

    //diagnostic
    Route::get('/diagnostic', [DiagnosticController::class, 'index']);
    Route::get('/diagnostic-lastHeartbeat', [DiagnosticController::class, 'lastHeartbeat']);
    Route::get('/diagnostic-lastDiagnostic', [DiagnosticController::class, 'lastDiagnostic']);
    Route::get('/diagnostic-lastheartbeat-data', [DiagnosticController::class, 'lastHeartbeatData']);
    Route::get('/diagnostic-lastDiagnostic-data', [DiagnosticController::class, 'lastDiagnosticData']);
    Route::get('/diagnostic-lastDiagnostic-detail/{sn}', [DiagnosticController::class, 'lastDiagnosticDetail']);
   
    
    //deletetask
    Route::get('/delete-task', [DeleteTaskController::class, 'index']);
    Route::get('/delete-task-datatable', [DeleteTaskController::class, 'list']);
    Route::post('/delete-task/save', [DeleteTaskController::class, 'store']);
    Route::get('/delete-task-form', [DeleteTaskController::class, 'form']);
    Route::get('/delete-task/{id}', [DeleteTaskController::class, 'formEdit']);
    Route::post('/delete-task/update', [DeleteTaskController::class, 'update']);
    Route::post('/delete-task/delete', [DeleteTaskController::class, 'delete']);

	//aid	
	Route::get('/aid-datatable', [AidController::class, 'list']);
	Route::get('/aid', [AidController::class, 'index']);
	Route::post('/aid/save', [AidController::class, 'store']);
	Route::get('/aid-form', [AidController::class, 'form']);
	Route::get('/aid/{id}', [AidController::class, 'formEdit']);
	Route::post('/aid/update', [AidController::class, 'update']);
	Route::post('/aid/delete',  [AidController::class,'delete']);
	
	 /* router capk*/ 
	Route::get('/capk-datatable', [CapkController::class,'list']);
	Route::get('/capk', [CapkController::class,'index']);
	Route::post('/capk/save', [CapkController::class,'store']);
	Route::get('/capk-form', [CapkController::class,'form']);
	Route::get('/capk/{id}', [CapkController::class, 'formEdit']);
	Route::post('/capk/update', [CapkController::class, 'update']);
	Route::post('/capk/delete',  [CapkController::class,'delete']);
	
	 /* router publicKey*/ 
	Route::get('/publicKey-datatable', [PublicKeyController::class,'list']);
	Route::get('/publicKey', [PublicKeyController::class,'index']);
	Route::post('/publicKey/save', [PublicKeyController::class,'store']);
	Route::get('/publicKey-form', [PublicKeyController::class,'form']);
	Route::get('/publicKey/{id}', [PublicKeyController::class, 'formEdit']);
	Route::post('/publicKey/update', [PublicKeyController::class, 'update']);
	Route::post('/publicKey/delete',  [PublicKeyController::class,'delete']);
	
	/* router tleSetting*/ 
	Route::get('/tleSetting-datatable', [TleSettingController::class,'list']);
	Route::get('/tleSetting', [TleSettingController::class,'index']);
	Route::post('/tleSetting/save', [TleSettingController::class,'store']);
	Route::get('/tleSetting-form', [TleSettingController::class,'form']);
	Route::get('/tleSetting/{id}', [TleSettingController::class, 'formEdit']);
	Route::post('/tleSetting/update', [TleSettingController::class, 'update']);
	Route::post('/tleSetting/delete',  [TleSettingController::class,'delete']);
	
	/* router terminalExt*/ 
 	Route::get('/terminalExt-datatable', [TerminalExtController::class,'list']);  //L
	Route::get('/terminalExt', [TerminalExtController::class,'index']);
	Route::post('/terminalExt/save', [TerminalExtController::class,'store']);
	Route::get('/terminalExt-form', [TerminalExtController::class,'form']);
	Route::get('/terminalExt/{id}', [TerminalExtController::class, 'formEdit']);  //L
	Route::post('/terminalExt/update', [TerminalExtController::class, 'update']);  //L
	Route::post('/terminalExt/delete',  [TerminalExtController::class,'delete']);
	
    /* router acquirer*/ 
    Route::get('/acquirer-datatable', [AcquirerController::class,'list']);
	Route::get('/acquirer', [AcquirerController::class,'index']);
	Route::post('/acquirer/save', [AcquirerController::class,'store']);
	Route::get('/acquirer-form', [AcquirerController::class,'form']);
	Route::get('/acquirer/{id}', [AcquirerController::class, 'formEdit']);
	Route::post('/acquirer/update', [AcquirerController::class, 'update']);
	Route::post('/acquirer/delete',  [AcquirerController::class,'delete']);
	
	/* router issuer*/ 
	Route::get('/issuer-datatable', [IssuerController::class,'list']);
	Route::get('/issuer', [IssuerController::class,'index']);
	Route::post('/issuer/save', [IssuerController::class,'store']);
	Route::get('/issuer-form', [IssuerController::class,'form']);
	Route::get('/issuer/{id}', [IssuerController::class, 'formEdit']);
	Route::post('/issuer/update', [IssuerController::class, 'update']);
	Route::post('/issuer/delete',  [IssuerController::class,'delete']);
	Route::post('/issuer/linkUnlink',  [IssuerController::class,'linkUnlink']);//L
	
	/* router card*/ 
	Route::get('/card-datatable', [CardController::class,'list']);
	Route::get('/card', [CardController::class,'index']);
	Route::post('/card/save', [CardController::class,'store']);
	Route::get('/card-form', [CardController::class,'form']);
	Route::get('/card/{id}', [CardController::class, 'formEdit']);
	Route::post('/card/update', [CardController::class, 'update']);
	Route::post('/card/delete',  [CardController::class,'delete']);
	Route::post('/card/linkUnlink',  [CardController::class,'linkUnlink']);//L  
        
   
    // Users
    Route::get('/user', 'UserController@index');
    Route::get('/user/profile', 'UserController@profile');
    Route::get('/user/change-password', 'UserController@changePassword');
    Route::get('/user/setting', 'UserController@setting');
    Route::get('/user/{id}', 'UserController@show');
    Route::get('/user/activate/{id}', 'UserController@activateUser');
    Route::get('/user/inactivate/{id}', 'UserController@inactivateUser');
    Route::get('/user-datatables', 'UserController@datatables');
    Route::get('/user-export', 'UserController@export');
    Route::post('/user-import', 'UserController@import');
    Route::post('/user/save', 'UserController@store');
    Route::post('/user/update', 'UserController@update');
    Route::post('/user/change-password', 'UserController@storeChangePassword');
    Route::post('/user/update-profile', 'UserController@updateProfile');
    Route::post('/user/setting', 'UserController@storeSetting');

    // User Groups
    Route::get('/user-group', 'UserGroupController@index');
    Route::get('/user-group/disable/', 'UserGroupController@disable');
    Route::get('/user-group-datatables', 'UserGroupController@datatables');
    Route::get('/user-group/detail/{id}', 'UserGroupController@show');
    Route::get('/user-group/disable/{id}', 'UserGroupController@disableUserGroup');
    Route::get('/user-group/activate/{id}', 'UserGroupController@activateUserGroup');
    Route::get('/user-group/privileges/{id}', 'UserGroupController@getPrivileges');
    Route::post('/user-group/privileges', 'UserGroupController@storePrivileges');
    Route::post('/user-group/save', 'UserGroupController@store');
    Route::post('/user-group/update', 'UserGroupController@update');

     // Menus
    Route::get('/menu', 'MenuController@index');
    Route::get('/menu/detail/{id}', 'MenuController@show');
    Route::get('/menu/delete/{id}', 'MenuController@destroy');
    Route::post('/menu/save', 'MenuController@store');
    Route::post('/menu/update', 'MenuController@update');
    Route::post('/menu/build', 'MenuController@build');
});



