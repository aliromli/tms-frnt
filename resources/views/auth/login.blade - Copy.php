@extends('auth.app')

@section('title', 'Login')

@section('content')
<div class="pace pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(0%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity">
    </div>
</div>

<div id="main" role="main">
    <!-- MAIN CONTENT -->
    
    <div id="wrapper">
        <div class="row wrapper-row">
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 wrapper-column">
                <div class="wrapper-buble">
                    <div class="bubble1"></div>
                </div>
                
                <div class="wrapper-content">
                    <div class="header">
                        <img src="{{ asset('img/logo.png') }}"  alt="TMS">
                        <div class="header-title"><b>Login TMS System</b></div>
                    </div>

                    <form align="center" action="{{ route('login') }}" id="login-form" class="smart-form client-form" method="post">
                        @csrf

                        <fieldset>
                            <section>
                                <label class="label">Username or Email</label>
                                <label class="input"> 
                                    <i class="icon-append fa fa-envelope"></i>
                                    <input id="email" type="text" class="form-control  {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                </label>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first() }}</strong>
                                </span>
                                @endif
                            </section>
                            <section>
                                <label class="label">Password</label>
                                <label class="input"> 
                                    <i class="icon-append fa fa-lock"></i>
                                    <input id="password" type="password" class="form-control  {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                </label>
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </section>
                            <section>
                                <div class="note pull-right">
                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif
                                </div>
                                <div class="checkbox pull-left">
                                    <label class="" for="remember">
                                        <input class="" value="true" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                                
                            </section>
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary btn-block btn-lg">
                                Sign in
                            </button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
