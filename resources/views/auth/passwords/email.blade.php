@extends('auth.app')

@section('title', 'Reset Password')

@section('content')
<!-- Start video hero -->
<!--<div class="video-hero jquery-background-video-wrapper demo-video-wrapper">
    <video class="jquery-background-video" autoplay muted loop>
        <source src="{{ asset('videos/medical.mp4') }}" type="video/mp4">
    </video>
</div>-->
<!-- End video hero -->

<div class="pace pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(0%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity">
    </div>
</div>
<div id="main" role="main">
    <!-- MAIN CONTENT -->
    
    <div id="wrapper">
        <div class="row wrapper-row">
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 wrapper-column">
                <div class="wrapper-buble">
                    <div class="bubble1"></div>
                </div>
                
                <div class="wrapper-content">
                    <div class="header">
                        <img src="{{ asset('img/logo.png') }}" alt="IHP">
                        <div class="header-title">Reset Password</div>
                    </div>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form align="center" action="{{ route('password.email') }}" id="login-form" class="smart-form client-form" method="post" novalidate="novalidate">
                        @csrf

                        <fieldset>
                            <section>
                                <label class="label">{{ __('E-Mail Address') }}</label>
                                <label class="input"> 
                                    <i class="icon-append fa fa-envelope"></i>
                                    <input id="email" type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                </label>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first() }}</strong>
                                </span>
                                @endif
                            </section>
                            <section>
                                <div class="note pull-left">
                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('login') }}">
                                        Remember password? login here
                                    </a>
                                    @endif
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<!--<script src="{{ asset('js/plugin/jquery-background-video/jquery.background-video.css') }}"></script>
<style>
    .video-hero {
        height: 100vh;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        position: absolute !important;
    }
</style>-->
@endsection

@section('script')
<!--<script src="{{ asset('js/plugin/jquery-background-video/jquery.background-video.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.jquery-background-video').bgVideo({fadeIn: 0, showPausePlay: false});
    });
</script>-->
@endsection