@extends('auth.app')

@section('title', 'Reset Password')

@section('content')
<!-- Start video hero -->
<!--<div class="video-hero jquery-background-video-wrapper demo-video-wrapper">
    <video class="jquery-background-video" autoplay muted loop>
        <source src="{{ asset('videos/medical.mp4') }}" type="video/mp4">
    </video>
</div>-->
<!-- End video hero -->

<div class="pace pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(0%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity">
    </div>
</div>
<div id="main" role="main">
    <!-- MAIN CONTENT -->

    <div id="wrapper">
        <div class="row wrapper-row">
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 wrapper-column">
                <div class="wrapper-buble">
                    <div class="bubble1"></div>
                </div>

                <div class="wrapper-content">
                    <div class="header">
                        <img src="{{ asset('img/logo.png') }}" alt="IHP">
                        <div class="header-title">Reset Password</div>
                    </div>

                    <form align="center" action="{{ route('password.update') }}" id="login-form" class="smart-form client-form" method="post">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <fieldset>
                            <section>
                                <label class="label">{{ __('E-Mail Address') }}</label>
                                <label class="input">
                                    <i class="icon-append fa fa-envelope"></i>
                                    <input readonly="" id="email" type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email">
                                </label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </section>
                            <section>
                                <label class="label">{{ __('Password') }}</label>
                                <label class="input">
                                    <i class="icon-append fa fa-lock"></i>
                                    <input id="password" type="password" class="form-control  @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" autofocus="">
                                </label>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </section>
                            <section>
                                <label class="label">{{ __('Confirm Password') }}</label>
                                <label class="input">
                                    <i class="icon-append fa fa-lock"></i>
                                    <input id="password-confirm" type="password" class="form-control " name="password_confirmation" required autocomplete="new-password">
                                </label>
                            </section>
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __('Reset Password') }}
                            </button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<!--<script src="{{ asset('js/plugin/jquery-background-video/jquery.background-video.css') }}"></script>
<style>
    .video-hero {
        height: 100vh;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        position: absolute !important;
    }
</style>-->
@endsection

@section('script')
<!--<script src="{{ asset('js/plugin/jquery-background-video/jquery.background-video.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.jquery-background-video').bgVideo({fadeIn: 0, showPausePlay: false});
    });
</script>-->
@endsection
