@extends('layouts.app')
@section('title', 'TleSetting')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">TleSetting</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/tleSetting')}}">TleSetting</a></li>
                    <li><span>Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>

<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">{{ $edit=='no' ? 'Add TleSetting' : 'Edit TleSetting ' }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="tl-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label col-md-3">TleId</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-tleId" value="{{$data ? $data[0]['tleId'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">TleEftSec</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-tleEftSec" value="{{$data? $data[0]['tleEftSec'] : ''}}"></div>
                    </div>		
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">AcquirerId</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-acquirerId" value="{{$data? $data[0]['acquirerId'] : ''}}"></div>
                    </div>	
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">LtmkAid</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-ltmkAid" value="{{$data? $data[0]['ltmkAid'] : ''}}"></div>
                    </div>
                    
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">VendorId</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-vendorId" value="{{$data? $data[0]['vendorId'] : ''}}"></div>
                    </div>
                   
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">TleVer</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-xs" id="tl-tleVer" value="{{$data? $data[0]['tleVer'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">KmsSecureNii</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-kmsSecureNii" value="{{$data? $data[0]['kmsSecureNii'] : ''}}"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">EdcSecureNii</label>
                        <div class="col-md-8"><input type='date' class="form-control input-xs" id="tl-edcSecureNii" value="{{$data? $data[0]['edcSecureNii'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">CapkExponent</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-capkExponent" value="{{$data? $data[0]['capkExponent'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">CapkLength</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="tl-capkLength" value="{{$data? $data[0]['capkLength'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">AidLength</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="tl-aidLength" value="{{$data? $data[0]['aidLength'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField1</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField1" value="{{$data? $data[0]['encryptedField1'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField2</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField2" value="{{$data? $data[0]['encryptedField2'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField3</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField3" value="{{$data? $data[0]['encryptedField3'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField4</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField4" value="{{$data? $data[0]['encryptedField4'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField5</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField5" value="{{$data? $data[0]['encryptedField5'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField6</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField6" value="{{$data? $data[0]['encryptedField6'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField7</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField7" value="{{$data? $data[0]['encryptedField7'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField8</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField8" value="{{$data? $data[0]['encryptedField8'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField9</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField9" value="{{$data? $data[0]['encryptedField9'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">EncryptedField10</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tl-encryptedField10" value="{{$data? $data[0]['encryptedField10'] : ''}}"></div>
                    </div>
					
					<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-tl" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-tl"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <a class="btn btn-flat btn-secondary" href="{{url('/tleSetting')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/tleSetting.js') }}"></script>

@endsection
