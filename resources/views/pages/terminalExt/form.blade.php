@extends('layouts.app')
@section('title', 'TerminalExt')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">TerminalExt</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/terminalExt')}}">TerminalExt</a></li>
                    <li><span>Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>

<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">{{ $edit=='no' ? 'Add TerminalExt' : 'Edit TerminalExt ' }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="te-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label col-md-3">Tid</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-tid" value="{{$data ? $data[0]['tid'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Mid</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-mid" value="{{$data? $data[0]['mid'] : ''}}"></div>
                    </div>		
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">MerchantName1</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-merchantName1" value="{{$data? $data[0]['merchantName1'] : ''}}"></div>
                    </div>	
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">MerchantName2</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-merchantName2" value="{{$data? $data[0]['merchantName2'] : ''}}"></div>
                    </div>	
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">MerchantName3</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-merchantName3" value="{{$data? $data[0]['merchantName3'] : ''}}"></div>
                    </div>	
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Merchant Password</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-merchant_password" value="{{$data? $data[0]['merchant_password'] : ''}}"></div>
                    </div>
                    
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Admin password</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-admin_password" value="{{$data? $data[0]['admin_password'] : ''}}"></div>
                    </div>
                   
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">CallCenter1</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-xs" id="te-callCenter1" value="{{$data? $data[0]['callCenter1'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">CallCenter2</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-callCenter2" value="{{$data? $data[0]['callCenter2'] : ''}}"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Settlement Max Trx Count</label>
                        <div class="col-md-8"><input type='number' class="form-control input-xs" id="te-settlementMaxTrxCount" value="{{$data? $data[0]['settlementMaxTrxCount'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Settlement Warning Trx Count</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="te-settlementWarningTrxCount" value="{{$data? $data[0]['settlementWarningTrxCount'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Settlement Password</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-settlementPassword" value="{{$data? $data[0]['settlementPassword'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Void Password</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-voidPassword" value="{{$data? $data[0]['voidPassword'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Brizzi Discount Percentage</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-brizziDiscountPercentage" value="{{$data? $data[0]['brizziDiscountPercentage'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Brizzi Discount Amount</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-brizziDiscountAmount" value="{{$data? $data[0]['brizziDiscountAmount'] : ''}}"></div>
                    </div>
				
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FallbackEnabled</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['fallbackEnabled'])=='true' ? 'checked' : '' : ''}}  name="fallbackEnabled[]" value="true" id="dp-fallbackEnabled-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-fallbackEnabled-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['fallbackEnabled'])=='false' ? 'checked' : '' : ''}} name="fallbackEnabled[]" value="false"  id="dp-fallbackEnabled-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-fallbackEnabled-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
				
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FeatureSale</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSale'])=='true' ? 'checked' : '' : ''}}  name="featureSale[]" value="true" id="dp-featureSale-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSale-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSale'])=='false' ? 'checked' : '' : ''}} name="featureSale[]" value="false"  id="dp-featureSale-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSale-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FeatureInstallment</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureInstallment'])=='true' ? 'checked' : '' : ''}}  name="featureInstallment[]" value="true" id="dp-featureInstallment-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureInstallment-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureInstallment'])=='false' ? 'checked' : '' : ''}} name="featureInstallment[]" value="false"  id="dp-featureInstallment-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureInstallment-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Feature Card Cerification</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-featureCardCerification" value="{{$data? $data[0]['featureCardCerification'] : ''}}"></div>
                    </div>
					
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FeatureSaleRedemption</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSaleRedemption'])=='true' ? 'checked' : '' : ''}}  name="featureSaleRedemption[]" value="true" id="dp-featureSaleRedemption-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSaleRedemption-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSaleRedemption'])=='false' ? 'checked' : '' : ''}} name="featureSaleRedemption[]" value="false"  id="dp-featureSaleRedemption-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSaleRedemption-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FeatureManualKeyIn</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureManualKeyIn'])=='true' ? 'checked' : '' : ''}}  name="featureManualKeyIn[]" value="true" id="dp-featureManualKeyIn-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureManualKeyIn-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureManualKeyIn'])=='false' ? 'checked' : '' : ''}} name="featureManualKeyIn[]" value="false"  id="dp-featureManualKeyIn-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureManualKeyIn-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FeatureSaleCompletion</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSaleCompletion'])=='true' ? 'checked' : '' : ''}}  name="featureSaleCompletion[]" value="true" id="dp-featureSaleCompletion-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSaleCompletion-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSaleCompletion'])=='false' ? 'checked' : '' : ''}} name="featureSaleCompletion[]" value="false"  id="dp-featureSaleCompletion-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSaleCompletion-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FeatureSaleTip</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSaleTip'])=='true' ? 'checked' : '' : ''}}  name="featureSaleTip[]" value="true" id="dp-featureSaleTip-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSaleTip-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSaleTip'])=='false' ? 'checked' : '' : ''}} name="featureSaleTip[]" value="false"  id="dp-featureSaleTip-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSaleTip-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FeatureSaleFareNonFare</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSaleFareNonFare'])=='true' ? 'checked' : '' : ''}}  name="featureSaleFareNonFare[]" value="true" id="dp-featureSaleFareNonFare-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSaleFareNonFare-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureSaleFareNonFare'])=='false' ? 'checked' : '' : ''}} name="featureSaleFareNonFare[]" value="false"  id="dp-featureSaleFareNonFare-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureSaleFareNonFare-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FeatureQris</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureQris'])=='true' ? 'checked' : '' : ''}}  name="featureQris[]" value="true" id="dp-featureQris-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureQris-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureQris'])=='false' ? 'checked' : '' : ''}} name="featureQris[]" value="false"  id="dp-featureQris-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureQris-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">FeatureContactless</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureContactless'])=='true' ? 'checked' : '' : ''}}  name="featureContactless[]" value="true" id="dp-featureContactless-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureContactless-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['featureContactless'])=='false' ? 'checked' : '' : ''}} name="featureContactless[]" value="false"  id="dp-featureContactless-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-featureContactless-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">ReprintOnlineRetry</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="te-reprintOnlineRetry" value="{{$data? $data[0]['reprintOnlineRetry'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">QrisCountDown</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="te-qrisCountDown" value="{{$data? $data[0]['qrisCountDown'] : ''}}"></div>
                    </div>
					
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">RandomPinKeypad</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['randomPinKeypad'])=='true' ? 'checked' : '' : ''}}  name="randomPinKeypad[]" value="true" id="dp-randomPinKeypad-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-randomPinKeypad-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['randomPinKeypad'])=='false' ? 'checked' : '' : ''}} name="randomPinKeypad[]" value="false"  id="dp-randomPinKeypad-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-randomPinKeypad-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">BeepPinKeypad</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['beepPinKeypad'])=='true' ? 'checked' : '' : ''}}  name="beepPinKeypad[]" value="true" id="dp-beepPinKeypad-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-beepPinKeypad-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['beepPinKeypad'])=='false' ? 'checked' : '' : ''}} name="beepPinKeypad[]" value="false"  id="dp-beepPinKeypad-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-beepPinKeypad-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">NextLogon</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="te-nextLogon" value="{{$data? $data[0]['nextLogon'] : ''}}"></div>
                    </div>
					
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">AutoLogon</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['autoLogon'])=='true' ? 'checked' : '' : ''}}  name="autoLogon[]" value="true" id="dp-autoLogon-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-autoLogon-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['autoLogon'])=='false' ? 'checked' : '' : ''}} name="autoLogon[]" value="false"  id="dp-autoLogon-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-autoLogon-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">PushLogon</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="te-pushLogon" value="{{$data? $data[0]['pushLogon'] : ''}}"></div>
                    </div>
					
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Host Report</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['hostReport'])=='true' ? 'checked' : '' : ''}}  name="hostReport[]" value="true" id="dp-hostReport-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-hostReport-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['hostReport'])=='false' ? 'checked' : '' : ''}} name="hostReport[]" value="false"  id="dp-hostReport-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-hostReport-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">HostLogging</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['hostLogging'])=='true' ? 'checked' : '' : ''}}  name="hostLogging[]" value="true" id="dp-hostLogging-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-hostLogging-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['hostLogging'])=='false' ? 'checked' : '' : ''}} name="hostLogging[]" value="false"  id="dp-hostLogging-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-hostLogging-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">ImportDefault</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['importDefault'])=='true' ? 'checked' : '' : ''}}  name="importDefault[]" value="true" id="dp-importDefault-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-importDefault-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['importDefault'])=='false' ? 'checked' : '' : ''}} name="importDefault[]" value="false"  id="dp-importDefault-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-importDefault-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-te" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-te"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <a class="btn btn-flat btn-secondary" href="{{url('/terminalExt')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/terminalExt.js') }}"></script>

@endsection
