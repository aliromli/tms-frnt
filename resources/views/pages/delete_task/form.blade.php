@extends('layouts.app')
@section('title', 'Delete Task')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Delete Task</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/merchant')}}">Delete Task</a></li>
                    <li><span>Delete Task Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
					<form class="form-horizontal" id="form-delete-task">
						<div class="modal-header bg-dark no-border">
							<h4 class="modal-title">New Delete Task</h4>
						</div>
						<div class="modal-body">
							<div class="form-group row">
								<input type="hidden" class="form-control input-xs" id="delete-task-id" value="{{$data ? $data[0]['id'] : ''}}">
								<label for="" class="control-label col-md-3">Name</label>
								<div class="col-md-8">
									<input type="text" class="form-control input-xs" id="delete-task-name" value="{{$data ? $data[0]['name'] : ''}}"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="" class="control-label col-md-3">Delete Time</label>
								<div class="col-md-8">
									<input type="datetime-local" step=1 class="form-control input-xs" id="delete-task-time" value="{{$data? $data[0]['deleteTime'] : ''}}"/>
								</div>
							</div>
							<div class="form-group row">
								<label for="" class="control-label col-md-3">Terminal</label>
								<div class="cardx col-md-8">
									<div class="xcard-body">
										<button type="button" class="btn btn-flat btn-primary btn-xs" id="add-terminals-to-list"> Add</button>
										<table class="table mt-2" id="table-form-terminal">
											<thead>
												<tr>
													<th>SN</th>
													<th>Model</th>
													<th>Merchant</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody> 
										
											<?php
												//    if($edit=="ya")
												//    {
												// 	   $df = "";
												// 	   foreach($data[0]["terminal"] as $dk)
												// 	   {
												// 		    $act = '<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-x2"><i class="fa fa-remove"></i></button>';
												// 			$df.='<tr class="tr-dt">
												// 					<td><span class="sn">'.$dk['name'].'</span></td>
												// 					<td><span class="model">'.$dk['packageName'].'</span></td>
												// 					<td><span class="merchant">'.$dk['appVersion'].'</span></span><span class="uid" style="display:none;">'.$dk['uid'].'</span></td>
												// 					<td>'.$act.'</td>
												// 				</tr>';
												// 	   }
												// 	   echo $df;
												//    }
												  ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="form-group row">
								<label for="" class="control-label col-md-3">Application</label>
								<div class="cardx col-md-8">
									<div class="xcard-body">
										<button type="button" class="btn btn-flat btn-primary btn-xs" id="add-app"> Add</button>
										<table class="table mt-2" id="table-form-app">
											<thead>
												<tr>
													<th>AppName</th>
													<th>PackageName</th>
													<th>AppVersion</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody> 
												  
												  <?php
												   if($edit=="ya")
												   {
													   $df = "";
													   foreach($data[0]["applications"] as $dk)
													   {
														    $act = '<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-x2"><i class="fa fa-remove"></i></button>';
															$df.='<tr class="tr-d">
																	<td><span class="appn">'.$dk['name'].'</span></td>
																	<td><span class="pack">'.$dk['packageName'].'</span></td>
																	<td><span class="ver">'.$dk['appVersion'].'</span></td>
																	<td>'.$act.'</td>
																</tr>';
													   }
													   echo $df;
												   }
												  ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="form-group row" id="div-version-dtask" style="{{ $edit=='no' ? 'display:none;':''}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8">
									<input readOnly type="text" class="form-control input-xs" id="version-app-dtask" value="{{$data? $data[0]['version'] : ''}}
"/>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-flat btn-primary" id="btn-submit-delete-task"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
							<a class="btn btn-flat btn-secondary" href="{{url('/delete-task')}}">@lang('general.back')</a>
						</div>
					</form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('modal')
    @include('pages.delete_task.terminal_list_modal')
    @include('pages.delete_task.app_form')
@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/delete_task.js') }}"></script>
@endsection
