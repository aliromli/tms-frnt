@extends('layouts.app')
@section('title', 'Delete Task')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


#dataTableDeleteTask {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Delete Task</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><span>Delete Task</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <div class="box-header no-border text-right filter" style="margin-bottom:20px;"> 
                    <a class="btn btn-flat btn-secondary clear-search" href="#">Clear Search</a> &nbsp;
                        <a class="btn btn-flat btn-secondary tugel" href="#"><i class="fa fa-chevron-down sign"></i> <span class="filter-title">Show</span><span class="filter-title hidden">Hide</span> Filter <i class="fa fa-filter"></i></a> &nbsp;
                        <a class="btn btn-flat btn-primary" href="{{url('/delete-task-form')}}"><i class="fa fa-plus-circle"></i> New Delete Task</a> &nbsp;
                    </div>
                    <div class="form-filter">
                        <form class="form-horizontal">
                                <div class="form-group row" xstyle="margin-left:-28;">
                                   <div class="col-md-3 mt-3">
                                       <input type="text" id="search-name-dt" class="form-control input-xs" placeholder="Name...">
                                   </div> 
                                   <div class="col-md-3 mt-3">
                                      <input type="text" id="search-pn-dt" class="form-control input-xs" placeholder="Package Name ...">
                                   </div>
                                   <div class="col-md-2 mt-3">
                                       <input type="text" id="search-sn-dt" class="form-control input-xs" placeholder="SN ...">
                                   </div>
                                   <div class="col-md-2 mt-3">
                                       <input type="text" id="search-appname-dt" class="form-control input-xs" placeholder="App Name ...">
                                   </div>
                               </div>
                               <div class="form-group row" xstyle="margin-left:-28;">
                                   <div class="col-md-10 mt-3">
                                      <select class="form-control" id="search-terminal-dt" name="terminal[]" multiple="multiple"></select>
                                   </div>
                                   <div class="col-md-10 mt-3">
                                       <select class="form-control" id="search-terminalgroup-dt" name="terminalgroup[]" multiple="multiple"></select>
                                   </div>
                                   <div class="col-md-2 mt-3">
                                       <button type="button" id="btn-cari-deltask" class="btn btn-flat btn-secondary btn-cari"> Cari</button>
                                   </div>
                               </div>

                        </form>
                    </div>
                    <div class="data-tables">
                        <table id="dataTableDeleteTask" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>Version</th>
                                    <th>Name</th>
                                    <th>Delete Time</th>
                                    <th>Status</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/delete_task.js') }}"></script>
@endsection
