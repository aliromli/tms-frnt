@extends('layouts.app')
@section('title', 'Aid')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">AID</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/aid')}}">Aid</a></li>
                    <li><span>Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">{{ $edit=='no' ? 'Add Aid' : 'Edit Aid ' }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="aid-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label col-md-3">Name</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-name" value="{{$data ? $data[0]['name'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Txn Type</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-txnType" value="{{$data? $data[0]['txnType'] : ''}}"></div>
                    </div>		
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Aid</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-aid" value="{{$data? $data[0]['aid'] : ''}}"></div>
                    </div>	
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Aid version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-aidVersion" value="{{$data? $data[0]['aidVersion'] : ''}}"></div>
                    </div>
                    
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Tac Default</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-tacDefault" value="{{$data? $data[0]['tacDefault'] : ''}}"></div>
                    </div>
                   
                   
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Tac Denial</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-xs" id="aid-tacDenial" value="{{$data? $data[0]['tacDenial'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Tac Online</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-tacOnline" value="{{$data? $data[0]['tacOnline'] : ''}}"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Threshold</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-threshold" value="{{$data? $data[0]['threshold'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Target Percentage</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-targetPercentage" value="{{$data? $data[0]['targetPercentage'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Max Target Percentage</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-maxTargetPercentage" value="{{$data? $data[0]['maxTargetPercentage'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Ddol</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-ddol" value="{{$data? $data[0]['ddol'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Tdol</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-tdol" value="{{$data? $data[0]['tdol'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Floor Limit</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-floorLimit" value="{{$data? $data[0]['floorLimit'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">App Select</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-appSelect" value="{{$data? $data[0]['appSelect'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Aid Priority</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-aidPriority" value="{{$data? $data[0]['aidPriority'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">TrxType9C</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-trxType9C" value="{{$data? $data[0]['trxType9C'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Category Code</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-categoryCode" value="{{$data? $data[0]['categoryCode'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">ClKernel ToUse</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-clKernelToUse" value="{{$data? $data[0]['clKernelToUse'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">ClOptions</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-clOptions" value="{{$data? $data[0]['clOptions'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">ClTrxLimit</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-clTrxLimit" value="{{$data? $data[0]['clTrxLimit'] : ''}}"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">ClCVMLimit</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-clCVMLimit" value="{{$data? $data[0]['clCVMLimit'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">ClFloorLimit</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-clFloorLimit" value="{{$data? $data[0]['clFloorLimit'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Remark</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-remark" value="{{$data? $data[0]['remark'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Emv Conf TerminalCapability</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-emvConfTerminalCapability" value="{{$data? $data[0]['emvConfTerminalCapability'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Additional Terminal Capability</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-additionalTerminalCapability" value="{{$data? $data[0]['additionalTerminalCapability'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">DataTtq</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="aid-dataTtq" value="{{$data? $data[0]['dataTtq'] : ''}}"></div>
                    </div>
					<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-aid" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-aid"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <a class="btn btn-flat btn-secondary" href="{{url('/aid')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/aid.js') }}"></script>

@endsection
