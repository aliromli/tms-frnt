@extends('layouts.app')
@section('title', 'Merchant')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Merchant</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/merchant')}}">Merchant</a></li>
                    <li><span>Merchant Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" id="form-merchant">
                        <div class="modal-header bg-dark no-border">
                            <h4 class="modal-title">New Merchant</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <input type="hidden" class="form-control input-xs" id="merchant-id" value="{{$data ? $data['id'] : ''}}">
                                <label for="" class="control-label col-md-3">@lang('merchant.name')</label>
                                <div class="col-md-8"><input type="text" class="form-control input-xs" id="merchant-name" value="{{$data ? $data['name'] : ''}}"></div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="control-label col-md-3">@lang('merchant.company')</label>
                                <div class="col-md-8"><input type="text" class="form-control input-xs" id="merchant-company" value="{{$data? $data['companyName'] : ''}}"></div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="control-label col-md-3">@lang('merchant.address')</label>
                                <div class="col-md-8"><input type="text" class="form-control input-xs" id="merchant-address" value="{{$data? $data['address'] : ''}}"></div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="control-label col-md-3">@lang('merchant.district')</label>
                                <div class="col-md-8">
                                    <select class="form-control" id="merchant-district">
                                        @if($edit=='ya')
                                            @foreach($district as $c)
                                                @if($data['district']['id'] == $c['id'])
                                                    <option value="{{ $c['id'] }}" selected>{{ $c['name'] }}</option>
                                                @else
                                                    <option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
                                                @endif
                                            @endforeach
                                            <option value="">&raquo; @lang('general.select') @lang('merchant.district')</option>
                                        @else
                                            <option value="">&raquo; @lang('general.select') @lang('merchant.district')</option>
                                            @foreach($district as $c)
                                            <option value="{{ $c['id'] }}" selected>{{ $c['name'] }}</option>
                                            @endforeach
                                        @endif
                                        </select>
                                </div>
                            </div>
                               
                            <div class="form-group row">
                                <label for="" class="control-label col-md-3">@lang('merchant.zipcode')</label>
                                <div class="col-md-8"><input type="text" class="form-control input-xs" id="merchant-zipcode" value="{{$data? $data['zipcode'] : ''}}"></div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="control-label col-md-3">@lang('merchant.merchantType')</label>
                                <div class="col-md-8">
                                    <select class="form-control" id="merchant-merchantType">
                                       @if($edit=='ya')
                                            @foreach($merchantType as $c)
                                                
                                                @if($data['merchantType']['id'] == $c['id'])
                                                    
                                                    <option value="{{ $c['id'] }}" selected>{{ $c['name'] }}</option>
                                                @else
                                                    
                                                    <option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
                                                @endif
                                            @endforeach
                                            <option value="">&raquo; @lang('general.select') @lang('merchant.name')</option>
                                        @else
                                            <option value="">&raquo; @lang('general.select') @lang('merchant.name')</option>
                                            @foreach($merchantType as $c)
                                            <option value="{{ $c['id'] }}" selected>{{ $c['name'] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                           
                            <div class="form-group row" id="div-m-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                                <label for="" class="control-label col-md-3">Version</label>
                                <div class="col-md-8"><input readOnly type="text" class="form-control input-xs" id="version-m" value="{{$data? $data['version'] : ''}}"></div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-flat btn-primary" id="btn-submit-merchant"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                            <a class="btn btn-flat btn-secondary" href="{{url('/merchant')}}">@lang('general.back')</a>
                        </div>
                    </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/merchant.js') }}"></script>

@endsection
