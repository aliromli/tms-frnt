@extends('layouts.app')
@section('title', 'Issuer')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Issuer</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/issuer')}}">Issuer</a></li>
                    <li><span>Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">               
					<form class="form-horizontal" id="form-dp">
						<div class="modal-header bg-dark no-border">
							<h4 class="modal-title">{{ $edit=='no' ? 'Add Issuer' : 'Edit Issuer ' }}</h4>
						</div>
						<div class="modal-body">
							<div class="form-group row">
								<input type="hidden" class="form-control input-xs" id="is-id" value="{{$data ? $data[0]['id'] : ''}}">
								<label for="" class="control-label col-md-3">Name</label>
								<div class="col-md-8"><input type="text" class="form-control input-xs" id="is-name" value="{{$data ? $data[0]['name'] : ''}}"></div>
							</div>
							<div class="form-group row">
								<label for="" class="control-label col-md-3">Issuer Id</label>
								<div class="col-md-8"><input type="text" class="form-control input-xs" id="is-issuerId" value="{{$data? $data[0]['issuerId'] : ''}}"></div>
							</div>		
							
							<div class="form-group row">
								<label for="" class="control-label col-md-3">OnUs</label>
								<div class="col-md-8">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" {{$data? json_encode($data[0]['onUs'])=='true' ? 'checked' : '' : ''}}  name="OnUs[]" value="true" id="is-OnUs-true"  class="custom-control-input">
										<label class="custom-control-label" for="is-OnUs-true">TRUE</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" {{$data? json_encode($data[0]['onUs'])=='false' ? 'checked' : '' : ''}} name="OnUs[]" value="false"  id="is-OnUs-false"  class="custom-control-input">
										<label class="custom-control-label" for="is-OnUs-false">FALSE</label>
									</div>
								</div>
							</div>
							<div class="form-group row">
								<label for="" class="control-label col-md-3">AcquirerId</label>
								<div class="col-md-8"><input type="text" class="form-control input-xs" id="is-acquirerId" value="{{$data? $data[0]['acquirerId'] : ''}}"></div>
							</div>
							
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':''}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-is" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-flat btn-primary" id="btn-submit-is"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
							<a class="btn btn-flat btn-secondary" href="{{url('/issuer')}}">@lang('general.back')</a>
						</div>
					</form>
                 
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/issuer.js') }}"></script>

@endsection
