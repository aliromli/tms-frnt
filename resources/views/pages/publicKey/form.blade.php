@extends('layouts.app')
@section('title', 'Public Key')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">PublicKey</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/publicKey')}}">PublicKey</a></li>
                    <li><span>Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">{{ $edit=='no' ? 'Add PublicKey' : 'Edit PublicKey ' }}</h4>
                </div>
                <div class="modal-body">
                   	
					<div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="publicKey-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label col-md-3">Name</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="publicKey-name" value="{{$data ? $data[0]['name'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">IDX</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="publicKey-idx" value="{{$data? $data[0]['idx'] : ''}}"></div>
                    </div>		
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">RID</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="publicKey-rid" value="{{$data? $data[0]['rid'] : ''}}"></div>
                    </div>	
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Modulus</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="publicKey-modulus" value="{{$data? $data[0]['modulus'] : ''}}"></div>
                    </div>
                    
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Exponent</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="publicKey-exponent" value="{{$data? $data[0]['exponent'] : ''}}"></div>
                    </div>
                   
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Algo</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-xs" id="publicKey-algo" value="{{$data? $data[0]['algo'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Hash</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="publicKey-hash" value="{{$data? $data[0]['hash'] : ''}}"></div>
                    </div>
					<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-publicKey" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-publicKey"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <a class="btn btn-flat btn-secondary" href="{{url('/publicKey')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/publicKey.js') }}"></script>

@endsection
