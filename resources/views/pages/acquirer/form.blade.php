@extends('layouts.app')
@section('title', 'Acquirer')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Acquirer</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/acquirer')}}">Acquirer</a></li>
                    <li><span>Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
	"" : "required",
            "" : "DEBIT",
            "" : "max",
            "" : "IDC", 
            "" : "GPC",
            "" : "22",
            "" : "22",
            "" : "AI",
            "" : "www.tess.com",
            "" : "2345",
            "" : 1,
            "" :  "467091b7-fd14-45f2-8f3a-74d960838127",
            "" : "1000",
            "" : "1001",
            "" : "1234",
            "" : "123456",
            "" : "090",
            "" : "900",
            "" : 1,
            "" : 1,
            "" : 1,
            "" : 1,
            "" : "57976052-5612-4dc8-b467-5fa516bca6b1"
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">{{ $edit=='no' ? 'Add Acquirer' : 'Edit Acquirer ' }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="ac-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label col-md-3">Name</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-name" value="{{$data ? $data[0]['name'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Type</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-type" value="{{$data? $data[0]['type'] : ''}}"></div>
                    </div>		
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Description</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-description" value="{{$data? $data[0]['description'] : ''}}"></div>
                    </div>	
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">HostId</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-hostId" value="{{$data? $data[0]['hostId'] : ''}}"></div>
                    </div>
                    
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Settlement HostId</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-settlementHostId" value="{{$data? $data[0]['settlementHostId'] : ''}}"></div>
                    </div>
                   
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Number Of Print</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-xs" id="ac-numberOfPrint" value="{{$data? $data[0]['numberOfPrint'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Resp Timeout</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-respTimeout" value="{{$data? $data[0]['respTimeout'] : ''}}"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">AcquirerId</label>
                        <div class="col-md-8"><input type='date' class="form-control input-xs" id="ac-acquirerId" value="{{$data? $data[0]['acquirerId'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Host Dest Addr</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-hostDestAddr" value="{{$data? $data[0]['hostDestAddr'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Host Dest Port</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="ac-hostDestPort" value="{{$data? $data[0]['hostDestPort'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Tle Acquirer</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="ac-tleAcquirer" value="{{$data? $data[0]['tleAcquirer'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">TleSettingId</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-tleSettingId" value="{{$data? $data[0]['tleSettingId'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Master Key Location</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-masterKeyLocation" value="{{$data? $data[0]['masterKeyLocation'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">MasterKey</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-masterKey" value="{{$data? $data[0]['masterKey'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">WorkingKey</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-workingKey" value="{{$data? $data[0]['workingKey'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Batch Number</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-batchNumber" value="{{$data? $data[0]['batchNumber'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">MerchantId</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-merchantId" value="{{$data? $data[0]['merchantId'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">TerminalId</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-terminalId" value="{{$data? $data[0]['terminalId'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Show Print ExpDate</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-showPrintExpDate" value="{{$data? $data[0]['showPrintExpDate'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">checkCardExpDate</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-checkCardExpDate" value="{{$data? $data[0]['checkCardExpDate'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Credit Settlement</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-creditSettlement" value="{{$data? $data[0]['creditSettlement'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Debit Settlement</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-debitSettlement" value="{{$data? $data[0]['debitSettlement'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">Terminal ExtId</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="ac-terminalExtId" value="{{$data? $data[0]['terminalExtId'] : ''}}"></div>
                    </div>
					
					<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-ac" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-ac"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <a class="btn btn-flat btn-secondary" href="{{url('/acquirer')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/acquirer.js') }}"></script>

@endsection
