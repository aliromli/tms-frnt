@extends('layouts.app')
@section('title', 'Device Profile')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Device Profile</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/merchant')}}">Device Profile</a></li>
                    <li><span>Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">{{ $edit=='no' ? 'Add Device Profile' : 'Edit Device Profile ' }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="dp-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label col-md-3">@lang('device-profile.name')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="dp-name" value="{{$data ? $data[0]['name'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.heartbeatInterval')</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="dp-heartbeatInterval" value="{{$data? $data[0]['heartbeatInterval'] : ''}}"></div>
                    </div>					
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.diagnosticInterval')</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="dp-diagnosticInterval" value="{{$data? $data[0]['diagnosticInterval'] : ''}}"></div>
                    </div>
                    
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.maskHomeButton')</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['maskHomeButton'])=='true' ? 'checked' : '' : ''}}  name="maskHomeButton[]" value="true" id="dp-maskHomeButton-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-maskHomeButton-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['maskHomeButton'])=='false' ? 'checked' : '' : ''}}  name="maskHomeButton[]" value="false"  id="dp-maskHomeButton-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-maskHomeButton-false">FALSE</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.maskStatusBar')</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['maskStatusBar'])=='true' ? 'checked' : '' : ''}}  name="maskStatusBar[]" value="true" id="dp-maskStatusBar-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-maskStatusBar-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['maskStatusBar'])=='false' ? 'checked' : '' : ''}}  name="maskStatusBar[]" value="false"  id="dp-maskStatusBar-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-maskStatusBar-false">FALSE</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.scheduleReboot')</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['scheduleReboot'])=='true' ? 'checked' : '' : ''}} name="scheduleReboot[]" value="true" id="dp-scheduleReboot-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-scheduleReboot-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['scheduleReboot'])=='false' ? 'checked' : '' : ''}} name="scheduleReboot[]" value="false"  id="dp-scheduleReboot-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-scheduleReboot-false">FALSE</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.scheduleRebootTime')</label>
                        <div class="col-md-8">
                            <input class="form-control-sm" type="time" step=1 value="now" id="dp-scheduleRebootTime">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Default</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['default'])=='true' ? 'checked' : '' : ''}}  name="default[]" value="true" id="dp-default-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-default-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['default'])=='false' ? 'checked' : '' : ''}} name="default[]" value="false"  id="dp-default-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-default-false">FALSE</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.relocationAlert')</label>
                        <div class="col-md-8">
                        <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]["relocationAlert"])=="true" ? "checked" : "" : ""}} name="relocationAlert[]" value="true" id="dp-relocationAlert-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-relocationAlert-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]["relocationAlert"])=="false" ? "checked" : "" : ""}} name="relocationAlert[]" value="false"  id="dp-relocationAlert-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-relocationAlert-false">FALSE</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.movingThreshold')</label>
                        <div class="col-md-8">
                            <input type="number" class="form-control input-xs" id="dp-movingThreshold" value="{{$data? $data[0]['movingThreshold'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.adminPassword')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="dp-adminPassword" value="{{$data? $data[0]['adminPassword'] : ''}}"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.frontApp')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="dp-frontApp" value="{{$data? $data[0]['frontApp'] : ''}}"></div>
                    </div>
                  
					<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="version-dp" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-dp"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <a class="btn btn-flat btn-secondary" href="{{url('/device-profile')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/device_profile.js') }}"></script>

@endsection
