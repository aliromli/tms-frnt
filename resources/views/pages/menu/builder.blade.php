@extends('layouts.app')
@section('title', 'Menu Builder')
@section('ribbon')
@endsection

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  

@section('content')

<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Menu Builder</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{ URL::to('/menu') }}">@lang('menu.menu')</a></li>
                    <li>@lang('menu.builder')</li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="header-title">Menu Builder</h4> -->
                    <div class="data-tables">

                        <div class="box bg-none no-border">
                            <div class="box-header no-border text-right">
                                <button class="btn btn-primary" id="btn-add"><i class="fa fa-plus-circle"></i> @lang('menu.add_new_menu')</button> &nbsp;
                                <button class="btn btn-default" id="btn-submit"><i class="fa fa-check-circle"></i> @lang('general.update')</button>
                            </div>

                            <!-- Menu list update -->
                            <input id="menu-list" type="hidden">

                            <div class="box-body">
                                <div class="dd" id="nestable">
                                    <ol class="dd-list">
                                        @each('pages.menu.menu', $menus, 'menu')
                                    </ol>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
    @include('pages.menu.new-item');
@endsection

@section('script')
<script src="{{ asset('assets/js/libs/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/plugin/jquery-nestable/jquery.nestable.min.js') }}"></script>
<script src="{{ asset('assets/js/scriptsMenu.js') }}"></script>
@endsection


@section('css')
<!-- <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/bootstrap-datepicker.css') }}"> -->
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery-ui.css') }}"> -->
<style>
    .dd {
        margin-bottom: 15px;
    }
    .builder-menu,
    .builder-footer {
        position: relative;
        overflow: auto;
        margin-left: -15px;
        margin-right: -15px;
    }
    .builder-menu {
        border-bottom: 1px solid #ddd;
        padding: 0 15px;
        margin-bottom: 10px;
    }
    .builder-footer {
        border-top: 1px solid #ddd;
        padding: 15px;
        margin-top: 10px;
    }
    .builder-menu .title {
        font-size: 13px;
        font-weight: 700;
    }
    .builder-menu >button {
        position: absolute;
        display: inline;
        top: 0;
        right: 15px;
    }
</style>
@endsection


