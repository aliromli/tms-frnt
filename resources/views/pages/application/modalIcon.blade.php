<div class="modal fade" id="modal-show-icon">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>
            <form class="form-horizontal" id="form-c">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">View Icon</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row" id="divct-version">
                        <div>
                            @if($edit=='ya')
                                <img src="{{$data ? $data['iconUrl'] : ''}}" class="img" />
                            @endif
                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-flat btn-default" data-dismiss="modal">@lang('general.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>
