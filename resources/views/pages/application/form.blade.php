@extends('layouts.app')
@section('title', 'Form Application')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
<style>

</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Application</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/application')}}">Application</a></li>
                    <li><span>Application Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                <form class="form-horizontal" id="form-application">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">{{ $edit=='ya' ? 'Edit Application' : 'New Application'}}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="application-id" value="{{$data ? $data['id'] : ''}}">
                        <label for="" class="control-label col-md-3">@lang('application.name')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="application-name" value="{{$data ? $data['name'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('application.packageName')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="application-packageName" value="{{$data? $data['packageName'] : ''}}"></div>
                    </div>					<div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('application.description')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="application-description" value="{{$data? $data['description'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('application.appVersion')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="application-appVersion" value="{{$data? $data['appVersion'] : ''}}"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Uninstallable</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['uninstallable'])=='true' ? 'checked' : '' : ''}}  name="uninstallable[]" value="true" id="dp-uninstallable-true"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-uninstallable-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data['uninstallable'])=='false' ? 'checked' : '' : ''}} name="uninstallable[]" value="false"  id="dp-uninstallable-false"  class="custom-control-input">
                                <label class="custom-control-label" for="dp-uninstallable-false">FALSE</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('application.companyName')</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-xs" id="application-companyName" value="{{$data? $data['companyName'] : ''}}">
                        </div>
                    </div>
                    
                    <div class="form-group required row">
                        <label for="" class="control-label col-md-3" id="label-apk">APK</label>
                        <div class="col-md-5">
                           <input type="file" name="apk" class="form-control input-xs" id="application-apk">
                        </div>
                    </div>
                    <div class="form-group required row">
                        <label for="" class="control-label col-md-3" id="label-icon">Icon</label>
                        <div class="col-md-5">
                           <input type="file" name="icon" class="form-control input-xs" id="application-icon">
                        </div>
                        <div class="col-md-3">
                            @if($edit=='ya')
                                 <a href="{{$data ? $data['iconUrl'] : ''}}" id="show-icon_xx" target="_blank">Show Icon</a>
                                 
                            @endif
                           
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('application.deviceModel')</label>
                        <div class="col-md-8">
                            <input type="hidden" class="form-control input-xs" id="application-deviceModelId" value="{{$data? $data['deviceModel']['id'] : ''}}">
                            <input type="text" class="form-control input-xs" id="application-deviceModel-search" value="{{$data? $data['deviceModel']['model'] : ''}}">
                            <div class="form-control-feedback">Autocomplete. Insert min. 3 chars. </div>
                        </div>
                    </div>
					<div class="form-group row" id="div-m-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input readOnly type="text" class="form-control input-xs" id="version-app" value="{{$data? $data['version'] : ''}}"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-application"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <a class="btn btn-flat btn-secondary" href="{{url('/application')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script src="{{ asset('assets/js/application.js') }}"></script>

@endsection
