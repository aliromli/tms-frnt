@extends('layouts.app')
@section('title', 'APK')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
<style>

</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Application</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/application')}}">Application</a></li>
                    <li><span>AAPK</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                <form class="form-horizontal" id="form-application">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">APK</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">URL APK</label>
                        <div class="col-md-6"><input type="text" class="form-control input-xs" id="application-apk" value="{{$data ? $data['url'] : ''}}"></div>
                        <div class="col-md-2"><a class="btn btn-flat btn-primary input-xs"  href="{{$data ? $data['url'] : ''}}">Click</a></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Md5</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="application-md5" value="{{$data ? $data['md5'] : ''}}"></div>
                    </div>
		        </div>
                <div class="modal-footer">
                    <a class="btn btn-flat btn-secondary" href="{{url('/application')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script src="{{ asset('assets/js/application.js') }}"></script>

@endsection
