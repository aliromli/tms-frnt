@extends('layouts.app')
@section('title', 'Application')
@section('ribbon')
@endsection


<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>
#dataTableApplication {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
    height: 300px;
}  
</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Application</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><span>Application</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="header-title">Data User</h4> -->
                    <div class="box-header no-border text-right filter" style="margin-bottom:20px;"> 
                    <a class="btn btn-flat btn-secondary clear-search" href="#">Clear Search</a> &nbsp;
                        <a class="btn btn-flat btn-secondary tugel" href="#"><i class="fa fa-chevron-down sign"></i> <span class="filter-title">Show</span><span class="filter-title hidden">Hide</span> Filter <i class="fa fa-filter"></i></a> &nbsp;
                        <a class="btn btn-flat btn-primary" href="{{url('/application-form')}}"><i class="fa fa-plus-circle"></i> @lang('Application.new')</a> &nbsp;
                 		
                    </div>
                  
                    <div class="form-filter">
                        <form class="form-horizontal">
                                <div class="form-group row" xstyle="margin-left:-28;">
                                    <div class="col-md-3">
                                        <input type="text" id="search-name-app" class="form-control input-xs" placeholder="Name...">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" id="search-sn-app" class="form-control input-xs" placeholder="SN...">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" id="search-model-app" class="form-control input-xs" placeholder="Device Model...">
                                        <input type="hidden" id="search-model-app-id" class="form-control input-xs">
                                    </div>
                                    
                                    <div class="col-md-2"> 
                                        <button type="button" id="btn-cari-app" class="btn  btn-flat btn-secondary btn-cari">Cari</button>
                                    </div>
                                </div> 
                        </form>
                    </div>
                    
                    <div class="data-tables">
                        <table id="dataTableApplication" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('application.version')</th>
                                    <th>@lang('application.name')</th>
                                    <th>@lang('application.packageName')</th>
                                    <th>@lang('application.description')</th>
                                    <th>@lang('application.appVersion')</th>
                                    <th>@lang('application.companyName')</th>
                                    <th>@lang('application.uninstallable')</th>
                                    <th>FileSize Apk</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('modal')
    @include('pages.application.modalIcon')
@endsection


@section('script')
<!-- Start datatable js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('assets/js/application.js') }}"></script>

@endsection
