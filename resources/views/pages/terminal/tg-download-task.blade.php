<div class="modal fade" id="modal-list-tg-form-t">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div> 
            </div>
            <form class="form-horizontal" id="form-list-tg-g">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">Terminal Group List</h4>
                </div>
                <div class="modal-body">
                    <div class="data-tables">
                        <table id="dataTableTgListGFormT" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>Version</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-flat btn-xs" id="btn-tg-to-list-t"><i class="fa fa-check-circle"></i> Add</button>
                    <button class="btn btn-default btn-flat btn-xs" data-dismiss="modal">@lang('general.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>
