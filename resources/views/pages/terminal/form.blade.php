@extends('layouts.app')
@section('title', 'Terminal')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Terminal</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/merchant')}}">Terminal</a></li>
                    <li><span>Terminal Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                <form class="form-horizontal" id="form-merchant">
                    <div class="modal-header bg-dark no-border">
                        <h4 class="modal-title">{{ $edit=="no" ? "New" : "Edit" }} Terminal</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="hidden" class="form-control input-xs" id="terminal-id" value="{{$data ? $data[0]['id'] : ''}}">
                            <label for="" class="control-label col-md-3">@lang('terminal.sn')</label>
                            <div class="col-md-8"><input type="text" class="form-control input-xs" id="terminal-sn" value="{{$data ? $data[0]['sn'] : ''}}"></div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="control-label col-md-3">@lang('terminal.merchant')</label>
                            <div class="col-md-8">
                                <select class="form-control input-xs" id="terminal-merchant">
                                    @if($edit=="ya")
                                        @foreach($merchant as $c) 
                                            @if($data[0]['merchant']['id'] == $c['id'])
                                                <option value="{{ $c['id'] }}" selected>{{ $c['name'] }}</option>
                                            @else
                                                <option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
                                            @endif
                                        @endforeach
                                        <option value="">&raquo; @lang('general.select') @lang('merchant.district')</option>
                             
                                    @else
                                        <option value="">&raquo; @lang('general.select') @lang('merchant.district')</option>
                                        @foreach($merchant as $c)
                                            <option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
                                        @endforeach
                                    @endif


                                 </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="control-label col-md-3">@lang('terminal.devicemodel')</label>
                            <div class="col-md-8">
                                <select class="form-control input-xs" id="terminal-devicemodel">
                                   @if($edit=="ya")
                                            @foreach($devicemodel as $c)
                                                
                                                @if($data[0]['model']['id'] == $c['id'])
                                                    
                                                    <option value="{{ $c['id'] }}" selected>{{ $c['model'] }}</option>
                                                @else
                                                    
                                                    <option value="{{ $c['id'] }}" >{{ $c['model'] }}</option>
                                                @endif
                                            @endforeach
                                            <option value="">&raquo; @lang('general.select') @lang('terminal.devicemodel')</option>
                                    @else
                                            <option value="">&raquo; @lang('general.select') @lang('terminal.devicemodel')</option>
                                            @foreach($devicemodel as $c)
                                                <option value="{{ $c['id'] }}" >{{ $c['model'] }}</option>
                                            @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="control-label col-md-3">@lang('terminal.deviceprofile')</label>
                            <div class="col-md-8">
                                <select class="form-control input-xs" id="terminal-deviceprofile">
                                   @if($edit=="ya")
                                        @foreach($deviceprofile as $c)
                                            
                                            @if($data[0]['profile']['id'] == $c['id'])
                                                
                                                <option value="{{ $c['id'] }}" selected>{{ $c['name'] }}</option>
                                            @else
                                                
                                                <option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
                                            @endif
                                        @endforeach
                                        <option value="">&raquo; @lang('general.select') @lang('terminal.deviceprofile')</option>
                                    @else
                                        <option value="">&raquo; @lang('general.select') @lang('terminal.deviceprofile')</option>
                                        @foreach($deviceprofile as $c)
                                             <option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="control-label col-md-3">Terminal Group</label>
                            <div class="cardx col-md-8">
                                <div class="xcard-body">
                                    <button type="button" class="btn btn-flat btn-success btn-xs" id="add-tg-to-list-t"> Click List Terminal Group</button>
                                    <table class="table mt-2" id="list-group-tg-t">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" xid="div-t-version" style="<?php echo ($edit=='no') ? 'display:none;':''; ?>">
                            <label for="" class="control-label col-md-3">Version</label>
                            <div class="col-md-8"><input readOnly type="text" class="form-control input-xs" id="version-terminal" value="{{$data? $data[0]['version'] : ''}}"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-flat btn-primary" id="btn-submit-terminal"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                        <a class="btn btn-flat btn-secondary" href="{{url('/terminal')}}">@lang('general.back')</a>
                    </div>
                </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.terminal.tg-download-task')
@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/terminal.js') }}"></script>

@endsection
