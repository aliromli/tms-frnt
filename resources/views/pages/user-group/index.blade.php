@extends('layouts.app')
@section('title', 'User Group')
@section('ribbon')
@endsection


<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  

@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">User Group</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><span>@lang('user-group.user_group')</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="header-title">Data User Group</h4> -->
                    <div class="box-header no-border text-right filter" style="margin-bottom:20px;"> 
                        <!-- <a class="" id="btn-filter"><i class="fa fa-minus-square-o"></i> Filter</a> &nbsp; -->
                        <a class="btn btn-secondary tugel" href="#"><i class="fa fa-chevron-down sign"></i> <span class="filter-title">Show</span><span class="filter-title hidden">Hide</span> Filter <i class="fa fa-filter"></i></a> &nbsp;
                        <button class="btn btn-primary" id="btn-add-user-group"><i class="fa fa-plus-circle"></i> Add User Group</button> 
                       
                    </div>
                    <div class="form-filter">
                        <form class="form-horizontal">
                            <div class="col-md-7">
                                <div class="form-group row" style="margin-left:-28;">
                                   
                                    <div class="col-md-4"><input type="text" id="search-user-group" class="form-control input-xs" placeholder="Search..."></div>
                                    <button type="button" id="btn-cari-usergroup" class="btn btn-xs btn-flat btn-light col-md-1" style="background-color:transparent" ><i class="ti-search" style="font-size:23px;"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="data-tables">
                        <table id="dataTableUserGroup"  width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:50px">No</th>
                                    <!-- <th>Id</th> -->
                                    <th>@lang('user-group.group_name')</th>
                                    <th>@lang('user-group.description')</th>
                                    <th style="width: 150px">@lang('general.action')</th>
                                </tr>                                
                            </thead>
                            <tbody>                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.user-group.new-user-group')
    @include('pages.user-group.privileges')
@endsection

@section('script')
<!-- <script src="{{ asset('js/plugin/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script> -->
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script> 
<script src="{{ asset('assets/js/scriptsUserGroup.js') }}"></script>

@endsection
