@extends('layouts.app')
@section('title', 'District')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  

@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">District</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><span>District</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
	
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="header-title">Data User</h4> -->
                    <div class="box-header no-border text-right filter" style="margin-bottom:20px;"> 
                    <a class="btn btn-flat btn-secondary clear-search" href="#">Clear Search</a> &nbsp;
                        <a class="btn btn-flat btn-secondary tugel" href="#"><i class="fa fa-chevron-down sign"></i> <span class="filter-title">Show</span><span class="filter-title hidden">Hide</span> Filter <i class="fa fa-filter"></i></a> &nbsp;
                        <button class="btn btn-flat btn-primary" id="btn-add-district" ><i class="fa fa-plus-circle"></i> @lang('district.new_district')</button> 
                       
                    </div>
                  
                    <div class="form-filter">
                        <form class="form-horizontal">
                           
                                <div class="form-group row" xstyle="margin-left:-28;">
                                   
                                    <div class="col-md-4"><input type="text" id="search-district" class="form-control input-xs" placeholder="District..."></div>
                                    <div class="col-md-4">
                                        <select class="form-control input-xs" id="search-district-city">
                                            <option value="">&raquo; @lang('general.select') @lang('district.name')</option>
                                            @foreach($city as $c)
                                            <option value="{{ $c['id'] }}">{{ $c['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" id="btn-cari-district" class="btn  btn-flat btn-secondary input-sm" style="height:3em;" ><i class="ti-search" style="font-size:22px;"></i></button>
                                    </div>
                                </div>

                                
                                
                           
                        </form>
                    </div>
                    <div class="data-tables">
                        <table id="dataTableDistrict"  xclass="text-center" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('district.version')</th>
                                    <th>@lang('district.name')</th>
                                    <th>@lang('district.city')</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('modal')
    @include('pages.district.new-district')
   
@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('assets/js/district.js') }}"></script>

@endsection
