@extends('layouts.app')
@section('title', 'Last Diagnostic Detail')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css"> -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Last Diagnostic Detail</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/diagnostic-lastDiagnostic')}}">Last Diagnostic</a></li>
                    <li><span>Last Diagnostic Detail</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">Last Diagnostic</h4>
                </div>
                <div class="modal-body">
                   
					<!-- <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.heartbeatInterval')</label>
                        <div class="col-md-8">
                            <input type="number" class="form-control input-xs" id="dp-heartbeatInterval" value="{{$data? $data[0]['heartbeatInterval'] : ''}}">
                        </div>
                    </div>					
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.diagnosticInterval')</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="dp-diagnosticInterval" value="{{$data? $data[0]['diagnosticInterval'] : ''}}"></div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('device-profile.scheduleRebootTime')</label>
                        <div class="col-md-8">
                            <input class="form-control-sm" type="time" value="13:45:00" id="dp-scheduleRebootTime">
                        </div>
                    </div> -->
                    
                    
					
                </div>
                <div class="modal-footer">
                    <a class="btn btn-flat btn-secondary" href="{{url('/diagnostic-lastDiagnostic')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script> -->


@endsection
