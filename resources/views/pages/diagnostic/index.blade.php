@extends('layouts.app')
@section('title', 'Diagnostic')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Diagnostic</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><span>Diagnostic</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="header-title">Data User</h4> -->
                    <div class="box-header no-border text-right filter" style="margin-bottom:20px;"> 
                        <a class="btn btn-flat btn-primary" href="{{url('/diagnostic-lastDiagnostic')}}"> Last Diagnostic</a> &nbsp;
                        <a class="btn btn-flat btn-primary" href="{{url('/diagnostic-lastHeartbeat')}}"> Last HeartBeat</a> &nbsp;
                      
                    </div>
                  
                    <div class="form-filter">
                       
                    </div>
                    
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


