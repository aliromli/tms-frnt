<div class="modal fade" id="modal-tenant">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>
            <form class="form-horizontal" id="form-tenant">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">New Tenant</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="tenant-id">
                        <label for="" class="control-label col-md-4">Tenant Name</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tenant-name"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-4">Super Tenant</label>
                        <div class="col-md-8">
                            <select class="form-control input-xs" id="super-tenant">
                                <option value="">&raquo; @lang('general.select') Tenant</option>
                                @foreach($tenant as $c)
                                <option value="{{ $c['id'] }}">{{ $c['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-4">Is Super</label>
                        <div class="col-md-8">
                        <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]["is_super"])=="true" ? "checked" : "" : ""}} name="is_super[]" value="true" id="tenant-is-super-true"  class="custom-control-input">
                                <label class="custom-control-label" for="tenant-is-super-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]["is_super"])=="false" ? "checked" : "" : ""}} name="is_super[]" value="false"  id="tenant-is-super-false"  class="custom-control-input">
                                <label class="custom-control-label" for="tenant-is-super-false">FALSE</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" id="divtn-version">
                        <label for="" class="control-label col-md-4">Version</label>
                        <div class="col-md-8"><input readOnly type="text" class="form-control input-xs" id="version-tnt"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-tnt"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <button class="btn btn-flat btn-default" data-dismiss="modal">@lang('general.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>
