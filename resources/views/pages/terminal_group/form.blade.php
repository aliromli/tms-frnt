@extends('layouts.app')
@section('title', 'Terminal Group')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Terminal Group</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/terminal-group')}}">Terminal Group</a></li>
                    <li><span>Terminal Group Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                <form class="form-horizontal" id="form-tg">
                    <div class="modal-header bg-dark no-border">
                        <h4 class="modal-title">{{ $edit=="no" ? "New" : "Edit" }} Terminal Group</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="hidden" class="form-control input-xs" id="terminal-g-id" value="{{$data ? $data[0]['id'] : ''}}">
                            <label for="" class="control-label col-md-3">Name</label>
                            <div class="col-md-8"><input type="text" class="form-control input-xs" id="terminal-g-name" value="{{$data ? $data[0]['name'] : ''}}"></div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="control-label col-md-3">Description</label>
                            <div class="col-md-8"><input type="text" class="form-control input-xs" id="terminal-g-description" value="{{$data ? $data[0]['description'] : ''}}"></div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="control-label col-md-3">Terminal</label>
                            <div class="cardx col-md-8">
                                <div class="xcard-body">
                                    <button type="button" class="btn btn-flat btn-primary" id="add-terminal-to-list"> Click List Terminal</button>
                                    <!-- <ul class="list-group mt-2" id="list-group">
                                    </ul> -->
                                    <table class="table mt-2" id="table-form-terminal-group">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Model</th>
                                                <th>Merchant</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row" xid="div-gt-version" style="<?php echo ($edit=='no') ? 'display:none;':''; ?>">
                            <label for="" class="control-label col-md-3">Version</label>
                            <div class="col-md-8"><input type="text" readonly class="form-control input-xs" id="version-terminal-g" value="{{$data? $data[0]['version'] : ''}}"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-flat btn-primary" id="btn-submit-terminal-group"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                        <a class="btn btn-flat btn-secondary" href="{{url('/terminal-group')}}">@lang('general.back')</a>
                    </div>
                </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.terminal_group.terminal_list_form')
@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/terminal_group.js') }}"></script>

@endsection
