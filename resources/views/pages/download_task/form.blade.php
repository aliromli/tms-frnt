@extends('layouts.app')
@section('title', 'Download Task')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  



<style>
#dataTableApp1 {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
#dataTableApp2 {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
</style>
@section('content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Download Task</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/download-task')}}">Downlaod Task</a></li>
                    <li><span>Download Task Form</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                <form class="form-horizontal" id="form-download-task">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">New Download Task</h4>
                </div>
                <div class="modal-body">
                    <!-- <div class="row">
                        <div class='col-sm-6'>
                            <div class="form-group">
                                <div class='input-group date' >
                                    <input type='text' id='datetimepicker1' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>    -->

                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="download-task-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label col-md-3">@lang('download-task.name')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="download-task-name" value="{{$data ? $data[0]['name'] : ''}}"></div>
                    </div>
								
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('download-task.status')</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['status'])=='1' ? 'checked' : '' : ''}}  name="downloadstatus[]" value="1" id="download-task-status-true"  class="custom-control-input">
                                <label class="custom-control-label" for="download-task-status-true">1</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['status'])=='0' ? 'checked' : '' : ''}} name="downloadstatus[]" value="0"  id="download-task-status-false"  class="custom-control-input">
                                <label class="custom-control-label" for="download-task-status-false">0</label>
                            </div>
                        </div>
                    </div>
                    <!-- 2009-10-16 21:30:45 -->
                    <div class="form-group row">
                        <label for="download-task-publish_time" class="control-label col-md-3">@lang('download-task.publish_time')</label>
                        <div class="col-md-8">
                            <input type='datetime-local' step=1 id='download-task-publish_time' value="{{$data? $data[0]['publishTime'] : ''}}" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="download-task-publish_time_type" class="control-label col-md-3">@lang('download-task.publish_time_type')</label>
                        <div class="col-md-8"><input type="text" class="form-control numeric input-xs" id="download-task-publish_time_type" value="{{$data? $data[0]['publishTimeType'] : ''}}"></div>
                    </div>
                 
                    <div class="form-group row">
                        <label for="download-task-installation_time" class="control-label col-md-3">@lang('download-task.installation_time')</label>
                        <div class="col-md-8">
                            <input type="datetime-local" step=1  class="form-control input-xs" id="download-task-installation_time" value="{{$data? $data[0]['installationTime'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('download-task.installation_time_type')</label>
                        <div class="col-md-8"><input type="text" class="form-control numeric input-xs" id="download-task-installation_time_type" value="{{$data? $data[0]['installationTimeType'] : ''}}"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">@lang('download-task.installationNotification')</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control numeric input-xs" id="download-task-installationNotification" value="{{$data? $data[0]['installationNotification'] : ''}}">
                        </div>
                    </div>
                    <!-- 2023-07-19T15:30:00 -->
                    <div class="form-group row">
                        <label for="download-task-downloadTimeType" class="control-label col-md-3 col-form-label">@lang('download-task.downloadTimeType')</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control numeric input-xs" id="download-task-downloadTimeType"  value="{{$data? $data[0]['downloadTimeType'] : ''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Download Time</label>
                        <div class="col-md-8">
                            <input type="datetime-local" step=1 value="{{$data? $data[0]['downloadTime'] : ''}}" class="form-control input-xs" id="download-task-downloadTime" >
                        </div>
                    </div> 

                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Application</label>
                        <div class="cardx col-md-8">
                            <div class="xcard-body">
                             
                                <button type="button" class="btn btn-flat btn-success btn-xs" id="add-app-to-list"> Click List Application</button>
                                <table class="table mt-2" id="list-group-app">
                                    <thead>
                                        <tr>
                                            <th>App Version</th>
                                            <th>Name</th>
                                            <th>Package Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Terminal Group</label>
                        <div class="cardx col-md-8">
                            <div class="xcard-body">
                              
                                <button type="button" class="btn btn-flat btn-success btn-xs" id="add-tg-to-list"> Click List Terminal Group</button>
                                <table class="table mt-2" id="list-group-tg">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">Terminal </label>
                        <div class="cardx col-md-8">
                            <div class="xcard-body">
                                <button type="button" class="btn btn-flat btn-success btn-xs" id="add-t-to-list"> Click List Terminal</button>
                                <table class="table mt-2" id="table-form-terminal-dnt">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Model</th>
                                            <th>Merchant</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                
					<div class="form-group row" id="div-dt-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input readOnly type="text" class="form-control input-xs" id="version-dt" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-dt"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <a class="btn btn-flat btn-secondary" href="{{url('/download-task')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('modal')
    @include('pages.download_task.app-download-task')
    @include('pages.download_task.tg-download-task')
    @include('pages.download_task.t-download-task')
@endsection

@section('script')
  
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<!-- 
<link rel="stylesheet" href="
https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.2.4/jquery.datetimepicker.css
">
<link rel="stylesheet" href="
https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.2.4/jquery.datetimepicker.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.2.4/jquery.datetimepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.2.4/jquery.datetimepicker.min.js
"></script> -->

<script src="{{ asset('assets/js/download_task.js') }}"></script>
 @endsection
