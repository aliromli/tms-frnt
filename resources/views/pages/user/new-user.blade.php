<div class="modal fade" id="modal-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>
            <form class="form-horizontal" id="form-user">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">New User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="user-id">
                        <label for="" class="control-label col-md-4">@lang('user.email')</label>
                        <div class="col-md-8"><input type="email" class="form-control input-xs" id="email"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-4">@lang('user.name')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="name"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-4">@lang('change-password.password')</label>
                        <div class="col-md-8"><input type="password" class="form-control input-xs" id="password"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-4">@lang('change-password.confirm_password')</label>
                        <div class="col-md-8"><input type="password" class="form-control input-xs" id="confirm-password"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-4">@lang('user.group')</label>
                        <div class="col-md-8">
                            <select class="form-control input-xs" id="user-group">
                                <option value="">&raquo; @lang('general.select') @lang('user-group.user_group')</option>
                                @foreach($userGroup as $group)
                                <option value="{{ $group->id }}">{{ $group->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" id="u-cs">
                        <label for="" class="control-label col-md-4">Tenant</label>
                        <div class="col-md-8">
                            <select class="form-control input-xs" id="tenant">
                                <option value="">&raquo; @lang('user.all') Tenant</option>
                                @foreach($tenant as $t)
                                <option value="{{ $t['id'] }}">{{ $t['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
					
                    <div class="form-group row">
                        <label for="" class="control-label col-md-4">@lang('user.active')</label>
                        <div class="col-md-8">
                            <div class="radio radio-inline">
                                <label><input  checked="" type="radio" name="active[]" value="Y" id="user-active-yes"> @lang('general.yes')</label>
                            </div>
                            <div class="radio radio-inline">
                                <label><input type="radio" name="active[]" value="N" id="user-active-no"> @lang('general.no')</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-flat btn-xs" id="btn-submit"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <button class="btn btn-default btn-flat btn-xs" data-dismiss="modal">@lang('general.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>
