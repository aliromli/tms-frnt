<!-- main content area start -->
<div class="main-content">
            
            <!-- header area end -->
            @include('layouts.headerTop')
            
         
            @yield('content')
           
</div>

<!-- MODAL -->
@yield('modal')
<!-- END MODAL -->