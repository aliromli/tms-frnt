@extends('layouts.app')

@section('ribbon')
<ol class="breadcrumb">
    <li>Report</li>
    <li>Pasien</li>
    <li>Medical Check Up Work Diagnosis</li>
</ol>
@endsection

@section('content-class', 'grid')
@section('content')

<div class="form-filter">
    <a class="" href="#"><i class="fa fa-chevron-down sign"></i> <span class="filter-title">Show</span><span class="filter-title hidden">Hide</span> Filter <i class="fa fa-filter"></i></a>

    <form action="" class="form-horizontal">
        <div class="row">

            <div class="col-md-4">
                <div class="form-group">
                    <input type="hidden" class="form-control input-xs" id="rontgen-id">
                    <label for="" class="control-label col-md-4">Tgl. Input</label>
                    <div class="col-md-8">
                        <div class="input-group input-daterange">
                            <input type="text" class="form-control input-xs" id="from-date" data-provide="datepicker" style="border-right-width: 1px">
                            <div class="input-group-addon">&nbsp;-&nbsp;</div>
                            <input type="text" class="form-control input-xs" id="to-date" data-provide="datepicker">
                        </div>
                    </div>
                </div>
            </div>

           

            

            
           
            <div class="col-md-4">
                <div class="form-group">
                    <label for="" class="control-label col-md-4">Patient ID</label>
                    <div class="col-md-8"><input type="text" class="form-control input-xs" id="id-pasien"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="" class="control-label col-md-4">@lang('mcu.patient_name')</label>
                    <div class="col-md-8"><input type="text" class="form-control input-xs" id="nama"></div>
                </div>
            </div>
            
           
            
           
            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-md-2"></label>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-xs btn-primary btn-block" id="btn-filter-submit"><i class="fa fa-check-circle"></i> Submit</button>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-xs btn-default btn-block" id="btn-w-download"><i class="fa fa-download"></i> @lang('general.download')</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>

<!--div id="graph" style="height: 300px; width: 100%"></div -->

<table id="user-table" class="table table-striped table-borderless" width="100%">
    <thead>
        <tr>
            <th style="width:20px;">IdPasien</th>
            <th>Name</th>
            <th>Tgl. Input</th>
            <th>Diagnosis Kerja</th>
            <th style="width: 100px">Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@endsection

@section('modal')
    @include('pages.customer.new-customer')
@endsection

@section('script')
<script src="{{ asset('js/plugin/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/plugin/highcharts/highcharts.js') }}"></script>
<script src="{{ asset('js/plugin/highcharts/highcharts-3d.js') }}"></script>
<script>
$(document).ready(function () {

    $('select').select2({
        width: '100%',
        containerCssClass: 'select-xs'
    });

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: 'true'
    });

    $('.input-daterange').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: 'true'
    });

    // Toggle filter
    $('.form-filter a').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
    });



	$('#btn-w-download').click(function() {

		var data = new FormData();
		data.append('idPasien',$('#id-pasien').val());
		data.append('nama',$('#nama').val());
		//data.append('nip',$('#no-nip').val());
		//data.append('tglLahir',$('#tgl-lahir').val());
		//data.append('lp',$('#lp').val());
		//data.append('bagian',$('#bagian').val());
		///data.append('idPerusahaan',$('#perusahaan').val());
		//data.append('idVendor',$('#vendor').val());
		//data.append('client',$('#client').val());
		data.append('startDate',$('#from-date').val());
		data.append('endDate',$('#to-date').val());
		//data.append('diagnosis',$('#diagnosis').val());

		$.ajax({
            url: baseUrl+"/report/patient/diagnosa-kerja/export",
            type: 'POST',
			contentType: false,
			processData: false,
			cache: false,
			xhrFields: {
                responseType: 'blob'
            },
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			data: data,
			success: function (data, textStatus, xhr) {
                // check for a filename
                var filename = "";
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                    var a = document.createElement('a');
                    var url = window.URL.createObjectURL(data);
                    a.href = url;
                    a.download = filename;
                    document.body.append(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url);
                }
                else {
                    alert("Error");
                }
                //i = i + 1;
                //if (i < max) {
                //    DownloadFile(list);
                //}
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            },

        });
    });


    // You can use 'alert' for alert message
    // or throw to 'throw' javascript error
    // or none to 'ignore' and hide error
    // or you own function
    // please read https://datatables.net/reference/event/error
    // for more information
    $.fn.dataTable.ext.errMode = 'none';

    /**
     * Datatable initial
     */
    var dataTable = $('#user-table').DataTable({
        dom: '<"dt-toolbar"<"col-sm-6 col-xs-12 hidden-xs"l><"col-sm-6 col-xs-12 hidden-xs text-right"<"toolbar">>>rt<"dt-toolbar-footer"<"col-sm-6 col-xs-12"i><"col-sm-6 col-xs-12 hidden-xs"p>><"clear">',
        processing: false,
        serverSide: true,
        scrollX: true,
        pageLength: 15,
        lengthMenu: [[15, 25, 50, 100], [15, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url: baseUrl+"/report/patient/diagnosis-kerja-datatables", 
            type: 'GET',
            data:  function(d){
                d.idPasien = $('#id-pasien').val();
                d.nama = $('#nama').val();
                //d.nip = $('#no-nip').val();
                //d.tglLahir = $('#tgl-lahir').val();
                //d.sex = $('#lp').val();
                //d.bagian = $('#bagian').val();
                //d.idPerusahaan = $('#perusahaan').val();
                //d.idVendor = $('#vendor').val();
                //d.client = $('#client').val();
                d.startDate = $('#from-date').val();
                d.endDate = $('#to-date').val();
                //d.diagnosis = $('#diagnosis').val();
            }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            paginate: {
                first: '&laquo;',
                last: '&raquo;',
                next: '&rsaquo;',
                previous: '&lsaquo;'
            }
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "id_pasien", sortable: false, searchable: false},
            {data: "nama_pasien", name: "nama_pasien"},
            {data: "tgl_input", name: "tgl_input"}, 
            {data: "penyakit", name: "penyakit"},
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
            {
                targets: 4,
                render: function(d) {
                    return `<a href="/report/patient/medical-check-up/detail/`+d+`" class="btn btn-action btn-primary btn-xs" data-id='+d.id+'><i class="fa fa-ellipsis-h"></i></a> &nbsp;
                            <a data-id='+d.id+' target="_blank" href="/report/patient/medical-check-up/print/`+d+`" class="btn btn-action btn-default btn-xs"><i class="fa fa-print"></i></a>`;
                }
            }
        ]
    });
	// <a data-id='+d.id+' class="btn btn-action btn-primary btn-xs"><i class="fa fa-plus-circle"></i></a> &nbsp;
                        

    // Show chart
    // updateChart();

    $('#btn-filter-submit').click(function() {
        // updateChart();
        dataTable.draw(true);
    });

    $('#perusahaan').on('change', function() {
        // Get departments
        $.getJSON(baseUrl+'/department/'+$(this).val(), function(resp) {
            $('#bagian').html('<option value="">&raquo; Semua Bagian</option>').trigger('change');
            $.each(resp, function(i, o) {
                $('#bagian').append('<option value="'+o.bagian+'">'+o.bagian+'</option>');
            });
        });

        // Get clients
        $.getJSON(baseUrl+'/client/'+$(this).val(), function(resp) {
            $('#client').html('<option value="">&raquo; Semua Client</option>').trigger('change');
            $.each(resp, function(i, o) {
                $('#client').append('<option value="'+o.client+'">'+o.client+'</option>');
            });
        });
    });

});

/*
function updateChart() {
    // Update chart
    $.ajax({
        url: baseUrl + '/report/patient/diagnosa-kesehatan-kerja-summary',
        type: 'GET',
        data: {
            idPasien: $('#id-pasien').val(),
            nama: $('#nama').val(),
            nip: $('#no-nip').val(),
            tglLahir: $('#tgl-lahir').val(),
            sex: $('#lp').val(),
            bagian: $('#bagian').val(),
            idPerusahaan: $('#perusahaan').val(),
            idVendor: $('#vendor').val(),
            client: $('#client').val(),
            diagnosis: $('#diagnosis').val(),
            startDate: $('#from-date').val(),
            endDate: $('#to-date').val()
        },
        success: function(resp) {
            var data = [];
            var categories = [];
            var colors = ['#da251d', '#ff9900', '#006db7', '#109618'];

            $.each(resp, function(i, o) {
                data.push({
                    name: o.wh_name,
                    color: colors[parseInt(o.wh_id) - 1],
                    y: o.total
                });
                categories.push(o.wh_id);
            });

            $('#graph').highcharts({
                chart: {
                    type: 'pie',
                    backgroundColor: '#edecec',
                    colors: colors,
                    options3d: {
                        enabled: true,
                        alpha: 60,
                        beta: 0
                    }
                },
                plotOptions: {
                    pie: {
                        depth: 25,
                        innerSize: 80,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: 15,
                            formatter: function() {
                                var name = (this.point.name);
                                return '<span style="font-size: 14px; color: #535a6c; font-weight: normal">  '+Math.round((this.point.y / this.point.total) * 100) +'%</span><br/><span style="font-size: 10px; color: #333333; font-weight: normal">'+name+'</span><br/><span style="font-size: 10px; font-weight: normal; color: #333333"> Total '+this.point.y+'</span>';
                            },
                            style: {
                                fontFamily: 'Roboto',
                                textOutline: false,
                                fontSize: '12px',
                                fontWeight: 'bold'
                            }
                        }
                    }
                },
                tooltip: {
                    style: {

                    }
                },
                legend: {
                    enable: true,
                    align: 'center'
                },
                xAxis: {
                    categories: categories,
                    lineColor: '#333333',
                    lineWidth: 1,
                    tickAmount: 15,
                    tickInterval: 1,
                    gridLineColor: '#f5f5f5',
                    tickWidth: 0
                },
                title: null,
                credits: false,
                series: [
                    {
                        slicedOffset: 30,
                        name: 'Work Health Diagnosis',
                        borderColor: 'transparent',
                        data: data
                    }
                ]
            });
        }
    });
} */

</script>
@endsection
