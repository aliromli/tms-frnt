@extends('layouts.app')
@section('title', 'Home')
@section('ribbon')
@endsection


@section('content')
<!-- breadcrumb -->
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Dashboard</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="index.html">Home</a></li>
                    <li><span>Dashboard</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
          @include('layouts.rightMenuProfile')
        </div> 
    </div>
</div>
<!-- end breadcrumb -->
<div class="main-content-inner">
               
                <!-- row area start-->
            </div>
@endsection
