<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DateTime;


class CardController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	return view('pages.card.index', [
					'data' => null
				]);
	}

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        //$param['stateId']   = $request->state; 
        $param['name']   = $request->name; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/card/list', $param)->json();
   
		//print_r($response);
		
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
       
        return view('pages.card.form', [
           
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
       
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/card/get', [
            'id' => $request->id
        ])->json();
  
        return view('pages.card.form', [
            'data'=>$response['data'],
            'edit' => 'yes'
        ]);
    }

    public function store(Request $request)
    {
     	$response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/card/add', [
			    'name' => $request->name,				
                'binRangeStart'  => $request->binRangeStart,
                'binRangeEnd'  => $request->binRangeEnd,
                'cardNumLength' => $request->cardNumLength,
                'panDigitUnmasking'  => $request->panDigitUnmasking,
                'printCardholderCopy'  => $request->printCardholderCopy,
                'printMerchantCopy'  => $request->printMerchantCopy,
                'printBankCopy'  => $request->printBankCopy,
                'pinPrompt'   => $request->pinPrompt,
				'pinLength'  => $request->pinLength
                
            ])->json();
			return $this->responseCode($response,'Card has been added successfully');  
    }

    public function  show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/card/get', [
                'id' => $request->id
            ])->json();
           
		
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
       
		
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/card/update', [
                'name' => $request->name,				
                'binRangeStart'  => $request->binRangeStart,
                'binRangeEnd'  => $request->binRangeEnd,
                'cardNumLength' => $request->cardNumLength,
                'panDigitUnmasking'  => $request->panDigitUnmasking,
                'printCardholderCopy'  => $request->printCardholderCopy,
                'printMerchantCopy'  => $request->printMerchantCopy,
                'printBankCopy'  => $request->printBankCopy,
                'pinPrompt'   => $request->pinPrompt,
				'pinLength'  => $request->pinLength,
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
			return $this->responseCode($response,'Card has been update successfully');
		
      
    }

    public function delete(Request $request)
    {
      
            $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/card/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Card has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
