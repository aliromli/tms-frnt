<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserGroup;
use App\Models\Process;
use App\Models\Customer;
use App\Models\Vendor;
use App\Exports\UsersExport;
use App\Jobs\ImportUser;
use App\Models\Setting;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Description of UserController
 *
 * @author In House Dev Program
 */
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::where('active', 'Y')->get();

        $param['pageNum']   = 1;
        $param['pageSize']   = 300;

        $tenantId = '';
        $signature = '';
        $refNumber = '';
        $reqTime = '';
        $x_cunsomer_username =  '';
        
        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
         ->send('GET',  $this->apiTms() . 'api/v1/tenant/list', [
            'body' => json_encode($param)
         ])->json();
        $tenant =  $response['rows'];
        $process = null;
        $userGroup = UserGroup::where('active', 'Y')->get();
        return view('pages.user.index', ['process' => $process, 'userGroup' => $userGroup, 'customers' => $customers, 'tenant' => $tenant]);
    }

    /**
     * Show user profile
     *
     * @return type
     */
    public function profile()
    {
        return view('pages.user.profile');
    }

    /**
     * Change password form
     *
     * @return type
     */
    public function changePassword()
    {
        return view('pages.user.change-password');
    }

    /**
     * Setting form
     *
     * @return void
     */
    public function setting()
    {
        return view('pages.user.setting');
    }

    /**
     * Submit change password
     *
     * @param Request $request
     * @return type
     */
    public function storeChangePassword(Request $request)
    {
        if($request->password != $request->repassword) {
            return back()->with('error', 'Password doesn\'t match');
        }

        $user = User::where('id', Auth::user()->id)->first();
        $user->password = Hash::make($request->password);
        if(!$user->save()) {
            return back()->with('error', 'Failed to update password. Please try again');
        }

        return back()->with('success', 'Password has been changed');
    }

    /**
     * Setting
     *
     * @param Request $request
     * @return void
     */
    public function storeSetting(Request $request)
    {
        $setting = Setting::updateOrCreate(
            ['user_id' => Auth::user()->id],
            ['language' => $request->language]
        );

        if(!$setting->save()) {
            return back()->with('error', 'Failed to update setting. Please try again');
        }

        Session::put('app-locale', Auth::user()->setting->language);

        return back()->with('success', 'Setting has been update');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $u = new User();
        $u->email = $request->email;
        $u->name = $request->name;
        $u->password = Hash::make($request->password);
        $u->user_group_id = $request->group;
        $u->customer_id = null;
        $u->tenant_id = $request->tenant;
        $u->vendor_id = null;
        $u->active = $request->active;

        if(!$u->save()) {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t add user. Please try again']);
        }

        return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, $id)
    {
        $u = $user->find($id);
        return response()->json($u);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $u = $user->find($request->id);
        $u->email = $request->email;
        $u->name = $request->name;
        $u->user_group_id = $request->group;
        if($request->group == 3) //tenant
		{
		    $u->tenant_id =  $request->tenant;
			$u->vendor_id = null;
            $u->customer_id = null;
        	
		}
        else if($request->group == 1) //admin
        {   
            $u->tenant_id =  null;
			$u->vendor_id = null;
            $u->customer_id = null;

        }
		// if($request->group == 2) //vendor
		// {
		//     $u->tea_id = null;
		// 	$u->vendor_id = $request->vendor;
        	
		// }
		// else if ($request->group == 3) //tenant
		// {
		// 	$u->customer_id = $request->customer;
        //     $u->vendor_id = null;
		// }
		// else if ($request->group == 1) //admin
		// {
		// 	$u->customer_id = null;
        //     $u->vendor_id = null;

		// }
		// else
		// {
		// 	$u->customer_id = null;
        //     $u->vendor_id = null;
		// }
        $u->active = $request->active;

        if(!empty($request->password)) {
            $u->password = Hash::make($request->password);
        }

        if(!$u->save()) {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t update user. Please try again']);
        }

        return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been updated successfully']);
    }

    /**
     * Update profile
     *
     * @param Request $request
     * @return type
     */
    public function updateProfile(Request $request)
    {
        // Check is email exists
        $u = User::where('email', $request->email);
        if($u->count() > 0 && ($u->first()->id != Auth::user()->id)) {
            return back()->with('error', 'Email used by another user');
        }

        $up = User::find(Auth::user()->id);
        $up->email = $request->email;
        $up->name = $request->name;

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            if($file->isValid()) {
                $nameStripped = str_replace(" ", "-", $file->getClientOriginalName());
                $file->storeAs('user', $nameStripped);
                $up->avatar = $nameStripped;

                // Set session avatar
                session()->put('user.avatar', $nameStripped);
            }
        }

        if(!$up->save()) {
            return back()->with('error', 'Failed to update profile');
        }

        // Set updated session
        session()->put('user.name', $request->name);
        session()->put('user.email', $request->email);

        return back()->with('success', 'Profile has been updated');

    }


    /**
     * Activate user
     *
     * @param User $user
     * @param type $id
     * @return type
     */
    public function activateUser(User $user, $id)
    {
        $u = $user->find($id);
        $u->active = 'Y';

        if(!$u->save()) {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t activate user. Please try again']);
        }

        return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been activated successfully']);
    }

    /**
     * Inactivate user
     *
     * @param User $user
     * @param type $id
     * @return type
     */
    public function inactivateUser(User $user, $id)
    {
        $u = $user->find($id);
        $u->active = 'N';

        if(!$u->save()) {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t inactivate user. Please try again']);
        }

        return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been inactivated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    /**
     * User datatables
     *
     * @return type JSON user
     */
    public function datatables(Request $request)
    {
      
        $users = User::with(['userGroup']);

        $recordTotal = $users->count();
       
        if(!empty($request->name))
        {
           
            $users->where('name', 'ILIKE', '%'.$request->name.'%');
        }
        if(!empty($request->group))
        {
            $users->where('user_group_id',  $request->group);
        }
        // if(!empty($request->customer))
        // {
        //     $users->where('customer_id', $request->customer);
        // }
        // return datatables($users)
        //     ->addColumn('action', function($users) {

        //         if($users->active == 'Y') {
        //             $btnActive = '<button class="btn btn-default btn-xs btn-inactive" data-id='.$users->id.'><i class="fa fa-ban"></i></button>';
        //         } else {
        //             $btnActive = '<button class="btn btn-default btn-xs btn-active" data-id='.$users->id.'><i class="fa fa-check-circle"></i></button>';
        //         }

        //         return '<button type="button" class="btn btn-warning btn-xs btn-edit" data-id='.$users->id.'><i class="fa fa-pencil"></i></button>&nbsp; '.$btnActive;
        //     })
        //     ->addColumn('active', function($users){
        //         return ($users->active == 'Y')?'<span class="text-success">Yes</span>':'<span class="text-danger">No</span>';
        //     })
        //     ->rawColumns(['action', 'active'])
        //     ->addIndexColumn()
        //     ->toJson();

        $recordFiltered = $users->count();

        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
        
       
        $ar_user= array();
        foreach($users->get() as $data)
        {
            
        $tenantId = '';
        $signature = '';
        $refNumber = '';
        $reqTime = '';
        $x_cunsomer_username =  '';
        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
         
            ->get( $this->apiTms() . 'api/v1/tenant/get', [
                'id' => $data->tenant_id
            ])->json();
            
            array_push($ar_user,array("id"=>$data->id,
                                     "email"=>$data->email,
                                     "name"=>$data->name,
                                     "userGroup"=>$data->userGroup->name,
                                     "tenant"=> ($response['responseCode'] =='0000') ?   $response['data'][0]['name'] : null,
                                     "active" => $data->active
                                    ));
        }


        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $recordTotal, 
            'recordsFiltered'   => $recordFiltered,
            'data'              => $ar_user,
            'input'             => [
                'start' => $request->start,
                'draw' => $request->draw,
                'length' =>  $request->length,
                'order' => $orderIndex,
                'orderDir' => $orderDir,
                'orderColumn' => $request->columns[$orderIndex]['data']
            ]
        ]);
    }

    /**
     * Export users
     *
     * @return type file .xlsx
     */
    // public function export()
    // {
    //     return Excel::download(new UsersExport, 'users.xlsx');
    // }

    /**
     * Import users
     *
     * @param Request $request
     * @return type database
     */
    // public function import(Request $request)
    // {
    //     if ($request->hasFile('file')) {

    //         $file = $request->file('file');
    //         $processId = null;

    //         if($file->isValid()) {
    //             $nameStripped = str_replace(" ", "-", $file->getClientOriginalName());
    //             $file->storeAs('upload', $nameStripped);

    //             // Push into process
    //             $process = new Process();
    //             $process->fill([
    //                 'upload'    => 'user',
    //                 'processed' => 0,
    //                 'success'   => 0,
    //                 'failed'    => 0,
    //                 'total'     => 100,
    //                 'status'    => 'ON PROGRESS'
    //             ]);
    //             $process->save();

    //             $processId = $process->id;

    //             ImportUser::dispatch('upload'.DIRECTORY_SEPARATOR.$nameStripped, $process->id)->delay(now()->addSeconds(10));
    //         }

    //         return response()->json([
    //             'responseCode' => 200,
    //             'responseMessage' => 'Uploaded',
    //             'processId' => $processId
    //         ]);
    //     }
    // }
}
