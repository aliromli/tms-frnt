<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DateTime;

class DeleteTaskController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	return view('pages.delete_task.index', [
	                'data' =>null
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        
        $param['appName']   = $request->appName; 
        $param['packageName']   = $request->packageName; 
        
        $param['sn']   = $request->sn; 
        $param['name']   = $request->name; 
        
        if(count(json_decode($request->terminalId,true)) > 0){
            $param['terminalId'] = $request->terminalId;
        }
        if(count(json_decode($request->terminalGroupId,true)) > 0){
            $param['terminalGroupId'] = $request->terminalGroupId;
        }
      
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/deleteTask/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        return view('pages.delete_task.form', [
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
       
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/deleteTask/get', [
            'id' => $request->id
        ])->json();

        return view('pages.delete_task.form', [
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }

    public function store(Request $request)
    {
        $dst = DateTime::createFromFormat('Y-m-d\TH:i:s', $request->deleteTime);
           
        $dst = date_create(json_decode(json_encode($dst),true)['date']);
        
        $dst = date_format($dst,'Y-m-d H:i:s');
        
        //print_r($dst);
        //exit;
        
        $response = $this->httpWithHeaders()

           ->post( $this->apiTms()  . 'api/v1/deleteTask/add', [
                'name' => $request->name,
                'deleteTime' => $dst,
                'applications' =>  json_decode($request->applications),
                'terminalIds' =>  json_decode($request->terminalIds),
               
            ])->json();
            if($response['responseCode'] =='5555')
            {
                return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
            }
            else if($response['responseCode'] =='3333')
            {
                return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
            }
            else{
                return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Delete Task has been Added successfully']);
            }
      
    }

    public function show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/deleteTask/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
        $dst = DateTime::createFromFormat('Y-m-d\TH:i:s', $request->deleteTime);
        $dst = date_create(json_decode(json_encode($dst),true)['date']);
        $dst = date_format($dst,'Y-m-d H:i:s');
        
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms() . 'api/v1/deleteTask/update', [
                'name' => $request->name,
                'deleteTime' =>  $dst,
                'applications' =>  json_decode($request->applications),
                'terminalIds' =>  json_decode($request->terminalIds),
                'id' => $request->id,
                'version' => $request->version

            ])->json();
           
        //'Can\'t Update Download Task. Please try again'
        if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
        else if($response['responseCode'] =='5555')
        {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
        else if($response['responseCode'] =='0500')
        {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
            
        }
        else if($response['responseCode'] =='3333')
        {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Delete Task has been Update successfully']);
        }
    }

    public function delete(Request $request)
    {
      
            $response = $this->httpWithHeaders()
            ->post( $this->apiTms() . 'api/v1/deleteTask/delete', [
                'version' => $request->version,
                'id' => $request->id,
            ])->json();
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Delete Task has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
