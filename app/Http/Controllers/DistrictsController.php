<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;



class DistrictsController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}';
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';
        
        // $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
        //  ->send('GET',  $this->apiTms() . 'api/v1/city/list', [
        //     'body' => json_encode($param)
        //  ])->json();

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
        ->get( $this->apiTms()  . 'api/v1/city/list', $param)->json();
    
 

       
		return view('pages.district.index', [
					'city' => $response['rows'],
				]);
    }

    /**
     * Country datatables
     *
     * @return type JSON country
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['cityId']   = $request->city; 
        $param['name']   = $request->district; 
        
        
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}';
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';


        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
        ->get( $this->apiTms()  . 'api/v1/district/list', $param)->json();
     
        if($response['responseCode'] === "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function store(Request $request)
    {
        
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}'; 
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->post( $this->apiTms()  . 'api/v1/district/add', [
                'city_id' => $request->city,
                'name' => $request->name
            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t add District. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'District has been added successfully']);
        }
    }

    public function  show(Request $request)
    {
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}'; 
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->get( $this->apiTms() . 'api/v1/district/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}'; 
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
         
            ->post( $this->apiTms() . 'api/v1/district/update', [

                'city_id' => $request->city,
                'name' => $request->name,
                'version' => $request->version,
                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t Update District. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'District has been added successfully']);
        }
    }

    public function delete(Request $request)
    {
      
            $tenantId = 'qualita';
            $signature = 'tes';
            $refNumber = '{{ref}}{{$isoTimestamp}}'; 
            $reqTime = '{{$timestamp}}';
            $x_cunsomer_username =  'tes';

            $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            
            ->post( $this->apiTms() . 'api/v1/district/delete', [
                'version' => $request->version,
                'id' => $request->id,

            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'District has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }

}
