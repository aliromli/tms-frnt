<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DateTime;


class CapkController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	return view('pages.capk.index', [
					'data' => null
				]);
	}

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        //$param['stateId']   = $request->state; 
        $param['name']   = $request->name; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/capk/list', $param)->json();
   
		//print_r($response);
		
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
       
        return view('pages.capk.form', [
           
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
       
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/capk/get', [
            'id' => $request->id
        ])->json();
  
        return view('pages.capk.form', [
            'data'=>$response['data'],
            'edit' => 'yes'
        ]);
    }

    public function store(Request $request)
    {
        
		$expiryDate = "";
		if($request->expiryDate!="")
		{
			
			$dst = DateTime::createFromFormat('Y-m-d', $request->expiryDate);
			$dst = date_create(json_decode(json_encode($dst),true)['date']);
			$expiryDate = date_format($dst,'Y-m-d');
			
		}
		
		$response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/capk/add', [
			   
                "name" => $request->name,
                "idx" => $request->idx,
                "rid" => $request->rid,
                "modulus" => $request->modulus,
                "exponent" => $request->exponent,
                "algo" => $request->algo,
                "hash" => $request->hash,
                "expiryDate" => $expiryDate,
                "remark" => $request->remark
                
            ])->json();
			$this->responseCode($response,'Capk has been added successfully');  
    }

    public function  show(Request $request)
    {
       

        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/capk/get', [
                'id' => $request->id
            ])->json();
           
		
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        $expiryDate = "";
		if($request->expiryDate!="")
		{
			
			$dst = DateTime::createFromFormat('Y-m-d', $request->expiryDate);
			$dst = date_create(json_decode(json_encode($dst),true)['date']);
			$expiryDate = date_format($dst,'Y-m-d');
			
		}
		
		
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/capk/update', [

                "name" => $request->name,
                "idx" => $request->idx,
                "rid" => $request->rid,
                "modulus" => $request->modulus,
                "exponent" => $request->exponent,
                "algo" => $request->algo,
                "hash" => $request->hash,
                "expiryDate" => $expiryDate,
                "remark" => $request->remark,
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
			return $this->responseCode($response,'Capk has been update successfully');
		
      
    }

    public function delete(Request $request)
    {
      
            $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/capk/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Capk has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
