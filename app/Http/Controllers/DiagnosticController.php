<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class DiagnosticController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
		return view('pages.diagnostic.index', [
					'data'=>null,
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function lastHeartbeat(Request $request)
    {
        return view('pages.diagnostic.last-heartbeat', [
            'data'=>null,
        ]);
    }
    public function lastHeartbeatData(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['sn']   = $request->sn; 
        $param['terminalId']   = $request->terminalId; //"ec2fb6cc-ff74-e645-ecc2-45b3236b712d"; //$request->city; 
        

        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/diagnostic/lastHeartbeat', $param)->json();
       

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => count($response['data']),
                'recordsFiltered'   => count($response['data']), 
                'data'              => $response['data'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }
    public function lastDiagnosticDetail(){
        return view('pages.diagnostic.last-diagnostic-detail', [
            'data'=>null,
        ]);
    }

    public function lastDiagnostic(Request $request)
    {
        return view('pages.diagnostic.last-diagnostic', [
            'data'=>null,
        ]);
    }
    public function lastDiagnosticData(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['sn']   = $request->sn; //"V1E0176210";//$request->sn; 
        $param['terminalId']   = $request->terminalId;
        
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/diagnostic/lastDiagnostic', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => count($response['data']),
                'recordsFiltered'   => count($response['data']), 
                'data'              => $response['data'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }


}
