<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class AidController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	return view('pages.aid.index', [
					'data' => null
				]);
	}

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        //$param['stateId']   = $request->state; 
        $param['name']   = $request->name; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/aid/list', $param)->json();
   
		//print_r($response);
		
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
       
        return view('pages.aid.form', [
           
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
       
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/aid/get', [
            'id' => $request->id
        ])->json();
  
        return view('pages.aid.form', [
            'data'=>$response['data'],
            'edit' => 'yes'
        ]);
    }

    public function store(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/aid/add', [
			   
                "name" => $request->name,
                "txnType" => $request->txnType,
                "aid" => $request->aid,
                "aidVersion" =>  $request->aidVersion,
                "tacDenial" => $request->tacDenial, 
                "tacDefault" => $request->tacDefault,
                "tacOnline" => $request->tacOnline,
                "threshold" => $request->threshold,
                "targetPercentage" => $request->targetPercentage,      
                "maxTargetPercentage" =>  $request->maxTargetPercentage,   
                "ddol" => $request->ddol,      
                "tdol" => $request->tdol,        
                "floorLimit" => $request->floorLimit, 
                "appSelect" => $request->appSelect,
                "aidPriority" => $request->aidPriority,
                "trxType9C" => $request->trxType9C,
                "categoryCode" => $request->categoryCode,
                "clKernelToUse" => $request->clKernelToUse,
                "clOptions" => $request->clOptions,
                "clTrxLimit" => $request->clTrxLimit,
                "clCVMLimit" => $request->clCVMLimit,
                "clFloorLimit" => $request->clFloorLimit,
                "remark" => $request->remark,
                "emvConfTerminalCapability" => $request->emvConfTerminalCapability,
                "additionalTerminalCapability" => $request->additionalTerminalCapability,
                "dataTtq" => $request->dataTtq
            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'AID has been added successfully']);
        }
    }

    public function  show(Request $request)
    {
       

        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/aid/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/aid/update', [

                 "name" => $request->name,
                "txnType" => $request->txnType,
                "aid" => $request->aid,
                "aidVersion" =>  $request->aidVersion,
                "tacDenial" => $request->tacDenial, 
                "tacDefault" => $request->tacDefault,
                "tacOnline" => $request->tacOnline,
                "threshold" => $request->threshold,
                "targetPercentage" => $request->targetPercentage,      
                "maxTargetPercentage" =>  $request->maxTargetPercentage,   
                "ddol" => $request->ddol,      
                "tdol" => $request->tdol,        
                "floorLimit" => $request->floorLimit, 
                "appSelect" => $request->appSelect,
                "aidPriority" => $request->aidPriority,
                "trxType9C" => $request->trxType9C,
                "categoryCode" => $request->categoryCode,
                "clKernelToUse" => $request->clKernelToUse,
                "clOptions" => $request->clOptions,
                "clTrxLimit" => $request->clTrxLimit,
                "clCVMLimit" => $request->clCVMLimit,
                "clFloorLimit" => $request->clFloorLimit,
                "remark" => $request->remark,
                "emvConfTerminalCapability" => $request->emvConfTerminalCapability,
                "additionalTerminalCapability" => $request->additionalTerminalCapability,
                "dataTtq" => $request->dataTtq,
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'AID has been update successfully']);
        }
    }

    public function delete(Request $request)
    {
      
            $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/aid/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Aid has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
