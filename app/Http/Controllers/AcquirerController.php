<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DateTime;


class AcquirerController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	return view('pages.acquirer.index', [
					'data' => null
				]);
	}

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        //$param['stateId']   = $request->state; 
        $param['name']   = $request->name; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/acquirer/list', $param)->json();
   
		//print_r($response);
		
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
       
        return view('pages.acquirer.form', [
           
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
       
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/acquirer/get', [
            'id' => $request->id
        ])->json();
  
        return view('pages.acquirer.form', [
            'data'=>$response['data'],
            'edit' => 'yes'
        ]);
    }

    public function store(Request $request)
    {
     	$response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/acquirer/add', [
			    'name' => $request->name,
                'type' => $request->type,
                'description' => $request->description,
                'hostId' => $request->hostId,
                'settlementHostId' => $request->settlementHostId,
                'respTimeout' => $request->respTimeout,
                'acquirerId' => $request->acquirerId,
                'hostDestAddr' => $request->hostDestAddr,
                'hostDestPort' => $request->hostDestPort,
                'tleAcquirer' => $request->tleAcquirer,
                'tleSettingId' => $request->tleSettingId,
                'masterKeyLocation' => $request->masterKeyLocation,
                'masterKey' => $request->masterKey,
                'workingKey' => $request->workingKey,
                'batchNumber' => $request->batchNumber,
                'merchantId' => $request->merchantId,
                'terminalId' => $request->terminalId,
                'showPrintExpDate' => $request->showPrintExpDate,
                'checkCardExpDate' => $request->checkCardExpDate,
                'creditSettlement' => $request->creditSettlement,
                'debitSettlement' => $request->debitSettlement,
                'terminalExtId' => $request->terminalExtId,
                
                
            ])->json();
			$this->responseCode($response,'Acquirer has been added successfully');  
    }

    public function  show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/acquirer/get', [
                'id' => $request->id
            ])->json();
           
		
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
       
		
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/acquirer/update', [
                 'name' => $request->name,
                'type' => $request->type,
                'description' => $request->description,
                'hostId' => $request->hostId,
                'settlementHostId' => $request->settlementHostId,
                'respTimeout' => $request->respTimeout,
                'acquirerId' => $request->acquirerId,
                'hostDestAddr' => $request->hostDestAddr,
                'hostDestPort' => $request->hostDestPort,
                'tleAcquirer' => $request->tleAcquirer,
                'tleSettingId' => $request->tleSettingId,
                'masterKeyLocation' => $request->masterKeyLocation,
                'masterKey' => $request->masterKey,
                'workingKey' => $request->workingKey,
                'batchNumber' => $request->batchNumber,
                'merchantId' => $request->merchantId,
                'terminalId' => $request->terminalId,
                'showPrintExpDate' => $request->showPrintExpDate,
                'checkCardExpDate' => $request->checkCardExpDate,
                'creditSettlement' => $request->creditSettlement,
                'debitSettlement' => $request->debitSettlement,
                'terminalExtId' => $request->terminalExtId,
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
			return $this->responseCode($response,'Acquirer has been update successfully');
		
      
    }

    public function delete(Request $request)
    {
      
            $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/acquirer/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Acquirer has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
