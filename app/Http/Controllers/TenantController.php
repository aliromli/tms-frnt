<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class TenantController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
        $param['pageNum']   = 1;
        $param['pageSize']   = 500;
        
        $tenantId = '';
        $signature = '';
        $refNumber = '';
        $reqTime = '';
        $x_cunsomer_username =  '';
        
        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
         ->send('GET',  $this->apiTms() . 'api/v1/tenant/list', [
            'body' => json_encode($param)
         ])->json();
 
		return view('pages.tenant.index', [
			        'tenant' => $response['rows'],
                    'data'=> null
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
      
        $param['name']   = $request->name; 
        
        
        $tenantId = '';
        $signature = '';
        $refNumber = '';
        $reqTime = '';
        $x_cunsomer_username =  '';


        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
        ->get( $this->apiTms()  . 'api/v1/tenant/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function store(Request $request)
    {
        
        $tenantId = '';
        $signature = '';
        $refNumber = ''; 
        $reqTime = '';
        $x_cunsomer_username =  '';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->post( $this->apiTms()  . 'api/v1/tenant/add', [
                'super_tenant_id' => $request->super_tenant_id,
                'name' => $request->name,
                'is_super' => $request->is_super,
            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t add Tenant. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Tenant has been added successfully']);
        }
    }

    public function  show(Request $request)
    {
        $tenantId = '';
        $signature = '';
        $refNumber = ''; 
        $reqTime = '';
        $x_cunsomer_username =  '';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->get( $this->apiTms() . 'api/v1/tenant/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
        $tenantId = '';
        $signature = '';
        $refNumber = ''; 
        $reqTime = '';
        $x_cunsomer_username =  '';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
         
            ->post( $this->apiTms() . 'api/v1/tenant/update', [

                'super_tenant_id' => $request->super_tenant_id,
                'name' => $request->name,
                'is_super' => $request->is_super,
                'version' => $request->version,
                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t Update Tenant. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Tenant has been added successfully']);
        }
    }

    public function delete(Request $request)
    {
      
            $tenantId = '';
            $signature = '';
            $refNumber = ''; 
            $reqTime = '';
            $x_cunsomer_username =  '';
            $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->post( $this->apiTms() . 'api/v1/tenant/delete', [
                'version' => $request->version,
                'id' => $request->id,

            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Tenant has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
