<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class TerminalController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
      
		return view('pages.terminal.index', [
				    'data'=>null,
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['modelId']   = $request->deviceModelId; 
        $param['merchantId']   = $request->merchantId; 
        $param['profileId']   = $request->profileId; 
        $param['sn']   = $request->sn; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminal/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
       
        $responseMer = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchant/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedm = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/deviceModel/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedp = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/profile/list', [
            'body' => json_encode($param)
        ])->json();

        return view('pages.terminal.form', [
           
            'merchant'=> $responseMer['rows'] ,
            'devicemodel'=>$responsedm['rows'],
            'deviceprofile'=>$responsedp['rows'],
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
      
        $responseMer = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchant/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedm = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/deviceModel/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedp = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/profile/list', [
            'body' => json_encode($param)
        ])->json();


        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/terminal/get', [
            'id' => $request->id
        ])->json();

        return view('pages.terminal.form', [
           
            'merchant'=> $responseMer['rows'] ,
            'devicemodel'=>$responsedm['rows'],
            'deviceprofile'=>$responsedp['rows'],
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }
    
    public function store(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/terminal/add', [
                'sn' => $request->sn,
                'modelId' => $request->modelId,
                'merchantId' => $request->merchantId,
                'profileId' => $request->profileId,
                'terminalGroupIds' =>  json_decode($request->terminalGroupIds),
                
            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]); //'Can\'t add Terminal. Please try again'
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Termianl has been added successfully']);
        }
    }

    public function  show(Request $request)
    {
        

        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminal/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
      

        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/terminal/update', [

                'sn' => $request->sn,
                'modelId' => $request->modelId,
                'merchantId' => $request->merchantId,
                'profileId' => $request->profileId,
                'terminalGroupIds' =>  json_decode($request->terminalGroupIds),
                'id' => $request->id,
                'version' => $request->version

                

            ])->json();
           
      //'Can\'t Update Terminal. Please try again'
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Terminal has been update successfully']);
        }
    }

    public function delete(Request $request)
    {
      

            $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/terminal/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Terminal has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }

    public function lockUnlock(Request $request)
    {
        $response = $this->httpWithHeaders()
            
        ->post( $this->apiTms() . 'api/v1/terminal/lockUnlock', [

            'version' => $request->version,

            'id' => $request->id,

            'action' => "UNLOCK",
   
            'lockReason' => "Kunci aja soih"


        ])->json();
       
  
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'OK']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function restart(Request $request)
    {
        $response = $this->httpWithHeaders()
            
        ->post( $this->apiTms() . 'api/v1/terminal/restart', [

            'version' => $request->version,

            'id' => $request->id,


        ])->json();
       
  
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'OK']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }
    }
    
    public function autoComplateMerchant(Request $request)
    {
        $search = $request->search;
        $response = null;
        if($search !== ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/merchant/list', [
                'name' =>$search
            ])->json();
           
        }
        // else{
        //     $response = $this->httpWithHeaders()
        //     ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
        //         'modelName' => $search
        //     ])->json();
        // }

        //print_r($response);
        
        if($response['responseCode'] == '0000')
        {
            $r = array();
            foreach($response['rows'] as $data){
                $r[] = array("value"=>$data['id'],"label"=>$data['name']);
            }

            return response()->json($r);

        }

        
    }


    public function autoCompleteDeviceModel(Request $request)
    {
        $search = $request->search;
        $response = null;
        if($search !== ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
                'modelName' =>$search
            ])->json();
           
        }
        // else{
        //     $response = $this->httpWithHeaders()
        //     ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
        //         'modelName' => $search
        //     ])->json();
        // }

        //print_r($response);
        
        if($response['responseCode'] == '0000')
        {
            $r = array();
            foreach($response['rows'] as $data){
                $r[] = array("value"=>$data['id'],"label"=>$data['model']);
            }

            return response()->json($r);

        }

        
    }

    public function autoComplateProfile(Request $request)
    {
        $search = $request->search;
        $response = null;
        if($search !== ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/profile/list', [
                'name' =>$search
            ])->json();
           
        }
        // else{
        //     $response = $this->httpWithHeaders()
        //     ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
        //         'modelName' => $search
        //     ])->json();
        // }

        //print_r($response);
        
        if($response['responseCode'] == '0000')
        {
            $r = array();
            foreach($response['rows'] as $data){
                $r[] = array("value"=>$data['id'],"label"=>$data['name']);
            }

            return response()->json($r);

        }

        
    }

    

}
