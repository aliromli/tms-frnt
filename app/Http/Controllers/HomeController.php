<?php

namespace App\Http\Controllers;


use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    
    public function index()
    {
    //    echo '<pre>';
             //print(session()->get('user.userGroup.name'));
          //print(session()->get('user.userGroup.menuActions'));
              //dd(session()->get('menus'));
          //print(session()->get('user.tenant_id'));
    //       //echo session()->get('tenant');
    //       //echo session()->get('merchant');
        // echo '</pre>';
        // echo json_encode($this->statusActions('/home','READ','Y'));
        
       //echo session()->get('user');
		
		return view('home', [
					'totalTransaction' => null,
					'totalPatient' => null,
					'totalPacket' => null,
					'totalDepartment' => null,
				]);
		
		
       
    }

    /**
     * After reset password, setup session and redirect to home
     *
     * @return void
     */
    public function setup()
    {
        // Check if user has setting language
        $appLocale = Auth::user()->setting != null ? Auth::user()->setting->language : 'en';

        // Build menus
        $userGroupActions = collect(Auth::user()->userGroup->menuActions)->pluck('id')->toArray();
        $menus = Menu::where('parent_id', null)
                ->whereHas('actions', function($q) use ($userGroupActions) { $q->whereIn('id', $userGroupActions); })
                ->orderBy('sequence')
                ->get();

        // Set session and redirect to home
        session(['user' => Auth::user(), 'menus' => $menus, 'app-locale' => $appLocale]);

        return redirect('home');
    }

	
	
	


}
