<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class CitiesController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
        $param['pageNum']   = 1;
        $param['pageSize']   = 500;
        
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}';
        $reqTime = '{{$isoTimestamp}}';
        $x_cunsomer_username =  'tes';
        
        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
         ->send('GET',  $this->apiTms() . 'api/v1/state/list', [
            'body' => json_encode($param)
         ])->json();

       
		return view('pages.city.index', [
					'state' => $response['rows'],
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['stateId']   = $request->state; 
        $param['name']   = $request->city; 
        
        
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}';
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';


        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
        ->get( $this->apiTms()  . 'api/v1/city/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function store(Request $request)
    {
        
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}'; 
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->post( $this->apiTms()  . 'api/v1/city/add', [
                'states_id' => $request->state,
                'name' => $request->name
            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t add City. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'City has been added successfully']);
        }
    }

    public function  show(Request $request)
    {
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}'; 
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->get( $this->apiTms() . 'api/v1/city/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}'; 
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
         
            ->post( $this->apiTms() . 'api/v1/city/update', [

                'states_id' => $request->state,
                'name' => $request->name,
                'version' => $request->version,
                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t Update City. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'City has been added successfully']);
        }
    }

    public function delete(Request $request)
    {
      
            $tenantId = 'qualita';
            $signature = 'tes';
            $refNumber = '{{ref}}{{$isoTimestamp}}'; 
            $reqTime = '{{$timestamp}}';
            $x_cunsomer_username =  'tes';

            $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            
            ->post( $this->apiTms() . 'api/v1/city/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'City has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
