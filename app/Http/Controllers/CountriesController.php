<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Config;

class CountriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      	return view('pages.country.index', [
					'data' => null,
				]);
	 
    }

    /**
     * Country datatables
     *
     * @return type JSON country
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        // if(!empty($request->name))
        // {
           
        //     $users->where('name', 'ILIKE', '%'.$request->name.'%');
        // }
        // if(!empty($request->group))
        // {
        //     $users->where('user_group_id',  $request->group);
        // }
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 10;
        //$param['name']   = 10;
        //$param['code']   = 10;
               
        $ep = Config::get('services.tms.ep_tms');
        $response = Http::withHeaders([
                     //'Api-Key' => env('CLIENT_API_KEY'),
                    'Tenant-id' => 'qualita',
                    'signature' => 'tes',
                    'Reference-Number' => '{{ref}}{{$isoTimestamp}}',
                    'Request-Timestamp' => '{{$isoTimestamp}}',
                    'X-Consumer-Username' => 'tes'
         ])
         ->send('GET',  $ep . 'api/v1/country/list', [
            'body' => json_encode($param)
         ])->json();

     
        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $response['total'],
            'recordsFiltered'   => $response['total'],
            'data'              => $response['rows'],
            'input'             => [
                'start' => $request->start,
                'draw' => $request->draw,
                'length' => $request->length,
                'order' => $orderIndex,
                'orderDir' => $orderDir,
                'orderColumn' => $request->columns[$orderIndex]['data']
            ]
        ]);
       
    }

    public function store(Request $request)
    {
        
      
        $param['code']   =  $request->code;
        $param['name']   =  $request->name;
        $ep = Config::get('services.tms.ep_tms');           
        $response = Http::withHeaders([
            //'Api-Key' => env('CLIENT_API_KEY'),
            'Tenant-id' => 'qualita',
            'signature' => 'tes',
            'Reference-Number' => '{{ref}}{{$randomUUID}}',
            'Request-Timestamp' => '{{$isoTimestamp}}',
            'X-Consumer-Username' => 'tes'
            ])
            ->post( $ep . 'api/v1/country/add', [

                'code' => $request->code,

                'name' => $request->name

            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t add Country. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Country has been added successfully']);
        }
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function  show(Request $request)
    {
        //$u = $user->find($id);
        //return response()->json($u);
        //$param['id']   =  $request->id;
        //$param['name']   =  $request->name;
        $ep = Config::get('services.tms.ep_tms');           
        $response = Http::withHeaders([
            //'Api-Key' => env('CLIENT_API_KEY'),
            'Tenant-id' => 'qualita',
            'signature' => 'tes',
            'Reference-Number' => '{{ref}}{{$isoTimestamp}}',//'{{ref}}{{$randomUUID}}',
            'Request-Timestamp' => '{{$timestamp}}',
            'X-Consumer-Username' => 'tes'
            ])
            ->get( $ep . 'api/v1/country/get', [

                'id' => $request->id

            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        $param['code']   =  $request->code;
        $param['name']   =  $request->name;
        $ep = Config::get('services.tms.ep_tms');           
        $response = Http::withHeaders([
            //'Api-Key' => env('CLIENT_API_KEY'),
            'Tenant-id' => 'qualita',
            'signature' => 'tes',
            'Reference-Number' => '{{ref}}{{$randomUUID}}',
            'Request-Timestamp' => '{{$isoTimestamp}}',
            'X-Consumer-Username' => 'tes'
            ])
            ->post( $ep . 'api/v1/country/update', [

                'code' => $request->code,

                'name' => $request->name,

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t Update Country. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Country has been added successfully']);
        }
    }
    public function delete(Request $request)
    {

      
        $ep = Config::get('services.tms.ep_tms');           
        $response = Http::withHeaders([
            //'Api-Key' => env('CLIENT_API_KEY'),
            'Tenant-id' => 'qualita',
            'signature' => 'tes',
            'Reference-Number' => '{{ref}}{{$randomUUID}}',
            'Request-Timestamp' => '{{$timestamp}}',
            'X-Consumer-Username' => 'tes'
            ])
            ->post( $ep . 'api/v1/country/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Country has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
