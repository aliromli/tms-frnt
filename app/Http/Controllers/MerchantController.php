<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class MerchantController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
       
        $responseDistrict = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/district/list', [
            'body' => json_encode($param)
        ])->json();

        $responseType = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchantType/list', [
            'body' => json_encode($param)
        ])->json();

       
		return view('pages.merchant.index', [
					//'tenant' => array("tripmitra","qualita"), //$response['rows'],
                    'district'=> $responseDistrict['rows'] ,
                    'merchantType'=>$responseType['rows'],
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        //$param['stateId']   = $request->state; 
        $param['name']   = $request->name; 
      
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/merchant/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
        // $tenantId = 'qualita';
        // $signature = 'tes';
        // $refNumber = '{{ref}}{{$isoTimestamp}}';
        // $reqTime = '{{$timestamp}}';
        // $x_cunsomer_username =  'tes';
       
        $responseDistrict = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/district/list', [
            'body' => json_encode($param)
        ])->json();

        $responseType = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchantType/list', [
            'body' => json_encode($param)
        ])->json();

        return view('pages.merchant.form', [
            //'tenant' => array("tripmitra","qualita"), 
            'district'=> $responseDistrict['rows'] ,
            'merchantType'=>$responseType['rows'],
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
        // $tenantId = 'qualita';
        // $signature = 'tes';
        // $refNumber = '{{ref}}{{$isoTimestamp}}';
        // $reqTime = '{{$timestamp}}';
        // $x_cunsomer_username =  'tes';
       
        $responseDistrict = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/district/list', [
            'body' => json_encode($param)
        ])->json();

        $responseType = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchantType/list', [
            'body' => json_encode($param)
        ])->json();

        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/merchant/get', [
            'id' => $request->id
        ])->json();

        return view('pages.merchant.form', [
           
            'district'=> $responseDistrict['rows'] ,
            'merchantType'=>$responseType['rows'],
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }

    public function store(Request $request)
    {
        
        // $tenantId = 'qualita';
        // $signature = 'tes';
        // $refNumber = '{{ref}}{{$isoTimestamp}}'; 
        // $reqTime = '{{$timestamp}}';
        // $x_cunsomer_username =  'tes';

        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/merchant/add', [
                'name' => $request->name,
                'companyName' => $request->companyName,
                'address' => $request->address,
                'districtId' => $request->districtId,
                'zipcode' => $request->zipcode,
                'merchantTypeId'  => $request->merchantTypeId,
            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t add Merchant. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Merchant has been added successfully']);
        }
    }

    public function  show(Request $request)
    {
       
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/merchant/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
     
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/merchant/update', [

                'name' => $request->name,
                'companyName' => $request->companyName,
                'address' => $request->address,
                'districtId' => $request->districtId,
                'zipcode' => $request->zipcode,
                'merchantTypeId'  => $request->merchantTypeId,
                'id' => $request->id,
                'version' => $request->version


            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t Update Marhcant. Please try again']);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'marchant has been Update successfully']);
        }
    }

    public function delete(Request $request)
    {
      
            $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/merchant/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Merchant has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
