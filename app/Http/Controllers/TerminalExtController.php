<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DateTime;


class TerminalExtController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	return view('pages.terminalExt.index', [
					'data' => null
				]);
	}

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        //$param['stateId']   = $request->state; 
        $param['name']   = $request->name; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminalExt/list', $param)->json();
   
		//print_r($response);
		
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
       
        return view('pages.terminalExt.form', [
           
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
       
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/terminalExt/get', [
            'id' => $request->id
        ])->json();
  
        return view('pages.terminalExt.form', [
            'data'=>$response['data'],
            'edit' => 'yes'
        ]);
    }

    public function store(Request $request)
    {
     	$response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/terminalExt/add', [
			   
                'tid' =>  $request->tid,
				'mid' =>  $request->mid,
				'merchantName1' =>  $request->merchantName1,
				'merchantName2' =>  $request->merchantName2,
				'merchantName3' =>  $request->merchantName3,
				'merchant_password' =>  $request->merchant_password,
				'admin_password' =>  $request->admin_password,
				'callCenter1' =>  $request->callCenter1,
				'callCenter2' =>  $request->callCenter2,
				'settlementMaxTrxCount' =>  $request->settlementMaxTrxCount,
				'settlementWarningTrxCount' =>  $request->settlementWarningTrxCount,
				'settlementPassword' =>  $request->settlementPassword,
				'voidPassword' =>  $request->voidPassword,
				'brizziDiscountPercentage' =>  $request->brizziDiscountPercentage,
				'brizziDiscountAmount' =>  $request->brizziDiscountAmount,
				'fallbackEnabled' =>  $request->fallbackEnabled,
				'featureSale' =>  $request->featureSale,
				'featureInstallment' =>  $request->featureInstallment,
				'featureCardCerification' =>  $request->featureCardCerification,
				'featureSaleRedemption' =>  $request->featureSaleRedemption,
				'featureManualKeyIn' =>  $request->featureManualKeyIn,
				'featureSaleCompletion' =>  $request->featureSaleCompletion,
				'featureSaleTip' =>  $request->featureSaleTip,
				'featureSaleFareNonFare' =>  $request->featureSaleFareNonFare,
				'featureQris' =>  $request->featureQris,
				'featureContactless' =>  $request->featureContactless,
				'randomPinKeypad' =>  $request->randomPinKeypad,
				'beepPinKeypad' =>  $request->beepPinKeypad,
				'autoLogon' =>  $request->autoLogon,
				'pushLogon' =>  $request->pushLogon,
				'hostReport' =>  $request->hostReport,
				'hostLogging' =>  $request->hostLogging,
				'importDefault' =>  $request->importDefault

                
            ])->json();
			$this->responseCode($response,'TerminalExt has been added successfully');  
    }

    public function  show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminalExt/get', [
                'id' => $request->id
            ])->json();
           
		
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
       
		
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/terminalExt/update', [
                'tid' =>  $request->tid,
				'mid' =>  $request->mid,
				'merchantName1' =>  $request->merchantName1,
				'merchantName2' =>  $request->merchantName2,
				'merchantName3' =>  $request->merchantName3,
				'merchant_password' =>  $request->merchant_password,
				'admin_password' =>  $request->admin_password,
				'callCenter1' =>  $request->callCenter1,
				'callCenter2' =>  $request->callCenter2,
				'settlementMaxTrxCount' =>  $request->settlementMaxTrxCount,
				'settlementWarningTrxCount' =>  $request->settlementWarningTrxCount,
				'settlementPassword' =>  $request->settlementPassword,
				'voidPassword' =>  $request->voidPassword,
				'brizziDiscountPercentage' =>  $request->brizziDiscountPercentage,
				'brizziDiscountAmount' =>  $request->brizziDiscountAmount,
				'fallbackEnabled' =>  $request->fallbackEnabled,
				'featureSale' =>  $request->featureSale,
				'featureInstallment' =>  $request->featureInstallment,
				'featureCardCerification' =>  $request->featureCardCerification,
				'featureSaleRedemption' =>  $request->featureSaleRedemption,
				'featureManualKeyIn' =>  $request->featureManualKeyIn,
				'featureSaleCompletion' =>  $request->featureSaleCompletion,
				'featureSaleTip' =>  $request->featureSaleTip,
				'featureSaleFareNonFare' =>  $request->featureSaleFareNonFare,
				'featureQris' =>  $request->featureQris,
				'featureContactless' =>  $request->featureContactless,
				'randomPinKeypad' =>  $request->randomPinKeypad,
				'beepPinKeypad' =>  $request->beepPinKeypad,
				'autoLogon' =>  $request->autoLogon,
				'pushLogon' =>  $request->pushLogon,
				'hostReport' =>  $request->hostReport,
				'hostLogging' =>  $request->hostLogging,
				'importDefault' =>  $request->importDefault
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
			return $this->responseCode($response,'TerminalExt has been update successfully');
		
      
    }

    public function delete(Request $request)
    {
      
            $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/terminalExt/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'TerminalExt has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
