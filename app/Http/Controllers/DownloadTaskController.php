<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DateTime;

class DownloadTaskController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	return view('pages.download_task.index', [
	                'data' =>null
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['packageName']   = $request->packageName; 
        $param['name']   = $request->name;
        $param['sn']   = $request->sn;
        if(count(json_decode($request->applicationId,true)) > 0){
            $param['applicationId'] = $request->applicationId;
        }
        if(count(json_decode($request->terminalId,true)) > 0){
            $param['terminalId'] = $request->terminalId;
        }
        if(count(json_decode($request->terminalGroupId,true)) > 0){
            $param['terminalGroupId'] = $request->terminalGroupId;
        }
      
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/downloadTask/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        

        return view('pages.download_task.form', [
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
       
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/downloadTask/get', [
            'id' => $request->id
        ])->json();

        return view('pages.download_task.form', [
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }

    public function store(Request $request)
    {
        $dst = DateTime::createFromFormat('Y-m-d\TH:i:s', $request->downloadTime);
        $dst = date_create(json_decode(json_encode($dst),true)['date']);
        $downloadTime = date_format($dst,'Y-m-d H:i:s');


        $dst2 = DateTime::createFromFormat('Y-m-d\TH:i:s', $request->installationTime);
        $dst2 = date_create(json_decode(json_encode($dst2),true)['date']);
        $installationTime = date_format($dst2,'Y-m-d H:i:s');

        $dst3 = DateTime::createFromFormat('Y-m-d\TH:i:s', $request->publishTime);
        $dst3 = date_create(json_decode(json_encode($dst3),true)['date']);
        $publishTime = date_format($dst3,'Y-m-d H:i:s');


        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/downloadTask/add', [
                'name' => $request->name,
                'publishTimeType' => $request->publishTimeType,
                'publishTime' => $publishTime, 
                'installationTimeType' => $request->installationTimeType,
                'installationTime' => $installationTime,
                'installationNotification'  => $request->installationNotification,
                'status' =>  $request->status,
                'applicationIds' =>  json_decode($request->applicationIds),
                'terminalGroupIds' =>  json_decode($request->terminalGroupIds),
                'terminalIds' =>  json_decode($request->terminalIds),
                'downloadTimeType' => $request->downloadTimeType,
                'downloadTime' =>$downloadTime
            ])->json();


          
            if($response['responseCode'] =='5555')
            {
                return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
            }
            else if($response['responseCode'] =='3333')
            {
                return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
            }
            else{
                return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Download Task has been Added successfully']);
            }
           
      
    }

    public function  show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/downloadTask/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        $dst = DateTime::createFromFormat('Y-m-d\TH:i:s', $request->downloadTime);
        $dst = date_create(json_decode(json_encode($dst),true)['date']);
        $downloadTime = date_format($dst,'Y-m-d H:i:s');


        $dst2 = DateTime::createFromFormat('Y-m-d\TH:i:s', $request->installationTime);
        $dst2 = date_create(json_decode(json_encode($dst2),true)['date']);
        $installationTime = date_format($dst2,'Y-m-d H:i:s');

        $dst3 = DateTime::createFromFormat('Y-m-d\TH:i:s', $request->publishTime);
        $dst3 = date_create(json_decode(json_encode($dst3),true)['date']);
        $publishTime = date_format($dst3,'Y-m-d H:i:s');

        
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/downloadTask/update', [
                'name' => $request->name,
                'publishTimeType' => $request->publishTimeType,
                'publishTime' => $publishTime, 
                'installationTimeType' => $request->installationTimeType,
                'installationTime' => $installationTime,
                'installationNotification'  => $request->installationNotification,
                'status' =>  $request->status,
                'applicationIds' =>  json_decode($request->applicationIds),
                'terminalGroupIds' =>  json_decode($request->terminalGroupIds),
                'terminalIds' =>  json_decode($request->terminalIds),
                'downloadTimeType' => $request->downloadTimeType,
                'downloadTime' => $downloadTime,
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
        //'Can\'t Update Download Task. Please try again'
        if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
        else if($response['responseCode'] =='5555')
        {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
        else if($response['responseCode'] =='0500')
        {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
            
        }
        else if($response['responseCode'] =='3333')
        {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Download Task has been Update successfully']);
        }
    }

    public function delete(Request $request)
    {
      
            $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/downloadTask/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Download Task has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }

    

    public function autoCompleteApplication(Request $request)
    {
        //api/v1/application/list
        $search = $request->q;
        $response = null;
        if($search == ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/application/list', [
                'name' => null
            ])->json();
           
        }else{
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/application/list', [
                'name' => $search
            ])->json();
        }

       
    
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'total_count'      => $response['total'],
                'incomplete_results'   => false, 
                'items'              => $response['rows'],
                
            ]);
        }

       
    }

    public function autoCompleteTerminal(Request $request)
    {
       
        $search = $request->q;
        $response = null;
        if($search == ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminal/list', [
                'name' => null
            ])->json();
           
        }else{
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminal/list', [
                'name' => $search
            ])->json();
        }

       
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'total_count'      => $response['total'],
                'incomplete_results'   => false, 
                'items'              => $response['rows'],
                
            ]);
        }

     
    }

    public function autoCompleteTerminalGroup(Request $request)
    {
       
        $search = $request->q;
        $response = null;
        if($search == ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminalGroup/list', [
                'name' => null
            ])->json();
           
        }else{
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminalGroup/list', [
                'name' => $search
            ])->json();
        }

       
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'total_count'      => $response['total'],
                'incomplete_results'   => false, 
                'items'              => $response['rows'],
                
            ]);
        }

       
    }


}
