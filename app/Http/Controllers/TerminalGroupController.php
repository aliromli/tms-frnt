<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class TerminalGroupController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
     	return view('pages.terminal_group.index', [
				    'data'=>null,
				]);
		
       
    }
    //autoCompleteTerminal
    public function autoCompleteTerminal(Request $request){

        $search = $request->search;
        $response = null;
        if($search == ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminal/list', [
                'modelName' => null
            ])->json();
           
        }else{
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminal/list', [
                'modelName' => $search
            ])->json();
        }

        $r = array();
        foreach($response['rows'] as $data){
            $r[] = array("value"=>$data['id'],"label"=>$data['model']);
        }

        return response()->json($r);

    }

    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['terminalId']   = $request->terminalId; 
        $param['name']   = $request->name;  
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminalGroup/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }


    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function listTerminal(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        //$param['stateId']   = $request->state; 
        //$param['name']   = $request->city;  //listTermina
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminalGroup/listTerminal', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        // $response = $this->httpWithHeaders()
        //     ->get( $this->apiTms() . 'api/v1/terminalGroup/get', [
        //         'id' => $request->id
        //     ])->json();
      
        return view('pages.terminal_group.form', [
            'data'=> null,
            'edit' => 'no'
        ]);

    }
    public function formEdit(Request $request)
    {
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/terminalGroup/get', [
            'id' => $request->id
        ])->json();
        return view('pages.terminal_group.form', [
            //'district'=> $responseDistrict['rows'] ,
            //'merchantType'=>$responseType['rows'],
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }
    public function store(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/terminalGroup/add', [
                'name' => $request->name,
                'description' => $request->description,
                'terminalIds' =>  json_decode($request->terminalIds),
            ])->json();
           
      
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' =>$response['responseDesc']]); //'Can\'t add Terminal Group. Please try again'
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Termianl Group has been added successfully']);
        }
    }

    public function  show(Request $request)
    {
       
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminalGroup/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms() . 'api/v1/terminalGroup/update', [

                'name' => $request->name,
                'description' => $request->description,
                'terminalIds' => json_decode($request->terminalIds),
                'id' => $request->id,
                'version' => $request->version
            ])->json();
        if($response['responseCode'] !='0000'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]); //'Can\'t Update Terminal Group. Please try again'
        }
        else{
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Terminal Group has been added successfully']);
        }
    }
    public function delete(Request $request)
    {
            $response = $this->httpWithHeaders()
            ->post( $this->apiTms() . 'api/v1/terminalGroup/delete', [
                'version' => $request->version,
                'id' => $request->id,
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Terminal Group has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}
