<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use App\Models\Menu;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function apiTms(){
   
        return Config::get('services.tms.ep_tms');
    }
  
    public function httpWithHeaders(){

        $response = Http::withHeaders([
           'Tenant-id' =>  session()->get('user.tenant_id'),//'testing',//'tripmitra',//'qualita',
           'signature' => 'tes',
           'Reference-Number' => '{{ref}}{{$isoTimestamp}}',
           'Request-Timestamp' => '{{$timestamp}}',
           'X-Consumer-Username' => session()->get('user.name')
        ]);

        return $response;

    }
	
	public function httpWithHeaders4(){

        $response = Http::withHeaders([
           //'Tenant-id' =>  session()->get('user.tenant_id'),//'testing',//'tripmitra',//'qualita',
           'signature' => 'tes',
           'Reference-Number' => '{{ref}}{{$isoTimestamp}}',
           'Request-Timestamp' => '{{$timestamp}}',
           'X-Consumer-Username' => session()->get('user.name')
        ]);

        return $response;

    }

    public function httpWithHeadersFile(){

        $response = Http::withHeaders([
            //'Api-Key' => env('CLIENT_API_KEY'),
           'Tenant-id' => session()->get('user.tenant_id'),
           'signature' => 'tes',
           'Reference-Number' => '{{ref}}{{$isoTimestamp}}',
           'Request-Timestamp' => '{{$timestamp}}',
           'X-Consumer-Username' =>session()->get('user.name'),
           'Content-Type' => 'x-www-form-urlencoded',//'multipart/form-data'
        ]);

        return $response;

    }
  
    public function httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username){

        $response = Http::withHeaders([
           'Tenant-id' => $tenantId,
           'signature' => $signature,
           'Reference-Number' => $refNumber,
           'Request-Timestamp' => $reqTime,
           'X-Consumer-Username' =>$x_cunsomer_username
        ]);

        return $response;

    }

    public function httpWithHeaders3($signature,$refNumber,$reqTime){

        $response = Http::withHeaders([
          
           'Tenant-id' => session()->get('user.tenant_id'),
           'signature' => $signature,
           'Reference-Number' => $refNumber,
           'Request-Timestamp' => $reqTime,
           'X-Consumer-Username' => session()->get('user.name')
        ]);

        return $response;

    }

    public function statusActions($url,$type,$is_visible){
        $m = Menu::where('action_url',$url)->first();
        $mn = session()->get('user.userGroup.menuActions');
        $r = false;
        foreach($mn as $d)
        {
            if($d->menu_id == $m->id && $d->action_type == $type && $d->is_visible == $is_visible)
            {
                $r = true;
                break;
            }
        }

        return $r;
    } 
	
	/* response update and add */
	public function responseCode($response,$str)
	{
		//5555 //validator
		//0400 not
		//0000 OK
		//3333 exception
		
		switch ($response['responseCode']) {
			
			  case "5555": //validator
					return response()->json(['responseCode' => '5555', 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
					break;
			  case "0400":
				   return response()->json(['responseCode' => '0400', 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
				   break;
			  case "3333":
					return response()->json(['responseCode' => '3333', 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
			  case "0000":
					return response()->json(['responseCode' => '0000', 'responseStatus' => 'OK', 'responseMessage' => $str]);
			  
		}
	}
	
	/* response show/get */
	public function responseCodeGet($response,$str)
	{
		switch ($response['responseCode']) {
			
			  case "5555": //validator
					return response()->json(['responseCode' => '5555', 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
					break;
			  case "0400":
				   return response()->json(['responseCode' => '0400', 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
				   break;
			  case "3333":
					return response()->json(['responseCode' => '3333', 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
			  case "0000":
					return response()->json(['responseCode' => '0000', 'responseStatus' => 'OK', 'responseMessage' => $str]);
			  
		}
	}

}
